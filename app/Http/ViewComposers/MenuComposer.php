<?php

namespace App\Http\ViewComposers;

use App\Models\MenuTag;
use Illuminate\View\View;

class MenuComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $menuTagModel;

    /**
     * Create a new profile composer.
     *
     * @param MenuService $menuService
     */
    public function __construct(MenuTag $menuTagModel)
    {
        // Dependencies automatically resolved by service container...
        $this->menuTagModel = $menuTagModel;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $menuTags = $this->menuTagModel->where('shown_in_header_menu', 1)->get();
        $view->with('menuTags', $menuTags);
    }

}