<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\MenuTag;

class MenuTagController extends Controller
{
    protected $tagModel;
    protected $menuTagModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Tag $tagModel, MenuTag $menuTagModel )
    {
        $this->tagModel = $tagModel;
        $this->menuTagModel = $menuTagModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->menuTagModel);
        $menuTags = $this->menuTagModel->orderBy('order', 'asc')->get();
        return view('admin.templates.menu_tag.index')->with([
            'menuTags' => $menuTags
        ]);
    }
    public function store(Request $request)
    {
        $this->authorize('store', $this->menuTagModel);
        $this->validate($request, [
          'name' => 'required|',
        ],[],[
            'name' =>  trans('table_fields.tags.name')
        ]);
        $tag = $this->tagModel->where('name', '=', $request->name)->first();
        if(is_null($tag)){
            $tagData = $request->only(['name']);
            $tagData['order'] = $this->tagModel->max('order') + 1;
            $tag = $this->tagModel->create($tagData);
        }
        if(!$this->menuTagModel->where('tag_id', '=', $tag->id)->exists()){
            $menuTagData = [
                'tag_id' => $tag->id,
                'order' => $this->menuTagModel->max('order') + 1,
                'shown_in_header_menu' => '1',

            ];
            $nemuTag = $this->menuTagModel->create($menuTagData);
        }
        
        return redirect()->route('admin.menu_tag.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.menu_tag')]));
    }
    public function update(Request $request, $id)
    {
        $menuTag = $this->menuTagModel->findOrFail($id);
        $this->authorize('update', $menuTag);
        $postData = [];
        if($request->has('shown_in_header_menu') && in_array($request->shown_in_header_menu, [0,1])){
            $postData['shown_in_header_menu'] = $request->shown_in_header_menu;
        }else{
            $postData['shown_in_header_menu'] = 0;
        }
        $menuTag->fill($postData)->save();
        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
        return redirect()->route('admin.menu_tag.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.menu_tag')]));
    }
    public function destroy($id)
    {
        $menuTag = $this->menuTagModel->findOrFail($id);
        $this->authorize('destroy', $menuTag);
        $order = $menuTag->order;
        $this->menuTagModel->destroy($id);
        foreach ($this->menuTagModel->where('order', '>', $order)->cursor() as $record) {
            $order = $record->order - 1;
            $record->order = $order;
            $record->save();
        }
        return redirect()->route('admin.menu_tag.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.menu_tag')]));
    }

}
