<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Inquiry;

class InquiryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $inquiryModel;
    public function __construct(Inquiry $inquiryModel)
    {
        $this->inquiryModel = $inquiryModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', Inquiry::class);
        $inquiries = $this->inquiryModel->getInquiries($request);
        return view('admin.templates.inquiry.index')->with([
            'inquiries' => $inquiries
        ]);
    }

    public function edit(Request $request, $id)
    {
        $inquiry = $this->inquiryModel->findOrFail($id);
        $this->authorize('edit', $inquiry);
        return view('admin.templates.inquiry.edit')->with([
            'inquiry' => $inquiry
        ]);
    }

    public function update(Request $request, $id)
    {
        $inquiry = $this->inquiryModel->findOrFail($id);
        $this->authorize('update', $inquiry);
    	$this->validate($request, [
          'email' => 'required|email',
          'content' => 'required',
          'work_status' => 'required',
        ],[],[
            'email' => trans('table_fields.inquiries.email'),
            'content' => trans('table_fields.inquiries.content'),
            'work_status' => trans('table_fields.inquiries.work_status'),
        ]);
    	$attributes = $request->only(['email', 'content', 'work_status']);
        $inquiry->fill($attributes)->save();
        return redirect()->route('admin.inquiry.edit', $inquiry)->with('success', trans('messages.success.update', ['Module' => trans('module.entry.inquiry')]));
    }

    public function updateAll(Request $request)
    {
        $this->validate($request, [
            'ids.*' => 'required|integer',
            'ids' => 'required',
            'work_status' => 'required'
        ],[],[
            'ids' => trans('table_fields.inquiries.ids'),
            'work_status' => trans('table_fields.inquiries.work_status')
        ]);
        $attributes = $request->only(['ids', 'work_status']);
        $this->inquiryModel->updateAll($attributes);
        return redirect()->route('admin.inquiry.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entries.inquiry')]));
    }

    public function export(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=inquiries.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $inquiries = $this->inquiryModel->all();
        $columns = array('ID', 'Email', 'Content', 'Status');

        $callback = function() use ($inquiries, $columns)
        {
            $file = fopen('php://output', 'w');
            fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($file, $columns);

            foreach($inquiries as $inquiry) {
                if($inquiry->work_status == 'incompatible'){
                    $work_status = '未対応';
                }elseif($inquiry->work_status == 'ignore'){
                    $work_status = '無視する';
                }else{
                    $work_status = '対応済み';
                }
                fputcsv($file, array($inquiry->id, $inquiry->email, $inquiry->content, $work_status));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

}
