<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IntroController extends Controller
{
	public function index()
	{
        return view('frontend.templates.intro.index');
    } 
}
