<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Post;
use Helper;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
	protected $postModel;
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct( Post $postModel )
    {
        $this->postModel = $postModel;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

	public function index(Request $request)
	{
		$topPostsNews = $this->postModel->getTopPostsNews(3);
        return view('frontend.templates.news.index')->with([
            'topPostsNews' => $topPostsNews
        ]);
    } 
}
