<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Mail\MailContact;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\Inquiry;
use App\Models\Page;
use App\Models\SupportEmail;

class InquiryController extends Controller
{
    protected $inquiryModel;
    protected $pageModel;
    protected $supportEmailModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Inquiry $inquiryModel, Page $pageModel, SupportEmail $supportEmailModel )
    {
        $this->inquiryModel = $inquiryModel;
        $this->pageModel = $pageModel;
        $this->supportEmailModel = $supportEmailModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $page = $this->pageModel->getPageBySlug('inquiry');
        if(!$page){
            abort(404);
        }
        $metaTitle = $page->title;
        return view('frontend.templates.inquiry.index')->with([
            'page' => $page,
            'metaTitle' => $metaTitle
        ]);
    }
    public function submit(Request $request)
    {
        $this->validate($request, [
          'email' => 'required|email',
          'message' => 'required',
        ],[],[
            'email' =>  trans('table_fields.inquiries.email'),
            'message' =>  trans('table_fields.inquiries.message')
        ]);
        $dataInquiry = $request->except('_token');
        $dataInquiry['content'] = $request->message;
        unset($dataInquiry['message']);
        $inquiry = $this->inquiryModel->create($dataInquiry);
        if($inquiry){
            $supportEmails = $this->supportEmailModel->where('is_approvide', 1)->get()->pluck('email');
            foreach ($supportEmails as $email) {
                Mail::to($email)->send(new MailContact($inquiry));
            }
        }
        
        return redirect()->route('frontend.inquiry.index');
    }
    
}
