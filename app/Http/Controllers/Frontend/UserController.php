<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;

class UserController extends Controller
{
    protected $userModel;
    protected $postModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( User $userModel, Post $postModel )
    {
        $this->userModel = $userModel;
        $this->postModel = $postModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function show(Request $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $request->merge(['user_id' => $id]);
        $posts = $this->postModel->getPosts($request, 1);
        $metaTitle = $user->name;
        return view('frontend.templates.user.show')->with([
            'user' => $user,
            'posts' => $posts,
            'metaTitle' => $metaTitle
        ]);
    }
}
