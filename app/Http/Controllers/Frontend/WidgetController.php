<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class WidgetController extends Controller
{
  protected $postModel;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct( Post $postModel )
  {
    $this->postModel = $postModel;
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function embed(Request $request)
  {
    $posts = $this->postModel->getLatestPosts(1, 4);
    return view('frontend.templates.widget.embed')->with([
      'posts' => $posts
    ]);
  }
}
