<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Post;
use Helper;
use App\Http\Controllers\Controller;

class CollectionsController extends Controller
{
	protected $postModel;
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct( Post $postModel )
    {
        $this->postModel = $postModel;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

	public function index(Request $request)
	{
		$topPosts = $this->postModel->getTopPosts(6);
        //print_r($topPosts);
        //die();
        return view('frontend.templates.collections.index')->with([
            'topPosts' => $topPosts
        ]);
    } 
}
