<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Post;
use Helper;
use App\Http\Controllers\Controller;

class CollectionsDetailController extends Controller
{
	protected $postModel;
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct( Post $postModel )
    {
        $this->postModel = $postModel;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

	public function index(Request $request, $slug)
	{
        $post = $this->postModel->getPostBySlug($request);
        if(!$post){
            abort(404);
        }
        if($post->media_images && json_decode($post->media_images, true)){
            $mainMedias = json_decode($post->media_images, true);
        }else{
            $mainMedias = [];
        }
        if($post->ogp_title){
            $metaTitle = $post->ogp_title;
        }else{
            $metaTitle = $post->title;
        }
        if($post->ogp_description){
            $metaDescription = $post->ogp_description;
        }else{
            $metaDescription = strip_tags(Helper::getPostSummary($post->content));
        }
        if($post->ogp_image_url){
            $metaImage = $post->ogp_image_url;
        }else{
            if($post->featureImage){
                $metaImage = env('APP_URL', false) . Helper::getMediaUrl($post->featureImage, 'original');
            }elseif(Helper::getDefaultCover($post)){
                $metaImage = env('APP_URL', false) . Helper::getDefaultCover($post);
            }
        }
        // print_r($request->slug);
        // die();
		//$showDetail = $this->postModel->getShowDetail($request->slug);
        $showDetail = $this->postModel->findOrFail($request->slug);
        return view('frontend.templates.collections.detail.index')>with([
            'showDetail' => $showDetail,
            'post' => $post,
            'mainMedias' => $mainMedias,
            'relatedPosts' => $relatedPosts,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaImage' => $metaImage
        ]);
    } 
}
