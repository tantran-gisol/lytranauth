<?php

namespace App\Intervention\Image\Filters;
use Intervention\Image\Filters\FilterInterface;
class Filter implements FilterInterface
{
    /**
     * Default size of filter effects
     */
    const DEFAULT_WIDTH = 90;

    /**
     * Size of filter effects
     *
     * @var integer
     */
    private $size;

    /**
     * Creates new instance of filter
     *
     * @param integer $size
     */
    public function __construct($size = null)
    {
        $this->size = is_numeric($size) ? intval($size) : self::DEFAULT_WIDTH;
    }

    /**
     * Applies filter effects to given image
     *
     * @param  Intervention\Image\Image $image
     * @return Intervention\Image\Image
     */
    public function applyFilter(\Intervention\Image\Image $image)
    {
    	$image->resize($this->size,null,function ($constraint) {
		    $constraint->aspectRatio();
   			$constraint->upsize();
		});
        return $image;
    }
}