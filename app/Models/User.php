<?php

namespace App\Models;

use App\Adiva\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'writer_profile_html', 'facebook_id', 'twitter_id', 'receive_notification', 'disable_article_publishing', 'role_id', 'site_id', 'is_official_writer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function previewPosts()
    {
        return $this->belongsToMany(Post::class);
    }
    public function lastMonthTotalViews()
    {
        return $this->hasMany(PostView::class)->whereDate('updated_at', '>', Carbon::now()->subDays(30));
    }

    public function getUserByEmail($email){
        $query = $this->query();
        $query->where('email', $email);
        return $query->first();
    }
    
    public function getUsersByView($request){
        $query = $this->query();
        if($request->has('id') && !empty($request->id)){
            $query->where('id', $request->id);
        }
        if($request->has('order') && in_array($request->order, ['asc', 'desc'])){
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', $request->order);
        }else{
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', 'desc');
        }
        return $query->paginate(20);
    }

    public function getOfficialWriters()
    {
        $query = $this->query();
        $query->where('is_official_writer', 1);
        return $query->paginate();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
