<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	protected $table = 'medias';
	protected $fillable = [
		'name', 'info', 'original_path', 'xlarge_path', 'large_path', 'medium_path', 'small_path', 'thumbnail_path', 'post_id', 'ratio3x2_path', 'ratio4x3_path', 'is_cover'
	];
	public function post()
  {
    return $this->belongsTo(Post::class);
  }
  public function getMedias($request){
    $query = $this->query();
    $query->orderBy('id','desc');
    if($request->has('s') && !empty($request->s)){
      $query->where('name', 'like', '%' . $request->s . '%');
    }
    if($request->has('pt') && !empty($request->pt)){
      $query->join('posts', 'posts.id', '=', 'medias.post_id')
            ->select('posts.id', 'posts.title', 'name', 'info', 'original_path', 'xlarge_path', 'large_path', 'medium_path', 'small_path', 'thumbnail_path', 'post_id', 'ratio3x2_path', 'ratio4x3_path', 'is_cover')
            ->where('title', 'like', '%' . $request->pt . '%');
    }
    return $query->paginate(20);
  }
}