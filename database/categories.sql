-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 04, 2022 lúc 09:36 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `lytranauth_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `featured_image`, `sub_featured_image`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Phụ kiện', NULL, NULL, NULL, '2020-01-20 15:52:14', '2022-04-04 09:25:57'),
(2, 'Thắt lưng', '/storage/media/original/1587792421_1579503207_feature-1-2.jpg', '/storage/media/original/1587792435_1579503224_feature-1-1.jpg', 1, '2020-01-20 15:53:49', '2022-04-04 09:26:39'),
(3, 'Túi xách', '/storage/media/original/1587792478_1579503447_feature-2-2.jpg', '/storage/media/original/1587792490_1579503468_feature-2-1.jpg', 1, '2020-01-20 15:57:54', '2022-04-04 09:29:09'),
(4, 'Ví da', '/storage/media/original/1587792512_1579503556_feature-3-2.jpg', '/storage/media/original/1587792522_1579503581_feature-3-1.jpg', 1, '2020-01-20 15:59:47', '2022-04-04 09:29:36'),
(6, 'Quần áo', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', NULL, '2020-01-20 16:06:14', '2022-04-04 09:26:12'),
(7, 'Váy', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', 6, '2020-01-20 16:11:43', '2022-04-04 09:30:18'),
(8, 'Quần áo nam', '/storage/media/original/1587805215_1578910851_23.jpg', '/storage/media/original/1587805215_1578910851_23.jpg', 6, '2020-01-20 16:13:40', '2022-04-04 09:30:36'),
(9, 'Quần áo nữ', '/storage/media/original/1587805461_1579504621_IMG_7865-2.jpg', '/storage/media/original/1587805461_1579504621_IMG_7865-2.jpg', 6, '2020-01-20 16:17:27', '2022-04-04 09:30:50'),
(10, 'Áo khoác', '/storage/media/original/1587805503_1578908597_1.jpg', '/storage/media/original/1587805503_1578908597_1.jpg', 6, '2020-01-20 16:19:16', '2022-04-04 09:31:17'),
(11, 'Mũ', '/storage/media/original/1587805535_1579504857_S__24739851.jpg', '/storage/media/original/1587805535_1579504857_S__24739851.jpg', 6, '2020-01-20 16:21:18', '2022-04-04 09:34:08'),
(12, 'Áo ấm', '/storage/media/original/1587805562_1579505079_aidea_showroom.jpg', '/storage/media/original/1587805562_1579505079_aidea_showroom.jpg', 6, '2020-01-20 16:25:00', '2022-04-04 09:34:28'),
(13, 'Áo vest', '/storage/media/original/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', '/storage/media/original/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 6, '2020-01-20 16:27:44', '2022-04-04 09:35:13'),
(14, 'Đồng hồ', NULL, NULL, 1, '2020-07-08 16:04:13', '2022-04-04 09:29:52'),
(15, 'Mỹ phẩm', NULL, NULL, NULL, '2022-04-04 09:32:08', '2022-04-04 09:32:08'),
(16, 'Son môi', NULL, NULL, 15, '2022-04-04 09:32:46', '2022-04-04 09:32:46'),
(18, 'Nước hoa', NULL, NULL, 15, '2022-04-04 09:33:13', '2022-04-04 09:33:13');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
