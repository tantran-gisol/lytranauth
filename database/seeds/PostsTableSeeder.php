<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
    	for ($i=1; $i <= 20; $i++) { 
    		$post = Post::updateOrCreate(['id' => $i],
    			[
    				'title' => $faker->sentence(6, true),
    				'content' => $faker->text(500),
    				'post_status' => 1,
    				'user_id' => rand(1,10),
    				'site_id' => 1
    			]
    		);
    		$post->tags()->sync(array(rand(1,10)));
    	}
    }
}
