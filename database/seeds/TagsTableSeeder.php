<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	for ($i=1; $i <= 10; $i++) { 
    		Tag::updateOrCreate(['id' => $i],
    			[
    				'name' => $faker->word,
    				'order' => $i
    			]
    		);
    	}
    }
}
