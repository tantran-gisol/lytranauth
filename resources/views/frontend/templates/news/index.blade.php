@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">TIN TỨC</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Tin tức</span></strong></li>
				</ul>
			</div>
	</section>

	<div class="container">
		<div class="row">
			<aside class="dqdt-sidebar sidebar left left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9 space-30">
				<aside class="aside-item collection-category">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Danh mục</span></h2>
					</div>
					<div class="categories-box">
						<ul class="lv1">
							<li class="nav-item nav-items">
								<a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('collections') }}" title="Giới thiệu">Sản phẩm</a>
							</li>
							<li class="nav-item nav-items active">
								<a href="{{ url('news') }}" title="Giới thiệu">Tin tức</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('contact') }}" title="Giới thiệu">Liên hệ</a>
							</li>
						</ul>
					</div>
				</aside>
				<div class="aside-item">
					<div class="aside-title">
						<h2 class="title-head margin-top-0">
							<a href="/tin-tuc" title="Bài viết liên quan">
								<span>Bài viết liên quan</span>
							</a>	
						</h2>
					</div>
					<div class="list-blogs">							
						<article class="blog-item blog-item-list">		
							<img src="{{asset('image/img/bao-da-chia-khoa-o-to-9346bd053a9f46c09e1c13a597b2603f-grande.jpg')}}" style=" width:40%;margin-right: 20px;" class="img-responsive" alt="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">
							<h3 class="blog-item-name">
								<a href="/ly-do-ma-ban-can-phai-mua-bao-da-chia-khoa-o-to-ngay-lap-tuc" title="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức</a>
							</h3>
						</article>												
						<article class="blog-item blog-item-list">		
							<img src="{{asset('image/img/balo-da-bo-sap-dang-doc-grande.jpg')}}" style="width:40%;margin-right: 20px;" class="img-responsive" alt="Lý do nên chọn mua balo da bò để sử dụng">
							<h3 class="blog-item-name">
								<a href="/ly-do-nen-chon-mua-balo-da-bo-de-su-dung" title="Lý do nên chọn mua balo da bò để sử dụng">Lý do nên chọn mua balo da bò để sử dụng</a>
							</h3>
						</article>															
						<article class="blog-item blog-item-list">		
							<img src="{{asset('image/img/3744eca3c35ed49e977d7cd46d1f88a5.jpg')}}" style="width:40%;margin-right: 20px;" class="img-responsive" alt="Bí kíp chọn mua balo đi làm công sở đẹp, giá tốt nhất">
							<h3 class="blog-item-name">
								<a href="/bi-kip-chon-mua-balo-di-lam-cong-so-dep-gia-tot-nhat" title="Bí kíp chọn mua balo đi làm công sở đẹp, giá tốt nhất">Bí kíp chọn mua balo đi làm công sở đẹp, giá tốt nhất</a>
							</h3>
						</article>						
					</div>
				</div>
				<aside class="aside-item hidden-767">
					<div class="aside-content">
						<a href="#" title="Thời trang nam">
							<img class="img-responsive center-block" src="{{asset('image/img/aside_banner.jpg')}}" alt="Thời trang nam">
						</a>
					</div>
				</aside>
			</aside>
			<section class="right-content col-lg-9 col-md-9  col-md-push-3 col-lg-push-3">
				<section class="list-blogs blog-main">
					@if(count($topPostsNews))
                    	@foreach($topPostsNews as $topPostsNews)	
					<article class="blog-item">
						<div class="blog-item-thumbnail">						
							<a href="/ly-do-ma-ban-can-phai-mua-bao-da-chia-khoa-o-to-ngay-lap-tuc" title="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">
								<picture>
									<img src="{{asset('image/img/bao-da-chia-khoa-o-to-9346bd053a9f46c09e1c13a597b2603f-grande.jpg')}}" style="max-width:100%;" class="img-responsive" alt="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">
								</picture>
							</a>
						</div>
						<div class="blog-item-info">
							<h3 class="blog-item-name"><a href="/ly-do-ma-ban-can-phai-mua-bao-da-chia-khoa-o-to-ngay-lap-tuc" title="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">{{$topPostsNews -> title }}</a></h3>
							<div class="blog-item-meta">
								<div class="post-author">
									Nguyễn Hữu Mạnh
								</div>
								<div class="post-time">
									{{$topPostsNews -> created_at }}
								</div>
								<div class="post-comment">
									<span>0</span> bình luận
								</div>
							</div>
							<p class="blog-item-summary margin-bottom-5">{{strip_tags( $topPostsNews -> content )}}</p>
						</div>
					</article>
					@endforeach
                	@endif	
				</section>
			</section>
		</div>
	</div>

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection