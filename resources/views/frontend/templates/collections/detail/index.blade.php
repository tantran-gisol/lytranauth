@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">GĂNG TAY CẢM ỨNG NAM GTTACUNA-35-D</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Găng tay </span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Găng tay cảm ứng nam GTTACUNA-35-D</span></strong></li>
				</ul>
			</div>
	</section>

	<div class="container">
		<div class="row">
			<aside class="dqdt-sidebar sidebar left left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9 space-30">
				<aside class="aside-item collection-category">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Danh mục</span></h2>
					</div>
					<div class="categories-box">
						<ul class="lv1">
							<li class="nav-item nav-items">
								<a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
							</li>
							<li class="nav-item nav-items active">
								<a href="{{ url('collections') }}" title="Giới thiệu">Sản phẩm</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('news') }}" title="Giới thiệu">Tin tức</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('contact') }}" title="Giới thiệu">Liên hệ</a>
							</li>
						</ul>
					</div>
				</aside>
				<div class="aside-filter" style="border: 1px solid #cdcdcd;">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>CÓ THỂ BẠN SẼ THÍCH</span></h2>
					</div>
					<!-- <div class="filter-container">
						<div class="filter-container__selected-filter" style="display: none;">
							<div class="filter-container__selected-filter-header clearfix">
								<span class="filter-container__selected-filter-header-title"><i class="fa fa-arrow-left hidden-sm-up"></i>Bạn chọn</span>
							</div>
							<div class="filter-container__selected-filter-list">
								<ul></ul>
							</div>
						</div>
					</div> -->
					<div class="list-product-slidebar">
						<div class="list-item">	
							<div class="thumb-imagtes">	
								<a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">
									<img src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="Túi đựng Laptop BackPack">
								</a>
							</div>
							<div class="product-info-text">
								<h3 class="product-name"><a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">Túi đựng Laptop BackPack</a></h3>
								<div class="price-box clearfix">
									<div class="special-price f-left">
										<span class="price product-price">400.000₫</span>
									</div>											
								</div>
								<!-- <div class="bizweb-product-reviews-badge" data-id="22629671"></div> -->
							</div>
						</div>
						<div class="list-item">	
							<div class="thumb-imagtes">	
								<a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">
									<img src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="Túi đựng Laptop BackPack">
								</a>
							</div>
							<div class="product-info-text">
								<h3 class="product-name"><a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">Túi đựng Laptop BackPack</a></h3>
								<div class="price-box clearfix">
									<div class="special-price f-left">
										<span class="price product-price">400.000₫</span>
									</div>											
								</div>
								<!-- <div class="bizweb-product-reviews-badge" data-id="22629671"></div> -->
							</div>
						</div>
						<div class="list-item">	
							<div class="thumb-imagtes">	
								<a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">
									<img src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="Túi đựng Laptop BackPack">
								</a>
							</div>
							<div class="product-info-text">
								<h3 class="product-name"><a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">Túi đựng Laptop BackPack</a></h3>
								<div class="price-box clearfix">
									<div class="special-price f-left">
										<span class="price product-price">400.000₫</span>
									</div>											
								</div>
								<!-- <div class="bizweb-product-reviews-badge" data-id="22629671"></div> -->
							</div>
						</div>
						<div class="list-item">	
							<div class="thumb-imagtes">	
								<a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">
									<img src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="Túi đựng Laptop BackPack">
								</a>
							</div>
							<div class="product-info-text">
								<h3 class="product-name"><a href="/dam-cong-so-cao-cap-mau-xanh" title="Túi đựng Laptop BackPack">Túi đựng Laptop BackPack</a></h3>
								<div class="price-box clearfix">
									<div class="special-price f-left">
										<span class="price product-price">400.000₫</span>
									</div>											
								</div>
								<!-- <div class="bizweb-product-reviews-badge" data-id="22629671"></div> -->
							</div>
						</div>
					</div>
				</div>
				<aside class="aside-item hidden-767">
					<div class="aside-content">
						<a href="#" title="Thời trang nam">
							<img class="img-responsive center-block" src="{{asset('image/img/aside_banner.jpg')}}" alt="Thời trang nam">
						</a>
					</div>
				</aside>
			</aside>
			<div class="col-lg-9 col-md-9 col-md-push-3 col-lg-push-3 details-product">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5">
						<div class="relative product-image-block no-thum">
							<div class="large-image">
								<div class="large_image_url">
									<img id="zoom_01" class="img-responsive center-block" src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="gang-tay-cam-ung-nam-gttacuna-35-d">
								</div>
								<div class="hidden">
									<div class="item">
										<a href=""></a>
									</div>	
								</div>
							</div>
						</div>
					</div>
					@if(count($showDetail))
                    @foreach($showDetail as $detail)
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 details-pro">
						<h1 class="title-head" itemprop="name">{{$detail -> title }}</h1>
						<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<div class="information">
								<p><span>Thương hiệu:</span> Đang cập nhật</p>
								<p class="inventory_quantity">
									<span>Tình trạng: </span>							
										Còn hàng	
								</p>
							</div>
							<div class="price-box clearfix">
								<div class="special-price">
									<span class="price product-price">
										{{$detail -> price }} VND
									</span>
									<meta itemprop="price" content="150000.0000">
									<meta itemprop="priceCurrency" content="VND">
								</div> <!-- Giá -->	
							</div>
							<!-- <div class="product-summary product_description margin-bottom-15">
								<div class="rte description hidden">
									– Găng tay da cừu thật 100% 
									– Lớp lót bông mềm mại, giữ ấm tốt 
									– Thiết kế ôm sát, có cảm ứng ở đầu găng giúp dễ dàng sử dụng các thiết bị smartphone 
									– Màu sắc: Đen 	
								</div>
							</div> -->
							<div class="form-product">
								<form enctype="multipart/form-data" id="add-to-cart-form" action="/cart/add" method="post" class="form-inline">
									<div class="box-variant clearfix ">
										<input type="hidden" name="variantId" value="50812345" />
									</div>
									<div class="form-group form-groupx">
										<div class="custom custom-btn-number form-control">									
											<span class="quantity-span hidden">Số lượng:</span>
											<!-- <span class="qtyminus" data-field="quantity">-</span> -->
											<input type="text" class="input-text qty" maxlength="3" data-field='quantity' value="1" id="qty" name="quantity">									
											<!-- <span class="qtyplus" data-field="quantity">+</span> -->										
										</div>
										<button type="submit" class="btn btn-lg btn-gray btn-cart btn_buy add_to_cart" title="Mua ngay">
											<span>Mua ngay</span>
										</button>																	
									</div>	
								</form>
								<div class="social-sharing">
									<div class="social-media">
										<label>Chia sẻ: </label>
										<a target="_blank" href="" class="share-facebook" title="Chia sẻ lên Facebook">
											<i class="fa fa-facebook-official"></i>
										</a>
										<a target="_blank" href="//twitter.com/share?url=https://galleria.mysapo.net/gang-tay-cam-ung-nam-gttacuna-35-d" class="share-twitter" title="Chia sẻ lên Twitter">
											<i class="fa fa-twitter"></i>
										</a>
										<a target="_blank" href="" class="share-pinterest" title="Chia sẻ lên pinterest">
											<i class="fa fa-pinterest"></i>
										</a>
										<a target="_blank" href=" class="share-google" title="+1">
											<i class="fa fa-google-plus"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
                  @endif
					<div class="row">
						<div class="col-xs-12 col-lg-12 col-md-12 margin-top-40 margin-bottom-10">
							<div class="product-tab e-tabs space-30">
								<ul class="tabs tabs-title clearfix">
									<li class="tab-link" data-tab="tab-1">
										<h3><span>Thông tin sản phẩm</span></h3>
									</li>																	
									<li class="tab-link" data-tab="tab-2">
										<h3><span>Chính sách</span></h3>
									</li>
								</ul>
								<div id="tab-1" class="tab-content">
									<div class="rte">	
										<p>– Găng tay da cừu thật 100%</p>
										<p>– Lớp lót bông mềm mại, giữ ấm tốt</p>
										<p>– Thiết kế ôm sát, có cảm ứng ở đầu găng giúp dễ dàng sử dụng các thiết bị smartphone</p>
										<p>– Màu sắc: Đen</p>
									</div>
								</div>
								<!-- <div id="tab-2" class="tab-content">
									Nội dung tùy chỉnh viết ở đây		
								</div> -->
							</div>			
						</div>
					</div>
					<div class="related-product products-view-grid">
						<div class="title-home">
							<h3><a href="/gang-tay" title="SẢN PHẨM CÙNG LOẠI">SẢN PHẨM CÙNG LOẠI</a></h3>
						</div>
						<div class="owl-carousel" id="slider_tiny_page_product_cung_loai"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection