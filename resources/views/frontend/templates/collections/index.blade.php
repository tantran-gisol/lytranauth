@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">TẤT CẢ SẢN PHẨM</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Tất cả sản phẩm</span></strong></li>
				</ul>
			</div>
	</section>

	<div class="container">
		<div class="row">
			<aside class="dqdt-sidebar sidebar left left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9 space-30">
				<aside class="aside-item collection-category">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Danh mục</span></h2>
					</div>
					<div class="categories-box">
						<ul class="lv1">
							<li class="nav-item nav-items">
								<a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
							</li>
							<li class="nav-item nav-items active">
								<a href="{{ url('collections') }}" title="Giới thiệu">Sản phẩm</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('news') }}" title="Giới thiệu">Tin tức</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('contact') }}" title="Giới thiệu">Liên hệ</a>
							</li>
						</ul>
					</div>
				</aside>
				<div class="aside-filter" style="border: 1px solid #cdcdcd;">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Bộ lọc</span></h2>
					</div>
					<!-- <div class="filter-container">
						<div class="filter-container__selected-filter" style="display: none;">
							<div class="filter-container__selected-filter-header clearfix">
								<span class="filter-container__selected-filter-header-title"><i class="fa fa-arrow-left hidden-sm-up"></i>Bạn chọn</span>
							</div>
							<div class="filter-container__selected-filter-list">
								<ul></ul>
							</div>
						</div>
					</div> -->
					<aside class="aside-item filter-price">
						<div class="module-title">
							<h2 class="title-head margin-top-0" style="margin-top:10px;margin-left:10px"><span>Giá sản phẩm</span></h2>
						</div>
						<div class="aside-content filter-group">
							<ul style="list-style: none;padding-left: 1rem;">
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-duoi-100-000d">
											<input type="checkbox" id="filter-duoi-100-000d" onchange="toggleFilter(this);" data-group="Khoảng giá" data-field="price_min" data-text="Dưới 100.000đ" value="(<100000)" data-operator="OR">
											<i class="fa"></i>Giá dưới 100.000đ
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-100-000d-200-000d">
											<input type="checkbox" id="filter-100-000d-200-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="100.000đ - 200.000đ" value="(>=100000 AND <200000)" data-operator="OR">
											<i class="fa"></i>100.000đ - 200.000đ							
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-200-000d-300-000d">
											<input type="checkbox" id="filter-200-000d-300-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="200.000đ - 300.000đ" value="(>=200000 AND <300000)" data-operator="OR">
											<i class="fa"></i>200.000đ - 300.000đ							
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-300-000d-500-000d">
											<input type="checkbox" id="filter-300-000d-500-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="300.000đ - 500.000đ" value="(>=300000 AND <500000)" data-operator="OR">
											<i class="fa"></i>300.000đ - 500.000đ							
										</label>
									</span>
								</li>						
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-500-000d-1-000-000d">
											<input type="checkbox" id="filter-500-000d-1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="500.000đ - 1.000.000đ" value="(>500000 AND <1000000)" data-operator="OR">
											<i class="fa"></i>500.000đ - 1.000.000đ							
										</label>
									</span>
								</li>	
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-tren1-000-000d">
											<input type="checkbox" id="filter-tren1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="Trên 1.000.000đ" value="(>1000000)" data-operator="OR">
											<i class="fa"></i>Giá trên 1.000.000đ
										</label>
									</span>
								</li>
							</ul>
						</div>
					</aside>

					<aside class="aside-item filter-type">
						<div class="module-title">
							<h2 class="title-head margin-top-0" style="margin-top:10px;margin-left:10px"><span>Loại</span></h2>
						</div>
						<div class="aside-content filter-group">
							<ul style="list-style: none;padding-left: 1rem;">	
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-dung-cu-tap">
											<input type="checkbox" id="filter-dung-cu-tap" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="Dụng cụ tập" value="(Dụng cụ tập)" data-operator="OR">
											<i class="fa"></i>Dụng cụ tập
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-gam">
											<input type="checkbox" id="filter-gam" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="Gấm" value="(Gấm)" data-operator="OR">
											<i class="fa"></i>Gấm
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-giay-vai">
											<input type="checkbox" id="filter-giay-vai" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="Giày vải" value="(Giày vải)" data-operator="OR">
											<i class="fa"></i>Giày vải
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-lo-vi-song">
											<input type="checkbox" id="filter-lo-vi-song" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="Lò vi sóng" value="(Lò vi sóng)" data-operator="OR">
											<i class="fa"></i>Lò vi sóng
										</label>
									</span>
								</li>			
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-loai-test">
											<input type="checkbox" id="filter-loai-test" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="loại test" value="(loại test)" data-operator="OR">
											<i class="fa"></i>loại test
										</label>
									</span>
								</li>
								<li class="filter-item filter-item--check-box filter-item--green">
									<span>
										<label for="filter-test">
											<input type="checkbox" id="filter-test" onchange="toggleFilter(this)"  data-group="Loại" data-field="product_type" data-text="test" value="(test)" data-operator="OR">
											<i class="fa"></i>test
										</label>
									</span>
								</li>			
							</ul>
						</div>
					</aside>
				</div>
				<aside class="aside-item hidden-767">
					<div class="aside-content">
						<a href="#" title="Thời trang nam">
							<img class="img-responsive center-block" src="{{asset('image/img/aside_banner.jpg')}}" alt="Thời trang nam">
						</a>
					</div>
				</aside>
			</aside>

			<section class="main_container collection col-lg-9 col-md-9 col-md-push-3 col-lg-push-3">
				<div class="category-products products">
					<div class="sortPagiBar">
						<div class="row">
							<div class="col-xs-5 col-md-6 col-sm-6">		
								<div class="view">
					        		<a href="#" class="icon-grid"></a>
					     			<a href="#" class="icon-list"></a>        
					    		</div>
							</div>
							<div class="col-xs-12 col-md-6 col-sm-6 text-xs-left text-sm-right">
								<div id="sort-by">
									<label class="left">Sắp xếp: </label>
								</div>
							</div>
						</div>
					</div>
					<section class="products-view products-view-grid">
						<ul class="">
							@if(count($topPosts))
					        @foreach($topPosts as $topPosts)
							    <li>
							        <div class="itemIMG">
							            @if($topPosts->featureImage)
					            		<img class="img-responsive center-block img-auto" src="{{ Helper::getMediaUrl($topPosts->featureImage, 'small') }}" width="100" height="100" />
					         			@else
					            		<img class="img-responsive center-block img-auto" src="{{ Helper::getDefaultCover($topPosts) }}" width="100" height="100" />
					          			@endif
							            <div class="addCART"><a href="#"><i class="fa fa-eye"></i></a></div>
							        </div>
							        <div class="itemTitle"><a href="#">{{$topPosts -> title }}</a>
							 			<span>{{$topPosts -> price }} VND</span>
							            <!-- <p class="des">Detailed item information</p> -->
							            <p>{{ strip_tags($topPosts -> content) }}</p>
							        </div>
							    </li>
							@endforeach
					    	@endif
						</ul>
					</section>
				</div>
			</section>


		</div>
	</div>

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection