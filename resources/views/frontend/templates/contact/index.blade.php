@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">Liên hệ</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Liên hệ</span></strong></li>
				</ul>
			</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-sm-12 margin-bottom-30 margin-top-20">
				<div class="box-maps">
					<div class="iFrameMap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6590.100240631746!2d106.5950245838679!3d10.849749002814612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752a45f5ef8153%3A0x6e438ec2db75bcd7!2zMjYgVMOieSBC4bqvYyBMw6JuLCBCw6AgxJBp4buDbSwgSMOzYyBNw7RuLCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1651653317596!5m2!1svi!2s" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<div class="col-sm-4" style="margin-top:20px">
				<h1 class="title-head page_title margin-bottom-5">Liên hệ</h1>
				<p></p>
				<div class="contact-box-info clearfix margin-bottom-30">
					<ul class="list-menu">
						<li>Địa chỉ: <span>26/1 Tây Lân - Bà Điểm - Huyện Hóc Môn - TP Hồ Chí Minh</span></li>
						<li>Điện thoại: 					
							<a href="tel:19006750">0977722146</a>						
						</li>
						<li>Email: 						
							<a href="mailto:support@sapo.vn">diemlytt@gmail.com</a>
						</li>
						<li>Website: 						
							<a href="mailto:support@sapo.vn">http://lytranauthentic.com</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-8" style="margin-top:20px">
				<div id="login">
					<h2 class="title-head"><span>Gửi tin nhắn cho chúng tôi</span></h2>
					<form accept-charset="utf-8" action="/contact" id="contact" method="post">
						<!-- <p id="errorFills" style="margin-bottom:10px; color: red;"></p> -->
						<div id="emtry_contact" class="form-signup form_contact clearfix">
							<div class="row row-8Gutter">
								<div class="col-md-6">
									<fieldset class="form-group">							
										<input type="text" placeholder="Họ tên*" name="contact[name]" id="name" class="form-control  form-control-lg" required>
									</fieldset>
								</div>
								<div class="col-md-6">
									<fieldset class="form-group">							
										<input type="email" placeholder="Email*" name="contact[email]" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$" data-validation="email"  id="email" class="form-control form-control-lg" required>
									</fieldset>
								</div>
								<div class="col-md-12">
									<fieldset class="form-group">						
										<input type="text" placeholder="Điện thoại*" name="contact[phone]"  class="form-control form-control-lg" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
									</fieldset>
								</div>
							</div>
							<fieldset class="form-group">							
								<textarea name="contact[body]" placeholder="Nhập nội dung*" id="comment" class="form-control form-control-lg" rows="6" Required></textarea>
							</fieldset>
							<div>
								<button type="submit" class="btn btn-primary" style="padding:5px 40px;text-transform: inherit;">Gửi liên hệ</button>
							</div> 
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection