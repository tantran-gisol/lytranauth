@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main feature list page content -->

		<div class="main-feature-list-page">
			<div class="container">
				
				<div class="feature-list-page-header row">
					<div class="col-lg-6 col-md-6 col-12">
						<div class="feature-list-page-header-img" style="background-image: url('{{ asset('image/img-1.jpg') }}');">
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-12">
						<h3>Tin tức</h3>
					</div>
				</div>

				<div class="line"></div>

				<div class="row">
					
					<!-- left feature list page content -->

					<div class="left-feature-list-page col-lg-8 col-md-8 col-12">
						<div class="feature-list-page row">
							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/django-img-demo.jpg') }}');">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/speedfight-img-demo.jpg') }}');">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/citystar-img-demo.jpg') }}');">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/adv-200.jpg') }}');">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/vx-1.jpg') }}');">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/adiva-logo.png') }}'); background-repeat: no-repeat; background-size: contain !important;">
									</div>
								</a>
							</div>

							<div class="feature-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="feature-list-page-item-content" style="background: url('{{ asset('image/peugeot-logo.png') }}'); background-repeat: no-repeat; background-size: contain !important;">
									</div>
								</a>
							</div>
						</div>
					</div>

					<!--x-- left feature list page content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main feature list page content --x-->

	</section>
@endsection