@extends('frontend.layouts.app')
@section('content')
    <section class="awe-section-1">
        <div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
    </section>

    <section class="bread-crumb">
        <div class="breadcrumb-container">
            <div class="title-page">ĐĂNG NHẬP TÀI KHOẢN</div>
                <ul class="breadcrumb">                 
                    <li class="home">
                        <a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>   
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </li>
                    <li><strong ><span itemprop="title"> Đăng nhập tài khoản</span></strong></li>
                </ul>
            </div>
    </section>

    <div class="container">
        <h1 class="title-head"><span>Đăng nhập tài khoản</span></h1>
        <div class="row">
            <div class="col-lg-6">
                <div class="page-login margin-bottom-30">
                    <div id="login">
                        <span>Nếu bạn đã có tài khoản, đăng nhập tại đây.</span>
                        <form accept-charset="utf-8" action="/account/login" id="customer_login" method="post">
                            <input name="FormType" type="hidden" value="customer_login" />
                            <input name="utf8" type="hidden" value="true" />
                            <div class="form-signup clearfix">
                                <fieldset class="form-group">
                                    <label>Email: </label>
                                    <input type="email" class="form-control form-control-lg" value="" name="email" id="customer_email" placeholder="Email">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Mật khẩu: </label>
                                    <input type="password" class="form-control form-control-lg" value="" name="password" id="customer_password" placeholder="Mật khẩu">
                                </fieldset>
                                <div class="pull-xs-left" style="margin-top: 25px;">
                                    <input class="btn-color btn btn-lg btn-style btn-dark"  type="submit" value="Đăng nhập" />
                                    <a href="/account/register" class="btn-link-style btn-register" style="margin-left: 20px;">Đăng ký</a>
                                </div>
                            </div>
                            <input name="returnUrl" type="hidden" value="/account"/></form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div id="recover-password"  class="form-signup">
                        <span>
                            Bạn quên mật khẩu? Nhập địa chỉ email để lấy lại mật khẩu qua email.
                        </span>                 
                        <form accept-charset="utf-8" action="/account/recover" id="recover_customer_password" method="post">
                            <input name="FormType" type="hidden" value="recover_customer_password" />
                            <input name="utf8" type="hidden" value="true" />
                            <div class="form-signup clearfix">
                                <fieldset class="form-group">
                                    <label>Email: </label>
                                    <input type="email" class="form-control form-control-lg" value="" name="Email" id="recover-email" placeholder="Email">
                                </fieldset>
                            </div>
                            <div class="action_bottom">
                                <input class="btn-color btn btn-lg btn-style btn-dark" style="margin-top: 25px; margin-bottom: 25px;" type="submit" value="Lấy lại mật khẩu" />
                            </div>
                            <input name="returnUrl" type="hidden" value="/account"/>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;margin-top: 5%;">
                <div class="col-md-12">
                    <h3><b>Đăng nhập bằng Facebook hoặc Google</b></h3>
                    <div style="display: inline-block;margin-right: 15px;">
                        <a href="javascript:void(0)" class="social-login--facebook" onclick="loginFacebook()">
                            <img width="129px" height="37px" alt="facebook-login-button" src="//bizweb.dktcdn.net/assets/admin/images/login/fb-btn.svg">
                        </a>
                    </div>
                     <div style="display: inline-block">
                        <a href="javascript:void(0)" class="social-login--google" onclick="loginGoogle()">
                            <img width="129px" height="37px" alt="google-login-button" src="//bizweb.dktcdn.net/assets/admin/images/login/gp-btn.svg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-newsletter" style="margin-top:20px">
        <div class="halu-card-hori">
            <div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
            <div class="col-item">
                <div class="box-text">
                    <h3>Đăng ký nhận tin</h3>
                    <p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
                    <form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
                        <input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
                        <button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>
@endsection          