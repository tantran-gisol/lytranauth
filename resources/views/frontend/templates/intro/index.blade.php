@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">GIỚI THIỆU</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Giới thiệu</span></strong></li>
				</ul>
			</div>
	</section>

	<div class="container">
		<div class="row">
			<aside class="dqdt-sidebar sidebar left left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9 space-30">
				<aside class="aside-item collection-category">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Danh mục</span></h2>
					</div>
					<div class="categories-box">
						<ul class="lv1">
							<li class="nav-item nav-items">
								<a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item nav-items active">
								<a href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('collections') }}" title="Giới thiệu">Sản phẩm</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('news') }}" title="Giới thiệu">Tin tức</a>
							</li>
							<li class="nav-item nav-items">
								<a href="{{ url('contact') }}" title="Giới thiệu">Liên hệ</a>
							</li>
						</ul>
					</div>
				</aside>				
			</aside>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-lg-push-3 col-md-push-3">
				<div class="content-page rte">
					<h3><strong>Đồ Da Galleria chuyên cung cấp các mặt hàng thời trang cao cấp cho nam.</strong></h3>
					<ul>
						<li>Đồ da&nbsp;tiền thân là xưởng sản xuất da được hình thành từ năm 1991 tại khu tập thể thuộc da Thái Bình.</li>
						<li>Trải qua nhiều năm xây dựng và phát triển, Công ty Tổng hợp đồ da Galleria đã trở thành cái tên quen thuộc, được nhiều người biết đến tại Hà Nội.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection