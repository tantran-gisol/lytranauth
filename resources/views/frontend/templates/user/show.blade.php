@extends('frontend.layouts.app')
@section('content')
	<section class="main">
		<!-- main category content -->

		<section class="main-category-content">
			<div class="container">
				<div class="d-flex">
					@if($user->avatar)
						<div>
							<img src="{{ Helper::getUserAvatarUrl($user->avatar) }}">
						</div>
					@else
						<div>
							<img src="{{ asset('image/no-img.png') }}">
						</div>
					@endif
					<div class="ml-3 align-self-center">
						<h3>{{ $user->name }}</h3>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="category-content row">
					
					<!-- left category content -->

					<div class="category-content-item-list col-lg-8 col-md-8 col-12">
						@if(count($posts))
							@foreach($posts as $post)
								<div class="category-content-item d-flex">
									<a href="{{ route('frontend.post.show', $post->slug) }}"><div class="category-content-item-img" style="width: 150px; height: 150px;">
										@if($post->featureImage)
											<img src="{{ Helper::getMediaUrl($post->featureImage, 'small') }}" />
										@else
											<img src="{{ Helper::getDefaultCover($post) }}" />
										@endif
									</div></a>

									<!-- <div class="row" style="margin-left: 0;"> -->

									<div class="category-item-text" style="margin-left: 0; font-size: 20px;">
										<div class="category-content-item-title font-weight-bold col-12">
											<a href="{{ route('frontend.post.show', $post->slug) }}">
												@if(!empty($post->vote_id) && array_sum($post->getVoteCountData()) > 0)
													<span class="vote-label">VOTE</span>
												@endif
												<span>{{ $post->title }}</span>
											</a>
										</div>
										@php
											$summary = Helper::getPostSummary($post->content);
										@endphp
										<div class="category-content-item-content col-12">
											@if(!empty($summary))
												{!! $summary !!}
											@else
												{!! strip_tags($post->content) !!}
											@endif
										</div>
										<div class="category-content-item-post col-12 d-none d-md-block">
											<div class="post-man-avatar d-flex">
												<a href="{{ route('frontend.user.show', $post->user) }}">@if($post->user->avatar)
														<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}">
													@else
														<img src="{{ asset('image/no-img.png') }}">
													@endif</a>
												<div style="padding: 8px 0; line-height: 1rem;">
													<div for="" class="post-man-name col-12">
														<a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a>
													</div>
													<div for="" class="post-man-date col-12">
														{{ date('Y-m-d', strtotime($post->created_at)) }}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						<!-- pagination -->
						{{ $posts->appends(request()->except('user_id'))->links('frontend.parts.pagination') }}
						<!--x-- pagination -->
					</div>

					<!--x-- left category content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</section>
		<!-- main category content -->
	</section>
@endsection