<body class="body_index">
  <header class="header" style="background-color: black;">
    <!-- <div class="search active">
      <div class="close-search">
        <i class="fa fa-close"></i>
      </div>
      <div class="header_search search_form">
        <form class="input-group search-bar search_form" action="/search" method="get" role="search">   
          <input type="search" name="query" value="" placeholder="Nhập từ khóa bạn muốn tìm kiếm ... " class="input-group-field st-default-search-input search-text" autocomplete="off">
          <button>Tìm kiếm</button>
        </form>
      </div> 
    </div> -->
    <div class="middle-header bg-cover">
      <div class="container">
        <div class="header-main">
          <div class="d-flex-desktop">
            <div class="flex align-center">
              <div class="icon-menu-mobile hidden-lg hidden-md">
                <i class="fa fa-bars" aria-hidden="true"></i>
              </div>
              <div class="icon-click-search">
                <img src="{{asset('image/img/icon-search.png')}}" alt="search" width="20" height="20">
              </div>
            </div>
            <div class="logo">
              <a href="/" class="logo-wrapper " title="galleria"> 
                <img class="img-auto" src="{{asset('image/img/logo.png')}}"  alt="galleria" width="50" height="30">
              </a>              
            </div>
            <div class="right">
              <a href="{{ url('login') }}" class="icon-acc">
                <img class="w-20" src="{{ asset('image/img/ic-account.png')}}" alt="login" height="20" width="20">  
              </a>
              <div class="mini-cart text-xs-center">
                <div class="heading-cart">
                  <a href="{{ url('cart') }}"  title="Giỏ hàng">
                    <img class="w-20" src="{{asset('image/img/ic-cart.png')}}" alt="giỏ hàng" height="20" width="20">
                    <span class="cartCount count_item_pr" id="cart-total">10</span>
                  </a>
                </div>
                <div class="top-cart-content hidden-sm hidden-xs">
                  <ul id="cart-sidebar" class="mini-products-list count_li">
                    <li class="list-item">
                      <ul></ul>
                    </li>
                    <li class="action">
                      <ul>
                        <li class="li-fix-1">
                          <div class="top-subtotal">
                            Tổng tiền thanh toán: 
                            <span class="price"></span>
                          </div>
                        </li>
                        <li class="li-fix-2" style="">
                          <div class="actions">
                            <a href="/cart" class="btn btn-primary"  title="Giỏ hàng">
                              <span>Giỏ hàng</span>
                            </a>
                            <a href="/checkout" class="btn btn-checkout btn-gray" title="Thanh toán ">
                              <span>Thanh toán</span>
                            </a>
                          </div>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>  
            </div>
          </div>
        </div>
        <div class="main-nav hidden-sm hidden-xs">
          <nav>
            <ul class="nav nav-mobile">
              <li class="hidden-sm hidden-xs nav-item active">
                <a class="nav-link" href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item ">
                <a class="nav-link" href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item  has-mega has-dropdown">
                <a href="{{ url('collections') }}" title="Sản phẩm" class="nav-link">Sản phẩm <i class="fa fa-angle-down" data-toggle="dropdown"></i></a>
                <div class="mega-content">
                  <div class="level0-wrapper2">
                    <div class="nav-block nav-block-center">
                      <ul class="level0">
                        <li class="level1 parent item ">
                          <h2 class="h4"><a href="/" title="Sản phẩm mới"><span>Sản phẩm mới</span></a></h2> 
                          <ul class="level1">
                            <li class="level2"> <a href="/" title="Túi xách"><span>Túi xách</span></a></li> 
                            <li class="level2"> <a href="/" title="Giày da"><span>Giày da</span></a></li>
                          </ul>
                        </li>
                        <li class="level1 item">
                          <h2 class="h4"><a href="/" title="Thắt lưng"><span>Thắt lưng</span></a></h2>
                        </li>
                      </ul>
                    </div>
                  </div>
                <div class="banner-mega-content">
                  <div class="col-md-6">
                    <a href="#" title="mega-menu">
                      <img class="basic img-responsive lazied" data-src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images1.png?1650527032568" src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images1.png?1650527032568" alt="Mega menu 1" data-lazied="IMG">
                    </a>
                  </div>
                  <div class="col-md-6">
                    <a href="#" title="mega-menu">
                      <img class="basic img-responsive lazied" data-src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images2.png?1650527032568" src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images2.png?1650527032568" alt="Mega menu 1" data-lazied="IMG">
                    </a>
                  </div>
                </div>
              </div>
            </li>
              <li class="hidden-sm hidden-xs nav-item  has-dropdown">
                <a href="{{ url('news') }}" title="Tin tức" class="nav-link">Tin tức</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item ">
                <a class="nav-link" href="{{ url('contact') }}" title="Liên hệ">Liên hệ</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
</body>
@if(isset($post))
	@php
		$social_share_option = (array_key_exists('general_social_share_option', $settings)) ? json_decode($settings['general_social_share_option'], true) : [];
	@endphp
	<section class="article-page" style="margin-top:15%">
		<div class="article-details-img {{($post->cover_image_style) ? $post->cover_image_style : ''}}">
			<div class="resize-img">
			@if($post->featureImage)
				<img class="img-resize" src="{{ Helper::getMediaUrl($post->featureImage, 'original') }}" class="d-block w-100">
			@elseif(Helper::getDefaultCover($post))
				<img class="img-resize" src="{{ Helper::getDefaultCover($post) }}" class="d-block w-100">
			@endif
			@if($post->cover_via_text)
				<a href="{{ (!empty($post->cover_via_href) && substr($post->cover_via_href, 0, 4) != 'http') ? 'http://'.$post->cover_via_href : $post->cover_via_href }}" target="_blank" class="cover-text">{{ $post->cover_via_text }}</a>
			@endif
			</div>
		</div>
		<div class="container-article-page">
			<div class="article-details-page-item">
				<div class="article-details-page-item-header">
					<h1>
						@if(!empty($post->vote_id) && array_sum($post->getVoteCountData()) > 0)
							<span class="voted">VOTE</span>
						@endif
						{{ $post->title }}
					</h1>
				</div>

				<div class="article-details-page-item-post col-12">
					<div class="row">
						<div class="post-man-avatar d-flex">
							<a href="{{ route('frontend.user.show', $post->user) }}">
								@if($post->user->avatar)
									<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}">
								@else
									<img src="{{ asset('image/no-img.png') }}">
								@endif
							</a>
							<div style="padding: 8px 0; line-height: 1rem;">
								<div for="" class="post-man-name col-12" style="font-size: 12px;">
									<a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a>
								</div>
								<div for="" class="post-man-date col-12" style="font-size: 12px;">
									{{ date('Y-m-d', strtotime($post->created_at)) }}
								</div>
							</div>
						</div>
						<a href="#" class="button-share-post d-none" data-toggle="modal" data-target="#modelSharePost-{{ $post->id }}"><i class="fas fa-share"></i> 共有</a>
					</div>
				</div>

				<div class="article-details-page-tags col-12 row">
					@if(count($post->tags))
						@foreach($post->tags as $tag)
							<div class="article-details-page-tags-item">
								<a href="{{ route('frontend.tag.show', $tag) }}"><i class="fas fa-tag"></i>{{ $tag->name }}</a>
							</div>
						@endforeach
					@endif
				</div>

				@php
					$summary = Helper::getPostSummary($post->content);
				@endphp
				<div class="summary-content {{ (!empty($summary)) ? 'line-bottom' : '' }}">
					@if(!empty($summary))
						<p>Summary</p>
					@endif
					<div class="d-flex justify-content-between">
						<div style="width:95%;">{!! $summary !!}</div>
						
						<div class="sns-share d-flex">
							@if(is_array($social_share_option) && in_array('facebook', $social_share_option))
								<a href="https://facebook.com/sharer/sharer.php?u={{ route('frontend.post.show', $post->slug) }}" target="_blank" class="sns-share-fb text-center"><i class="fab fa-facebook-f"></i></a>
							@endif
							@if(is_array($social_share_option) && in_array('twitter', $social_share_option))
								<a href="https://twitter.com/share?url={{ route('frontend.post.show', $post->slug) }}" target="_blank" class="sns-share-twt text-center"><i class="fab fa-twitter"></i></a>
							@endif
						</div>
					</div>
				</div>

				<div class="clear" style="clear: both;"></div>

				<div id="article-content-{{ $post->id }}" class="article-content post-content ck-content">
					@php
						$content = Helper::generatePostContent($post);
					@endphp
					{!! $content !!}
				</div>

				<!-- Main Album -->
				@if(isset($mainMedias) && count($mainMedias))
					<div class="main-album">
						<h5 class="heading"><i class="fa fa-picture-o"></i> Media ({{ count($mainMedias) }})</h5>
					  <div class="list-medias">
					  	@foreach($mainMedias as $media)
						    <div class="media-item">
						      <a href="{{ route('frontend.post.album', ['slug' => $post->slug, 'image' => $media['id']])}}"><img src="{{ $media['url'] }}" /></a>
						    </div>
						  @endforeach
					  </div>
					</div>
				@endif
				<!--x- Main Album -->
				
				<div class="article-details-page-item-post col-12 d-none d-md-block row" style="margin: 2rem 0 1.5rem 0; padding: 0;">
					<div class="post-man-avatar d-flex">
						<a href="{{ route('frontend.user.show', $post->user) }}">
							@if($post->user->avatar)
								<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}" />
							@else
								<img src="{{ asset('image/no-img.png') }}" />
							@endif
						</a>
						<div style="padding: 8px 0; line-height: 1rem;">
							<div for="" class="post-man-name col-12" style="font-size: 12px;">
								<a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a>
							</div>
							<div for="" class="post-man-date col-12" style="font-size: 12px;">
								{{ date('Y-m-d', strtotime($post->created_at)) }}
							</div>
						</div>
					</div>
				</div>
				
				<div class="article-details-page-tags col-12 row">
					@if(count($post->tags))
						@foreach($post->tags as $tag)
							<div class="article-details-page-tags-item">
								<a href="{{ route('frontend.tag.show', $tag) }}"><i class="fas fa-tag"></i>{{ $tag->name }}</a>
							</div>
						@endforeach
					@endif
				</div>

				<div class="sns-share-end col-12 row">
					@if(is_array($social_share_option) && in_array('facebook', $social_share_option))
						<a href="https://facebook.com/sharer/sharer.php?u={{ route('frontend.post.show', $post->slug) }}" target="_blank" class="sns-share-fb text-center"><i class="fab fa-facebook-f"></i>Facebook</a>
					@endif
					@if(is_array($social_share_option) && in_array('twitter', $social_share_option))
						<a href="https://twitter.com/share?url={{ route('frontend.post.show', $post->slug) }}" target="_blank" class="sns-share-twt text-center"><i class="fab fa-twitter"></i>Twitter</a>
					@endif
				</div>

				<!-- related posts -->
				@include('frontend.parts.related_posts')
				<!--x-- related posts -->
			</div>
		</div>
	</section>
	<div class="content-space">
		<a href="#main-header" class="scroll-top">
			<i class="fas fa-chevron-up"></i>
		</a>
	</div>
	<!-- modal -->
	<div class="modal fade" id="modelSharePost-{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLongTitle">埋め込みウィジェット</h5>
	        <!--<h5 class="modal-title" id="exampleModalLongTitle">Embedded Widget</h5>-->
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
            <!--<label for="recipient-name" class="col-form-label">Embed link:</label>-->
            <label for="recipient-name" class="col-form-label">埋め込みリンク:</label>
            <div class="d-flex">
              <input type="text" id="embed-link-{{ $post->id }}" class="form-control" value="{{ route('frontend.post.show', $post->slug) }}" readonly="">
              <button type="button" class="btn btn-primary btn-copy-text" data-input="embed-link-{{ $post->id }}" style="width:126px;margin-left: 10px">コピー</button>
            </div>
          </div>
          <div class="form-group">
            <!--<label for="message-text" class="col-form-label">Embed code:</label>-->
            <label for="message-text" class="col-form-label">埋め込みコード:</label>
            <div class="d-flex">
              <textarea class="form-control" id="embed-code-{{ $post->id }}" style="height: 120px" readonly=""><iframe src="{{ route('frontend.post.show', $post->slug) }}?view=embed" frameborder="0" style="width: 100%; height: 500px;"></iframe></textarea>
              <button type="button" class="btn btn-primary btn-copy-text" data-input="embed-code-{{ $post->id }}" style="width: 126px;height:37px;margin-left: 10px;">コピー</button>
            </div>
          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じ</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!--x- modal -->
@endif