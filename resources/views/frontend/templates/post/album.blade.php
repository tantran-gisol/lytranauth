@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main article details page content -->

		<div class="main-article-details-page-content">
			<section class="article-page">
				<div class="container-article-page">
					<div class="article-details-page-item">
						<!-- Main Album -->
						@if(isset($mainMedias) && count($mainMedias))
							<div class="post-main-flexslider flexslider">
			          <ul class="slides">
			          	@foreach($mainMedias as $media)
			          		<li data-thumb="{{ Helper::getMediaUrlById($media['id'], 'thumbnail') }}" {{ (request()->get('image') == $media['id']) ? 'id=slide-focus' : '' }}>
				  	    	    <img src="{{ $media['url'] }}" />
				  	    		</li>
								  @endforeach
			          </ul>
			          <a href="{{ route('frontend.post.show', $post->slug ) }}" class="album-content-link"><i class="fa fa-link" aria-hidden="true"></i> Quay lại nội dung bài viết</a>
			        </div>
						@endif
						<!--x- Main Album -->
					</div>
				</div>
			</section>
		</div>
		<!--x-- main article details page content --x-->
	</section>

@endsection