@extends('frontend.layouts.app')
@section('content')
	<section class="awe-section-1">
		<div class="halud-slide-show bg-cover hidden-sm hidden-xs" style="background-image: url(image/img/bg-header.jpg);height:170px;"></div>
	</section>

	<section class="bread-crumb">
		<div class="breadcrumb-container">
			<div class="title-page">GIỎ HÀNG</div>
				<ul class="breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/" title="Trang chủ" ><span itemprop="title">Trang chủ</span></a>	
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</li>
					<li><strong ><span itemprop="title">Giỏ hàng</span></strong></li>
				</ul>
			</div>
	</section>
	<section class="main-cart-page main-container col1-layout">
		<div class="main container hidden-xs hidden-sm">
			<div class="col-main cart_desktop_page cart-page">
				<div class="cart page_cart hidden-xs">
					<form action="/cart" method="post" novalidate="" class="margin-bottom-0">
						<div class="bg-scroll">
							<div class="cart-thead">
								<div style="width: 18%" class="a-center">Ảnh sản phẩm</div>
								<div style="width: 32%" class="a-center">Tên sản phẩm</div>
								<div style="width: 17%" class="a-center"><span class="nobr">Đơn giá</span></div>
								<div style="width: 14%" class="a-center">Số lượng</div>
								<div style="width: 14%" class="a-center">Thành tiền</div>
								<div style="width: 5%" class="a-center">Xoá</div>
							</div>
							<div class="cart-tbody">
								<div class="item-cart productid-50812350">
									<div style="width: 18%" class="image">
										<a class="product-image" title="Dây đồng hồ cá sấu DDHTA-CF-MM" href="/day-dong-ho-ca-sau-ddhta-cf-mm">
											<img width="75" height="auto" alt="Dây đồng hồ cá sấu DDHTA-CF-MM" src="{{asset('image/img/day-dong-ho-ca-sau-ddh200-cf.jpg')}}">
										</a>
									</div>
									<div style="width: 32%" class="a-center">
										<h3 class="product-name"><a class="text2line" href="/day-dong-ho-ca-sau-ddhta-cf-mm" title="Dây đồng hồ cá sấu DDHTA-CF-MM">Dây đồng hồ cá sấu DDHTA-CF-MM</a></h3>
										<!-- <span class="variant-title hidden">Default Title</span> -->
									</div>
									<div style="width: 17%" class="a-center">
										<span class="item-price">
											<span class="price">250.000₫</span>
										</span>
									</div>
									<div style="width: 14%" class="a-center">
										<div class="input_qty_pr">
											<input class="variantID" type="hidden" name="variantId" value="50812350">
											<input type="text" maxlength="12" readonly="" min="0" class="check_number_here input-text number-sidebar input_pop input_pop qtyItem50812350" id="qtyItem50812350" name="Lines" size="4" value="1">
<!-- 											<button class="reduced_pop items-count btn-minus" type="button">-</button>
											<button class="increase_pop items-count btn-plus" type="button">+</button> -->
										</div>
									</div>
								</div>
								<div style="width: 14%" class="a-center">
									<span class="cart-price">
										<!-- <span class="price">250.000₫</span> -->
									</span>
								</div>
								<div style="width: 5%" class="a-center">
									<a class="remove-itemx remove-item-cart" title="Xóa" href="javascript:;">
										<!-- <span><i class="fa fa-trash-o"></i></span> -->
									</a>
								</div>
							</div>
						</div>
					</form>
					<div class="row margin-top-20  margin-bottom-40">
						<div class="col-lg-7 col-md-7">
							<div class="form-cart-button">
								<div class="">
									<a href="/" class="form-cart-continue">Tiếp tục mua hàng</a>
								</div>
							</div>
						</div>
						<div class="col-lg-5 col-md-5 bg_cart shopping-cart-table-total">
							<div class="table-total">
								<table class="table">
									<tbody>
										<!-- <tr class="hidden">
											<td>Tiền vận chuyển</td>
											<td class="txt-right a-right">Tính khi thanh toán</td>
										</tr> -->
										<tr>
											<td class="total-text">Tổng tiền thanh toán</td>
											<td class="1 txt-right totals_price price_end a-right">250.000₫</td>
										</tr>
									</tbody>
								</table>
							</div>
							<a onclick="" class="btn-checkout-cart" title="Thanh toán">Tiến hành thanh toán</a>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>
	

	<div class="wrap-newsletter" style="margin-top:20px">
    	<div class="halu-card-hori">
      		<div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      		<div class="col-item">
        		<div class="box-text">
          			<h3>Đăng ký nhận tin</h3>
          			<p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          			<form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            			<input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            			<button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          			</form> 
        		</div>
      		</div>
    	</div>
 	</div>
@endsection