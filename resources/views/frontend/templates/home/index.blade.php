@extends('frontend.layouts.app')
@section('content')
  <section class="awe-section-1">
    <div class="halud-slide-show bg-cover hidden-sm hidden-xs">
      <div class="item" data-images="<?php echo url('/'); ?>/image/img/slider_1.png">
        <h3>HÃY BIẾN NÓ CỦA RIÊNG BẠN</h3>
        <p>Dù nhu cầu hoặc ngân sách của bạn là gì, chúng tôi có một chiếc túi đẹp cho bạn</p>  
      </div>
      <div class="item" data-images="<?php echo url('/'); ?>/image/img/slider_2.png">
        <h3>PHÙ HỢP VỚI NHIỀU LỨA TUỔI</h3>
        <p>Sứ giả da nguyên tấm của chúng tôi bền, đẹp</p>  
      </div>
      <div class="item" data-images="<?php echo url('/'); ?>/image/img/slider_3.jpg">
        <h3>SẴN SÀNG CHO DU LỊCH</h3>
        <p>Ba lô của chúng tôi sẽ thoải mái và an toàn chứa tất cả các thiết bị của bạn</p> 
      </div>  
    </div>
  </section>

  <section class="awe-section-2"> 
    <div class="bg-gray-700 hidden-tns-nav">
      <div class="container">
        <div class="title-text text-center">
          <h3>DANH MỤC SẢN PHẨM</h3>
          <p>Tất cả sản phẩm đều được bảo hành 12 Tháng </p>    
        </div>
        <div class="tns-outer" id="id_tiny_1-ow">
          <div class="tns-controls">
            <button type="button" data-controls="prev" tabindex="-1" aria-controls="id_tiny_1">prev</button>
            <button type="button" data-controls="next" tabindex="-1" aria-controls="id_tiny_1">next</button>
          </div>
          <div id="id_tiny_1-mw" class="tns-ovh">
            <div class="tns-inner" id="id_tiny_1-iw">
              <div class="d-flex-owl height-162  tns-slider tns-carousel tns-subpixel tns-no-calc tns-horizontal" data-xss-items="2" data-xs-items="3" data-sm-items="4" data-md-items="5" data-lg-items="5">
                <div class="space-item-tsn tns-item tns-slide-active" id="id_tiny_1-item0">
                @if(count($category))
                  @foreach($category as $category) 
                  <div class="halu-item-card text-center">
                    @if($category->featured_image)
                    <a class="banner border-radius-50" href="collections/{{ $category->id }}" title="">
                      <picture>
                        <img class="img-responsive img-full basic" src="{{ $category->featured_image }}" alt="alt" width="100" height="100">
                      </picture>
                    </a>
                    @endif
                    <a href="collections/{{ $category->id }}" title="{{ $category->name }}" style="text-decoration:none">
                      <h3>{{ $category->name }}</h3>
                    </a>
                  </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="awe-section-3"> 
    <div class="halu-card-hori">
      <div class="col-item flex-end">
        <div>
          <h3>TINH XẢO TRONG TỪNG THIẾT KẾ</h3>
          <p>Galleria mang tính biểu tượng của chúng tôi đã được thiết kế lại hoàn toàn từ trong ra ngoài cho năm 2021 MUA NGAY tại link dưới !</p>
          <a href="#" title="Mua ngay" class="btn btn-orange">Mua ngay</a>
        </div>  
      </div>
      <div class="col-item">
        <a href="#" title="banner">
          <picture>
            <img class="img-responsive img-auto" src="{{asset('image/img/banner1.jpg')}}" alt="TINH XẢO TRONG TỪNG THIẾT KẾ" width="100" height="100">   
          </picture>
        </a>
      </div>
    </div>
  </section>

  <section class="awe-section-5">   
    <section class="section-slide-new slider-product">
      <div class="container">
        <div class="title-text text-center">
          <h3><a href="san-pham-moi" title="Sản phẩm của chúng tôi">Sản phẩm của chúng tôi</a></h3>
          <p>Sản phẩm order với giá tốt nhất!</p>
        </div>
        <div class="tns-outer" id="id_tiny_2-ow">
          <div class="tns-controls">
            <button type="button" data-controls="prev" tabindex="-1" aria-controls="id_tiny_2">prev</button>
            <button type="button" data-controls="next" tabindex="-1" aria-controls="id_tiny_2">next</button>
          </div>
          <div id="id_tiny_2-mw" class="tns-ovh">
            <div class="tns-inner" id="id_tiny_2-iw">
              <div class="d-flex-owl height-220  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" id="id_tiny_2" style="transform: translate3d(0%, 0px, 0px);">
                <div class="space-item-tsn tns-item tns-slide-active" id="id_tiny_2-item0">
                  @if(count($topPosts))
                    @foreach($topPosts as $topPosts)
                  <div class="item" style="margin-left:10px">
                    <div class="product-box">
                       <div class="product-thumbnail">
                        <div class="sale-flash">
                          <span>44%</span>
                        </div>
                        <a href="{{ url($topPosts->id) }}" class="image_thumb">    
                          <!-- <img class="img-responsive center-block img-auto" src="{{asset('image/img/staad-14-2000x.jpg')}}" alt="Túi đựng Laptop BackPack" width="100" height="100"> -->
                          @if($topPosts->featureImage)
                            <img class="img-responsive center-block img-auto" src="{{ Helper::getMediaUrl($topPosts->featureImage, 'small') }}" width="100" height="100" />
                          @else
                            <img class="img-responsive center-block img-auto" src="{{ Helper::getDefaultCover($topPosts) }}" width="100" height="100" />
                          @endif
                        </a>
                        <a href="" class="btn-white btn_view btn right-to quick-view" title="Xem nhanh">
                          <i class="fa fa-eye"></i>
                        </a>
                      </div>
                      <div class="product-info a-left">
                        <h3 class="product-name text2line"><a href="/{{$topPosts->id}}" title="{{$topPosts -> title }}">{{$topPosts -> title }}</a></h3>
                        <div class="price-box clearfix">
                            <div class="special-price">
                              <span class="price product-price">{{$topPosts -> price }} VND</span>
                          </div>
                        </div>
                        <div class="product-action clearfix ">
                          <form action="/cart/add" method="post" class="variants form-nut-grid">
                            <div class="button-action">
                              <input type="hidden" name="variantId" value="49967013" />
                                <button class="btn-buy btn-cart btn btn-gray left-to add_to_cart" title="Mua hàng"><span>
                                  <img class="img-auto" src="{{asset('image/img/ic-cart-product.png')}}" alt="Mua hàng" width="30" height="30"> Mua hàng</span>
                                </button>
                            </div>
                          </form>
                        </div>
                        <div class="bizweb-product-reviews-badge" data-id="22629671"></div>
                      </div>
                    </div>
                  </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

  <section class="awe-section-6"> 
    <div id="box-video-youtube" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>   
      </div>
    </div>
    <div class="halu-card-hori space-30">
      <div class="col-item flex-end bg-main max-width-45 height-400">
        <div>
          <h3>Tầm nhìn từ chất lượng sản phẩm</h3>  
          <p>Các sản phẩm của Đồ Da Galleria đều được làm từ chất liệu da thật 100%, đường chỉ may tinh tế, chắc chắn và giá cả hợp lý.</p>
          <a href="{{ url('collections') }}" class="btn btn-orange" title="Mua ngay">Mua ngay</a>
        </div>    
      </div>  
      <div class="col-item bg-cover content-center" style="background-image: url(image/img/bg-play.jpg);">
        <a href="https://www.youtube.com/watch?v=2WONg7MzcTA&feature=emb_title" class="click-popup-youtube" data-toggle="modal" data-target="#box-video-youtube" data-videourl="https://www.youtube.com/watch?v=2WONg7MzcTA&ab_channel=4USHOPOFFICIAL">
          <img class="img-auto" href="https://www.youtube.com/watch?v=2WONg7MzcTA&feature=emb_title" src="{{asset('image/img/play-button.png')}}" alt="video" width="40" height="40">   
        </a>
      </div>  
    </div>
  </section>

  <section class="awe-section-7">
    <div class="container space-40">
      <div class="title-text text-center">
        <h3>KHÁCH HÀNG NÓI GÌ</h3>
        <p>Chúng tôi tự hào về chất lượng và dịch vụ của mình</p>
      </div>
      <div class="wrap-customer">
        <div class="tns-outer" id="id_tiny_3-ow">
          <div id="id_tiny_3-mw" class="tns-ovh">
            <div class="tns-inner" id="id_tiny_3-iw">
              <div class="d-flex-owl height-340  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" id="id_tiny_3" data-target-id="id_tiny_3" data-xss-items="1" data-xs-items="2" data-sm-items="3" data-md-items="5" data-lg-items="5" data-margin="15" style="transition-duration: 0s; transform: translate3d(0%, 0px, 0px);">
                <div class="space-item-tsn tns-item tns-slide-active" id="id_tiny_3-item0">
                  <div class="halu-customer-item">  
                    <div class="content">
                      <p><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-quote" viewBox="0 0 41 35"><path d="M10.208 17.711h6.124v16.332H0V21.684C0 8.184 5.444.956 16.332 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766zm24.498 0h6.124v16.332H24.498V21.684C24.498 8.184 29.942.956 40.83 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766z" fill="#000" fill-rule="evenodd"></path></svg></p>  
                      Mình rất thích đưa khách hàng của mình đến đây bởi vì phong cách rất chuyên nghiệp.Hơn nữa thức uống ở đây rất ngon, có hương vị rất khác biệt , các vị khách của mình vô cùng thích.
                    </div>
                    <div class="avatar">
                      <img class="basic img-auto img-responsive" src="{{asset('image/img/sec_khach_hang1.png')}}" alt="avatar" width="40" height="40">
                    </div>
                    <h3>Nguyễn Thị Lan</h3>
                  </div>
                  <div class="halu-customer-item">  
                    <div class="content">
                      <p><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-quote" viewBox="0 0 41 35"><path d="M10.208 17.711h6.124v16.332H0V21.684C0 8.184 5.444.956 16.332 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766zm24.498 0h6.124v16.332H24.498V21.684C24.498 8.184 29.942.956 40.83 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766z" fill="#000" fill-rule="evenodd"></path></svg></p>  
                      Phong cách Galleria vô cùng khác biệt nhưng lại hợp ý mình.Mình và các bạn của mình mỗi khi gặp mặt thì đều đến đây cả.Mong Galleria luôn cho nhiều đồ ăn mới nhé.
                    </div>
                    <div class="avatar">
                      <img class="basic img-auto img-responsive" src="{{asset('image/img/sec_khach_hang2.png')}}" alt="avatar" width="40" height="40">
                    </div>
                    <h3>Trịnh Hữu Đức</h3>
                  </div>
                  <div class="halu-customer-item">  
                    <div class="content">
                      <p><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-quote" viewBox="0 0 41 35"><path d="M10.208 17.711h6.124v16.332H0V21.684C0 8.184 5.444.956 16.332 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766zm24.498 0h6.124v16.332H24.498V21.684C24.498 8.184 29.942.956 40.83 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766z" fill="#000" fill-rule="evenodd"></path></svg></p>  
                      Nhân viên tư vấn tận tình, dễ thương. Nhân viên lấy mẫu lại trẻ đẹp, cẩn thận và chu đáo. Nói chung, anh đánh giá dịch vụ tốt. Anh thấy cần phải quảng bá rộng rãi nhiều hơn để có nhiều người biết đến hơn
                    </div>
                    <div class="avatar">
                      <img class="basic img-auto img-responsive" src="{{asset('image/img/sec_khach_hang3.png')}}" alt="avatar" width="40" height="40">
                    </div>
                    <h3>Hoàng Liên Sơn</h3>
                  </div>
                  <div class="halu-customer-item">  
                    <div class="content">
                      <p><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-quote" viewBox="0 0 41 35"><path d="M10.208 17.711h6.124v16.332H0V21.684C0 8.184 5.444.956 16.332 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766zm24.498 0h6.124v16.332H24.498V21.684C24.498 8.184 29.942.956 40.83 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766z" fill="#000" fill-rule="evenodd"></path></svg></p>  
                      Mình rất thích đưa khách hàng của mình đến đây bởi vì phong cách rất chuyên nghiệp.Hơn nữa thức uống ở đây rất ngon, có hương vị rất khác biệt , các vị khách của mình vô cùng thích.
                    </div>
                    <div class="avatar">
                      <img class="basic img-auto img-responsive" src="{{asset('image/img/sec_khach_hang2.png')}}" alt="avatar" width="40" height="40">
                    </div>
                    <h3>Nguyễn Văn Minh</h3>
                  </div>
                  <div class="halu-customer-item">  
                    <div class="content">
                      <p><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-quote" viewBox="0 0 41 35"><path d="M10.208 17.711h6.124v16.332H0V21.684C0 8.184 5.444.956 16.332 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766zm24.498 0h6.124v16.332H24.498V21.684C24.498 8.184 29.942.956 40.83 0v6.125c-4.083 1.14-6.124 4.414-6.124 9.82v1.766z" fill="#000" fill-rule="evenodd"></path></svg></p>  
                      Nhân viên tư vấn tận tình, dễ thương. Nhân viên lấy mẫu lại trẻ đẹp, cẩn thận và chu đáo. Nói chung, anh đánh giá dịch vụ tốt. Anh thấy cần phải quảng bá rộng rãi nhiều hơn để có nhiều người biết đến hơn
                    </div>
                    <div class="avatar">
                      <img class="basic img-auto img-responsive" src="{{asset('image/img/sec_khach_hang1.png')}}" alt="avatar" width="40" height="40">
                    </div>
                    <h3>Trịnh Kim Chi</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="awe-section-8"> 
    <div class="halu-banner-center bg-cover" style="background-image: url(image/img/banner5.jpg);">
      <div class="text">
        <h3>KHÁM PHÁ HỘI THẢO CỦA CHÚNG TÔI</h3>
        <p>Chúng tôi chỉ sử dụng những vật liệu tốt nhất và sự khéo léo trong các sản phẩm của mình. Nếu bạn quan tâm thì hãy đọc tiếp câu chuyện của chúng tôi</p> 
        <a href="{{ url('contact') }}" class="btn btn-orange btn-white-border" title="Đọc tiếp">Đọc tiếp</a>
      </div>
    </div>
  </section>

  <section class="awe-section-9"> 
    <section class="section_blogs" style="margin-top: 10px;">
      <div class="container">
        <div class="title-text text-center">
          <h3><a href="tin-tuc" title="Tin tức">Tin tức</a></h3>
          <p>Những bài viết mới nhất</p>
        </div>
        <div class="list-blogs-link">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="tns-outer" id="id_tiny_4-ow">
                <div id="id_tiny_4-mw" class="tns-ovh">
                  <div class="tns-inner" id="id_tiny_4-iw">
                    <div class="d-flex-owl height-570  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" id="id_tiny_4" data-target-id="id_tiny_4" style="transition-duration: 0s;transform: translate3d(0%, 0px, 0px);">
                      <div class="space-item-tsn tns-item tns-slide-active" id="id_tiny_4-item0">
                        @if(count($topPostsNews))
                        @foreach($topPostsNews as $topPostsNews)
                        <div class="item">
                          <div class="item_wrap_blog">
                            <div class="item-blg blog-large">
                              <div class="blog-inner">
                                <a class="banner" href="/ly-do-ma-ban-can-phai-mua-bao-da-chia-khoa-o-to-ngay-lap-tuc" title="Lý do mà bạn cần phải mua BAO DA CHÌA KHÓA Ô TÔ ngay lập tức">
                                  <img src="{{asset('image/img/bao-da-chia-khoa-o-to-9346bd053a9f46c09e1c13a597b2603f-grande.jpg')}}" class="img-responsive basic img-auto" alt="" width="100" height="100">
                                </a>
                                <div class="content__">
                                  <span class="time_post f"><i class="fa fa-calendar"></i>{{$topPostsNews -> created_at }}</span>
                                  <h3 class="h4">
                                    <a class="text2line" title="{{$topPostsNews -> title }}" href="{{$topPostsNews -> content}}">{{$topPostsNews -> title }}</a>
                                  </h3>
                                  <!-- <p>{{strip_tags($topPostsNews -> content)}}</p> -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
  
  <div class="wrap-newsletter">
    <div class="halu-card-hori">
      <div class="col-item bg-cover height-360" style="background-image: url(image/img/bg-newsletter.jpg);padding: 80px 0;"></div>
      <div class="col-item">
        <div class="box-text">
          <h3>Đăng ký nhận tin</h3>
          <p>Đăng ký nhận bản tin của chúng tôi để nhận các sản phẩm mới, mã khuyến mại nhanh nhất</p>
          <form action="https://facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" class="section-newsletter__form" name="mc-embedded-subscribe-form" target="_blank">
            <input type="email" value="" placeholder="Email của bạn" name="EMAIL" id="mail" aria-label="general-newsletter_form-newsletter_email" >
            <button  class="btn subscribe" name="subscribe" id="subscribe">Đăng ký</button>
          </form> 
        </div>
      </div>
    </div>
  </div>
@endsection