<footer>
  <div class="site-footer">
    <div class="container">
      <div class="row">
        <div class="block block-cs col-xs-12 col-sm-6 col-md-5 col-lg-5">
          <div class="footer-widget">
            <h4>
              <span>Hệ thống cửa hàng toàn quốc</span>
            </h4>
            <ul class="list-menu infomation" style="display: block;padding-left:0;">
              <li class="item">
                <div class="company"><i class="fa fa-map-marker"></i>
                  Lytranauthentic
                </div>
                <div class="address">Địa chỉ: 
                  26/1 Tây Lân - Bà Điểm - Huyện Hóc Môn - TP Hồ Chí Minh
                </div>
                <div class="hotline">Hotline: 
                  <a href="tel:19006750" title="19006750">0977722146</a>  
                </div>
                <div class="address">Website: 
                   http://lytranauthentic.com
                </div>
                <div class="address">Email: 
                  diemlytt@gmail.com
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="block block-cs col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <div class="footer-widget">
            <h4>
              <span>Chính sách</span>
              <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
            </h4>
            <ul class="list-menu has-toggle" style="padding-left:0">
              <li><a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a></li>              
              <li><a href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a></li>              
              <li><a href="{{ url('collections') }}" title="Sản phẩm">Sản phẩm</a></li>             
              <li><a href="{{ url('news') }}" title="Tin tức">Tin tức</a></li>             
              <li><a href="{{ url('contact') }}" title="Liên hệ">Liên hệ</a></li>             
            </ul>
          </div>
        </div>
        <div class="block block-cs col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="footer-widget">
            <h4>
              <span>Kết nối với chúng tôi</span>
            </h4>
            <ul class="list-menu has-toggle social-media-footer" style="display: block;padding-left:0;">
              <li>
                <a href="https://twitter.com/" title="twitter"><i class="fa fa-twitter"> </i></a>
              </li>
              <li>
                <a href="https://www.facebook.com/" title="facebook"><i class="fa fa-facebook"> </i></a>
              </li>
              <li>
                <a href="#" title="pinterest"><i class="fa fa-pinterest"> </i></a>
              </li>
              <li>
                <a href="https://instagram.com" title="instagram"><i class="fa fa-instagram"> </i></a>
              </li>
              <li>
                <a href="https://www.youtube.com/" title="youtube"><i class="fa fa-youtube"></i></a>
              </li>
            </ul>
          </div>
          <div class="footer-widget">
            <h4><span>Thanh toán</span></h4>
              <ul class="list-menu has-toggle payment" style="display: block;padding-left:0;">
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_1.png') }}" alt="galleria" width="50" height="30">
                </li>
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_2.png') }}" alt="galleria" width="50" height="30">
                </li>
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_3.png') }}" alt="galleria" width="50" height="30">
                </li>
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_4.png') }}" alt="galleria" width="50" height="30">
                </li>
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_5.png') }}" alt="galleria" width="50" height="30">
                </li>
                <li>
                  <img class="img-responsive" src="{{ asset('image/img/img_payment_6.png') }}" alt="galleria" width="50" height="30">
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright clearfix">
      <div class="container">
        <p class="text-center">
          © Bản quyền thuộc về Gisol
          <span class="nc hidden-xs">| </span><span class="cungcap">Cung cấp bởi <b> Gisol</b><a href="" rel="nofollow" title="Gisol">Gisol</a></span>
        </p>   
        <!-- <div class="back-to-top"><i class="fa fa-angle-double-up"></i></div> -->
      </div>
    </div>
</footer>