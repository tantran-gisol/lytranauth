<!-- bootstrap -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lobster+Two">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/flexslider/flexslider.css')}}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald" />

<!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/contact.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/collections.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/news.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/cart.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/login.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/custom.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  
<!-- Libraries CSS Files -->
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

<!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">