<body class="body_index">
  <header class="header">
    <!-- <div class="search active">
      <div class="close-search">
        <i class="fa fa-close"></i>
      </div>
      <div class="header_search search_form">
        <form class="input-group search-bar search_form" action="/search" method="get" role="search">   
          <input type="search" name="query" value="" placeholder="Nhập từ khóa bạn muốn tìm kiếm ... " class="input-group-field st-default-search-input search-text" autocomplete="off">
          <button>Tìm kiếm</button>
        </form>
      </div> 
    </div> -->
    <div class="middle-header bg-cover">
      <div class="container">
        <div class="header-main">
          <div class="d-flex-desktop">
            <div class="flex align-center">
              <div class="icon-menu-mobile hidden-lg hidden-md">
                <i class="fa fa-bars" aria-hidden="true"></i>
              </div>
              <div class="icon-click-search">
                <img src="{{asset('image/img/icon-search.png')}}" alt="search" width="20" height="20">
              </div>
            </div>
            <div class="logo">
              <a href="/" class="logo-wrapper " title="galleria"> 
                <img class="img-auto" src="{{asset('image/img/logo.png')}}"  alt="galleria" width="50" height="30">
              </a>              
            </div>
            <div class="right">
              <a href="{{ url('login') }}" class="icon-acc">
                <img class="w-20" src="{{ asset('image/img/ic-account.png')}}" alt="login" height="20" width="20">  
              </a>
              <div class="mini-cart text-xs-center">
                <div class="heading-cart">
                  <a href="{{ url('cart') }}"  title="Giỏ hàng">
                    <img class="w-20" src="{{asset('image/img/ic-cart.png')}}" alt="giỏ hàng" height="20" width="20">
                    <span class="cartCount count_item_pr" id="cart-total">10</span>
                  </a>
                </div>
                <div class="top-cart-content hidden-sm hidden-xs">
                  <ul id="cart-sidebar" class="mini-products-list count_li">
                    <li class="list-item">
                      <ul></ul>
                    </li>
                    <li class="action">
                      <ul>
                        <li class="li-fix-1">
                          <div class="top-subtotal">
                            Tổng tiền thanh toán: 
                            <span class="price"></span>
                          </div>
                        </li>
                        <li class="li-fix-2" style="">
                          <div class="actions">
                            <a href="/cart" class="btn btn-primary"  title="Giỏ hàng">
                              <span>Giỏ hàng</span>
                            </a>
                            <a href="/checkout" class="btn btn-checkout btn-gray" title="Thanh toán ">
                              <span>Thanh toán</span>
                            </a>
                          </div>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>  
            </div>
          </div>
        </div>
        <div class="main-nav hidden-sm hidden-xs">
          <nav>
            <ul class="nav nav-mobile">
              <li class="hidden-sm hidden-xs nav-item active">
                <a class="nav-link" href="{{ url('/') }}" title="Trang chủ">Trang chủ</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item ">
                <a class="nav-link" href="{{ url('intro') }}" title="Giới thiệu">Giới thiệu</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item  has-mega has-dropdown">
                <a href="{{ url('collections') }}" title="Sản phẩm" class="nav-link">Sản phẩm <i class="fa fa-angle-down" data-toggle="dropdown"></i></a>
                <div class="mega-content">
                  <div class="level0-wrapper2">
                    <div class="nav-block nav-block-center">
                      <ul class="level0">
                        <li class="level1 parent item ">
                          <h2 class="h4"><a href="/" title="Sản phẩm mới"><span>Sản phẩm mới</span></a></h2> 
                          <ul class="level1">
                            <li class="level2"> <a href="/" title="Túi xách"><span>Túi xách</span></a></li> 
                            <li class="level2"> <a href="/" title="Giày da"><span>Giày da</span></a></li>
                          </ul>
                        </li>
                        <li class="level1 item">
                          <h2 class="h4"><a href="/" title="Thắt lưng"><span>Thắt lưng</span></a></h2>
                        </li>
                      </ul>
                    </div>
                  </div>
                <div class="banner-mega-content">
                  <div class="col-md-6">
                    <a href="#" title="mega-menu">
                      <img class="basic img-responsive lazied" data-src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images1.png?1650527032568" src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images1.png?1650527032568" alt="Mega menu 1" data-lazied="IMG">
                    </a>
                  </div>
                  <div class="col-md-6">
                    <a href="#" title="mega-menu">
                      <img class="basic img-responsive lazied" data-src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images2.png?1650527032568" src="//bizweb.dktcdn.net/100/430/435/themes/831833/assets/mega-menu-images2.png?1650527032568" alt="Mega menu 1" data-lazied="IMG">
                    </a>
                  </div>
                </div>
              </div>
            </li>
              <li class="hidden-sm hidden-xs nav-item  has-dropdown">
                <a href="{{ url('news') }}" title="Tin tức" class="nav-link">Tin tức</a>
              </li>
              <li class="hidden-sm hidden-xs nav-item ">
                <a class="nav-link" href="{{ url('contact') }}" title="Liên hệ">Liên hệ</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
</body>