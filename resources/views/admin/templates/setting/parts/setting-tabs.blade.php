<nav class="media-tabs-management" style="margin-bottom: 10px;">
	<div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link {{ request()->is('admin/setting') ? 'active' : '' }}" href="{{ route('admin.setting.index') }}">Cài đặt chung</a>
		<a class="nav-item nav-link {{ request()->is('admin/setting/stylesheet') ? 'active' : '' }}" href="{{ route('admin.setting.stylesheet') }}">Khu vực ngang tùy chỉnh</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-2">カスタム横域</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/fixedpage') ? 'active' : '' }}" href="{{ route('admin.setting.fixedpage') }}">Trang cố định</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-4">状態</a> -->
		<!-- <a class="nav-item nav-link" href="#nav-tab-5">ファイル</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/widget') ? 'active' : '' }}" href="{{ route('admin.setting.widget') }}">Widget</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-7">キャッシュ更新</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/vote') ? 'active' : '' }}" href="{{ route('admin.setting.vote') }}">Bảng lựa chọn</a>
		<a class="nav-item nav-link {{ request()->is('admin/setting/footer') ? 'active' : '' }}" href="{{ route('admin.setting.footer') }}">Footer</a>
	</div>
</nav>