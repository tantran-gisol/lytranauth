@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Cài đặt media</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<section id="nav-tab-1">
			<div class="nav-tab-1-title">
				<p>Cài đặt thông tin cơ bản</p>
			</div>

			<div class="media-edit">
				<form action="{{ route('admin.setting.update') }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">
					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Tiêu đề</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">Ngoài tiêu đề trang web, tiêu đề cửa sổ<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">< meta property:'og:title' ></span><!-- <button class="btn btn-light" style="color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">
																	< meta property:'og:title' >
																	</button> --></p>
										<!-- <button>meta property:'og:title'</button> -->Nó được thêm vào sau tiêu đề của tất cả các trang.</p>
									<p>Đối với SEO, hãy viết ngắn gọn ở đây<span style="color: #C43638;">Khu vực ngang tùy chỉnh</span>Các thước đo SEO trên trang đầu (đường dẫn URL =): Nên đặt một tiêu đề dài với các thước đo từ khóa, v.v. trong tiêu đề.</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="" name="general_seo_title" value="{{ (array_key_exists('general_seo_title', $settings)) ? $settings['general_seo_title'] : '' }}" style="font-size: 12px;">
						</div>
					</div>

					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Điều hành công ty</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="background: #fff; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>
								
								<div style="color: #888;">
									<p style="margin-bottom: 1rem;">Được sử dụng cho phần Bản quyền của móc nối chung</p>
								</div>
							</div>
							
							<input type="text" class="form-control" placeholder="" name="general_copyright" value="{{ (array_key_exists('general_copyright', $settings)) ? $settings['general_copyright'] : '' }}" style="font-size: 12px;" >
						</div>
					</div>

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Mô tả</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">Mô tả về trang web.<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">< meta property='description' ></span><!-- <button class="btn btn-light" style="color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">
									< meta property='description' ></button> -->
										<!-- <button>meta property='description'</button> -->hoặc</p>

									<p style="margin-bottom: 0;">
										<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">< meta property:'og:description' ></span><!-- <button class="btn btn-light" style="color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">
																			< meta property:'og:description' ></button> -->
								<!-- <button>meta property:'og:description'</button> -->  được sử dụng cho.</p>
								</div>
							</div>

							<textarea class="form-control" style="width: 100%; height: 12rem; font-size: 12px;" name="general_seo_description">{{ (array_key_exists('general_seo_description', $settings)) ? $settings['general_seo_description'] : '' }}</textarea>
						</div>
					</div>

					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>URL hình ảnh OGP</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>
								
								<div style="color: #888;">
									<p style="margin-bottom: 1rem;">Đó là một hình ảnh khi chia sẻ trên SNS chẳng hạn như Facebook.</p>

									<p style="margin-bottom: 0;">Ngoài hình ảnh được chia sẻ của trang web, danh sách các trang bài viết không có hình ảnh nào được đặt</p>
									<p style="margin-bottom: 0;">Nó cũng được sử dụng làm hình thu nhỏ / hình ảnh OGP trong.</p>
								</div>
							</div>

							<div class="img-wrap" style="width: 300px; height: 160px; background: url('{{ (array_key_exists('general_seo_image', $settings)) ? $settings['general_seo_image'] : '' }}'); margin: 1rem 0 .5rem 0;" data-image-input-preview="general_seo_image">
									
							</div>
							
							<input type="text" class="form-control" placeholder="" name="general_seo_image" value="{{ (array_key_exists('general_seo_image', $settings)) ? $settings['general_seo_image'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">

							<button type="button" class="btn btn-no-radius open-library" data-input-name="general_seo_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
						</div>
					</div>

					<!-- <div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>独自ドメインマップ</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div style="width: 80%;margin-bottom: 1rem;">
								<div style="color: #888;">
									<p style="margin-bottom: 0;">独自ドメインが設定されています-<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">{{ (array_key_exists('general_seo_domain', $settings)) ? $settings['general_seo_domain'] : '' }}</span></p>

									<p style="margin-bottom: 0; color: #C43638;"><a data-toggle="collapse" href="#collapse-domain" role="button" aria-expanded="false" aria-controls="collapse-domain"><i class="fas fa-edit"></i>ドメイン設定を変更する</a></p>
									<div class="mt-3 collapse" id="collapse-domain">
										<input type="text" class="form-control" placeholder="" name="general_seo_domain" value="{{ (array_key_exists('general_seo_domain', $settings)) ? $settings['general_seo_domain'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">
									</div>
								</div>
							</div>
						</div>
					</div> -->

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>デザイン</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<button type="button" class="btn btn-no-radius" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
						</div>
					</div> -->

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>URL hình ảnh tiêu đề</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p>Đây là hình ảnh được hiển thị trong tiêu đề.</p>

									<p>Vùng hiển thị có chiều rộng 240px và cao 64px, căn trái và căn giữa theo chiều dọc.</p>

									<p style="margin-bottom: 0;">Để hiển thị đẹp mắt trên các thiết bị đầu cuối có độ phân giải cao như điện thoại thông minh và PC / Mac mới nhất hỗ trợ màn hình Retina, hãy tăng gấp đôi kích thước hình ảnh cho "thiết bị đầu cuối độ phân giải 2x" và 3x cho "thiết bị đầu cuối độ phân giải 3x". Vui lòng đặt kích thước hình ảnh </p>

									<p>Nếu không được chỉ định, ký tự có độ phân giải bình thường sẽ được sử dụng, nhưng nó có thể trông mờ so với các ký tự xung quanh.</p>

									<p>Khi chọn từ thư viện, hãy đảm bảo rằng kích thước đáp ứng các điều kiện, nhấp vào tệp tương ứng [Sử dụng hình ảnh này] và chọn hình ảnh gốc.</p>
								</div>
							</div>
							
							<div class="mb-4">
								<p style="margin-bottom: 0;">Đối với máy tính</p>
								@if(array_key_exists('general_logo', $settings) && !empty($settings['general_logo']))
									<div style="width: 240px; height: 64px;">
										<img src="{{ $settings['general_logo'] }}" class="w-100 h-100" data-image-input-preview="general_logo">
									</div>
								@endif
								<input type="text" class="form-control" placeholder="" name="general_logo" value="{{ (array_key_exists('general_logo', $settings)) ? $settings['general_logo'] : '' }}" style="margin-bottom: .5rem;font-size: 12px;">
								<button type="button" class="btn btn-no-radius open-library" data-input-name="general_logo" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
							</div>

							<div class="mb-4">
								<p style="margin-bottom: 0;">Đối với điện thoại di động</p>
								@if(array_key_exists('general_logo_mobile', $settings) && !empty($settings['general_logo_mobile']))
									<div style="width: 300px; height: 120px;">
										<img src="{{ $settings['general_logo_mobile'] }}" class="w-100 h-100" data-image-input-preview="general_logo_mobile">
									</div>
								@endif
								<input type="text" class="form-control" placeholder="" name="general_logo_mobile" value="{{ (array_key_exists('general_logo_mobile', $settings)) ? $settings['general_logo_mobile'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">
								<button type="button" class="btn btn-no-radius open-library" data-input-name="general_logo_mobile" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
							</div>
						</div>
					</div>

					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Hình ảnh biểu tượng</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">Đặt hình ảnh phổ biến được sử dụng bên dưới</p>

									<p style="margin-bottom: 0;">Biểu tượng yêu thích của trang web</p>

									<p style="margin-left: 1rem;">Các biểu tượng được hiển thị trong dấu trang, tab của trình duyệt, v.v.</p>

									<p style="margin-bottom: 0;">Biểu tượng clip web</p>

									<p style="margin-left: 1rem;">Biểu tượng khi đăng ký trên màn hình chính bằng điện thoại thông minh</p>
								</div>
							</div>

							<div class="img-wrap" style="width: 75px; height: 75px; background: url('{{ (array_key_exists('general_favicon', $settings)) ? Helper::getMediaUrlById($settings['general_favicon'], 'thumbnail') : '' }}');" data-image-input-preview="general_favicon"></div>
							<p style="margin-bottom: 0; color: #C43638;"><a data-toggle="collapse" href="#collapse-ico" role="button" aria-expanded="false" aria-controls="collapse-ico"><i class="fas fa-edit" aria-hidden="true"></i>Chỉnh sửa biểu tượng thấp</a></p>
							<div class="mt-3 collapse" id="collapse-ico">
								{{--<input type="text" class="form-control" placeholder="" name="general_favicon" value="{{ (array_key_exists('general_favicon', $settings)) ? $settings['general_favicon'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;"> --}}
								<input type="hidden" name="general_favicon" value="{{ (array_key_exists('general_favicon', $settings)) ? $settings['general_favicon'] : '' }}" >
								<button type="button" class="btn btn-no-radius open-library" data-input-name="general_favicon" data-input-type="id" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
							</div>
							<!-- <div class="dropdown">
								<button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdown-menu-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-align: left;">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdown-menu-button">
										<a class="dropdown-item" href="#">lorem ipsum</a>
										<a class="dropdown-item" href="#">lorem ipsum</a>
										<a class="dropdown-item" href="#">lorem ipsum</a>
								</div>
							</div> -->
						</div>
					</div>

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Từ ngữ trong menu tiêu đề "TRANG CHỦ"</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">Thay đổi từ ngữ của menu "TRANG CHỦ" trong tiêu đề. Nếu nó trống, nó sẽ không được hiển thị.</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="Home" name="general_home_label_top_menu" value="{{ (array_key_exists('general_home_label_top_menu', $settings)) ? $settings['general_home_label_top_menu'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">							
						</div>
					</div>

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>トップページタイプ</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">サイトトップの表示内容です。</p>

									<p style="margin-bottom: 0;">ニュースフィード</p>

									<p style="margin-left: 1rem;">優先コンテンツと新着記事の一覧を表示します</p>

									<p style="margin-bottom: 0;">メニューフィード</p>

									<p style="margin-left: 1rem;">ニュースフィードおよび各メニュータグ新着記事一覧をブロックで表示します</p>
								</div>
							</div>

							<div>
								<select style="width:60%;">
									<option value="">No Select</option>
									<option value="" selected>ニュースフィード：全記事を新着順に表示</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>ニュースフィード表示形式</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">ニュースフイードの表示形式です。</p>
									<p>「リスト型」以外では、<span style="color: #c43638;">優先コンテンツ設定-ニュースフィードでの表示形式</span>設定は無視されます。</p>
									<p style="color: #c43638;"><i class="fas fa-caret-right"></i>設定てきる形式</p>
								</div>
							</div>

							<div>
								<select style="width: 21%;">
									<option value="">No Select</option>
									<option value="" selected>カード型</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>公式ライター記事の表示形式</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;"><span style="color: #c43638;">優先コンテンツ設定-ニュースフィードでの表示形式</span>の初期値です。公式ライターの投稿が優先コンテンツに追加されるタイミングで使用されます。</p>

								</div>
							</div>

							<div>
								<select style="width: 43%;">
									<option value="">No Select</option>
									<option value="" selected>A-1：画像左・テキスト右</option>
								</select>
							</div>
						</div>
					</div>						

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>公式ライターのラベル</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p><span style="color: #c43638;" >公式ライター</span>のラベルを変更します。</p>
									<p>公式ライターが書いた記事の上に表示されるほか、<span  style="color: #c43638;">公式ライター一覧ページ(/_users）</span>のタイトルにも使用されます。</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="Official Staff" name="" style="margin-bottom: .5rem;">							
						</div>
					</div> -->

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>機能</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
						</div>
					</div> -->

					<!-- <div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>FacebookアプリID</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;"></i>
								</div>

								<div style="color: #888;">
									<p>Facebookでシェアされるための必要な情格（OGP）を正しく認能させ、Facebook上でインサイト対象にするだめに要な「fb:app_id」（FacebookアプリID）を設定します。</p>
									<p>取得方法や利用シーンは<span style="color: #c43638;">「fb:app_id ""インサイト"取得」などで検索</span>してくださ</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="" name="general_fb_id" value="{{ (array_key_exists('general_fb_id', $settings)) ? $settings['general_fb_id'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">							
						</div>
					</div> -->

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>コメントブラグイン</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="dropdown">
								<button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdown-menu-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-align: left;">
										使わない
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdown-menu-button">
										<a class="dropdown-item" href="#">lorem ipsum</a>
										<a class="dropdown-item" href="#">lorem ipsum</a>
										<a class="dropdown-item" href="#">lorem ipsum</a>
								</div>
							</div>
							<div>
								<select >
									<option value="">No Select</option>
									<option value="" selected>使わない</option>
								</select>
							</div>
						</div>
					</div> -->

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0; margin-bottom: 2.5rem;">
						<div class="col-lg-2 col-md-3">
							<label>Nút chia sẻ</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div style="width: 80%;margin-bottom: 1rem; padding: 0.5rem;">
								@php
									$social_share_option = (array_key_exists('general_social_share_option', $settings)) ? json_decode($settings['general_social_share_option'], true) : [];
								@endphp
								<input class="d-none" type="checkbox" name="general_social_share_option[]" value="default" checked>
								<div class="w-100">
									<label>
										<input type="checkbox" name="general_social_share_option[]" value="twitter" {{(is_array($social_share_option) && in_array('twitter', $social_share_option)) ? 'checked' : ''}} />
										<span>Tweet</span>
									</label>
								</div>
								<div class="w-100">
									<label>
										<input type="checkbox" name="general_social_share_option[]" value="facebook" {{(is_array($social_share_option) && in_array('facebook', $social_share_option)) ? 'checked' : ''}} />
										<span>Facebook</span>
									</label>
								</div>
								<!-- <div class="w-100">
									<input type="checkbox" name="">
									<label for="checkbox">LINE</label>
								</div>
								<div class="w-100">
									<input type="checkbox" name="">
									<label for="checkbox">はてブ</label>
								</div> -->
							</div>					
						</div>
					</div>

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>メディアリンク画像URL</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>
								
								<div style="color: #888;">
									<p style="margin-bottom: 0;">このメディアに所属する公開dinoテナント下部に表示されるブランドバー内の</p>
									<p>このメディアへめリンク画像です。</p>
									<p>高さ32px、左寄せでぴったり表示されます。幅は195pxしてください。</p>
									<p>空白にするとメディアのタイトル文字が使われます</p>
								</div>
							</div>
							
							<input type="text" class="form-control" placeholder="https://some.image server/path/tolimage.jpg" name="" style="margin-bottom: .5rem;">

							<button class="btn btn-no-radius" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;" aria-hidden="true"></i> メディアライブラリから選択</button>
						</div>
					</div> -->

					<!-- <div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>SEO関係</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
						</div>
					</div> -->

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>トラッキングコード配言</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div>
								<select >
									<option value="">No Select</option>
									<option value="" selected>の直町</option>
								</select>
							</div>

							<div class="d-flex" style="background: #D1E3FA; margin-bottom: 1rem; margin-top: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #fff;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">トラッキングコード自体の設定は<span style="color: #c43638; ">共通カスクム設定</span>にあります</p>

								</div>
							</div>
						</div>
					</div> -->

					<!-- <div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>サイトマップ（B）</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p>税索エンジン向けの<span style="color: #c43638;">サイトマップ</span>モ生成し、robots.txtに登録しています。</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="https://adiva-world.jp/sitemap-index.xml" name="" style="margin-bottom: .5rem; font-size: 12px; ">							
						</div>
					</div> -->

					<!-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Google AMP対応（B）</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex col-12 col-md-10" style="background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p>各コンテンツについて、<span style="color: #c43638; ">Google AMPページ</span>を生成しています。
									</p>
									<p style="margin-bottom: 0;">全自動で変換しますが、AMPが許容しない無効な開性（有効なURLでない<button class="btn btn-light" style="color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">
									target</button>、<button class="btn btn-light" style="color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">href</button>など）のあるコンテンツはエラーとなり、染録されないことがあります。</p>
									<p style="margin-bottom: 0;"><span style="color: #c43638;">Google Search Console</span>でAMPエラーを確認してください。</p>
								</div>
							</div>

							<div class="d-flex" style="background: #D1E3FA; margin-bottom: 1rem; margin-top: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #fff;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">AMP用トラッキングコードは<span style="color:  #c43638;">共通カスタム設定</span>に設定してください。</p>
								</div>
							</div>

							<div class="d-flex" style="background: #F6F5B6; margin-bottom: 1rem; margin-top: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-exclamation-triangle" style="color: #c4c236;"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">現在β機能のため、表示乱れ等が発生する可能性があります。また、ページ生成のアルゴリズム・表示は変更される可能性があります。</p>
								</div>
							</div>
						</div>
					</div> -->

					<div class="form-group row" style="background: #f8f8f8; padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Từ khóa meta</label>
						</div>

						<div class="col-12 col-lg-10 col-md-9">
							<div class="d-flex" style="width: 80%; background: #fff; margin-bottom: 1rem; padding: 0.5rem;">
								<div>
									<i class="fas fa-info-circle" style="color: #888;" aria-hidden="true"></i>
								</div>

								<div style="color: #888;">
									<p style="margin-bottom: 0;">Trên đầu trang
										<span class="btn btn-light" style="font-size: 12px; color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">< meta name="keywords"></span>Được dùng cho. Vui lòng mở đầu bằng dấu phẩy được phân tách, chúng tôi khuyên bạn nên sử dụng trong vòng 5 từ khóa.
									</p>
									<p>Tuy nhiên, tính đến năm 2016, hầu hết các công cụ tìm kiếm dường như bỏ qua các từ khóa meta.
									</p>
								</div>
							</div>

							<input type="text" class="form-control" placeholder="" name="general_seo_keywords" value="{{ (array_key_exists('general_seo_keywords', $settings)) ? $settings['general_seo_keywords'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">							
						</div>
					</div>
					<button class="btn btn-no-radius mt-3" style="background: #111; color: #fff;">Cập nhật</button>
					{{ csrf_field() }}
				</form>

			</div>
		</section>
	</section>
	<!-----x----- main -----x----->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection