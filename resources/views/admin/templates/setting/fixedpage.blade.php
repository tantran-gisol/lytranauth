@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Cài đặt media</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<section id="nav-tab-1">
			<div class="nav-tab-1-title">
				<p>Trang cố định</p>
			</div>
			<a href="{{ route('admin.page.create') }}"><button class="btn btn-no-radius" style="background: #111111; color: #ffffff;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;" aria-hidden="true"></i>Thêm trang cố định</button></a>

			<div class="setting-table-list table-responsive-md" style="overflow: auto;">
				<table class="table">
						<tr>
						   	<th>Tiêu đề</th>
						    <th>Tác giả</th>
						    <th>Ngày</th>
						    <th>Ban quản lý</th>
						</tr>
						@if(count($pages))
							@foreach( $pages as $page )
								<tr>
								    <td>
								    	<p style=" color: black; font-size: 13px;">{{ $page->title }}</p>
								    </td>
								    <td>{{ ($page->user) ? $page->user->name : '' }}</td>
								    <td style="color: #111111;"><p>{{ $page->created_at }}</p></td>
								    <td>
								    	<a href="{{ route('admin.page.edit', $page) }}" class="btn mb-1" style="font-size: 12px; background: #111111; color: #ffffff;width: 70px;">Chỉnh sửa</a>
								    	<a href="{{ route('admin.page.change_status', $page) }}" class="btn mb-1" style="font-size: 12px; background: #111111; color: #ffffff; width: 70px;">{{ $page->status ? 'Chặn' : 'Sử dụng' }}</a>
								    </td>
								</tr>
							@endforeach
						@endif
				</table>
			</div>
			<div style="margin-top: 20px;">
				<p style="color: #999999;">#{{ (($pages->currentPage() - 1) * $pages->perPage()) + 1 }}...#{{ ($pages->currentPage() * $pages->perPage()) <= $pages->total() ? $pages->currentPage() * $pages->perPage() : $pages->total() }} / {{ $pages->total() }}</p>
				<div class="pagination-menu d-flex">{{ $pages->onEachSide(1)->links() }}</div>
			</div>
		</section>
	</section>
	<!-----x----- main -----x----->
@endsection