@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main-writer" >
		<div class="list-of-content-title" style="margin-top: 30px;margin-bottom: 50px;">
			<h3 class="font-weight-bold">Quản lý tác giả</h3>
			<p class="font-weight-bold">Quản lý các tác giả truyền thông</p>
			@include('admin.parts.alert')
		</div>
		<div class="wirter-management-table table-responsive-md">
			<table class="table" style="width:100%;background-color: #DDDDDD">
				<tr>
					<th style="width: 13%">Hiển thị thứ tự</th>
					<th style="width: 47%">Tên</th>
					<th></th>
				</tr>
				@if(count($users))
					@foreach($users as $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->name }}</td>
							<td></td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<div>
			<div class="list-wirter-management d-flex">
				<div>
					<i class="fas fa-info-circle" style="color:#98908A;"></i>
				</div>
				<div style="margin-left: 10px;">
					<p>Nếu bạn đăng ký một nhà văn là "tác giả chính thức" của phương tiện truyền thông, những điều sau đây sẽ được áp dụng.</p>
					<ul>
						<li>
							Phương tiện truyền thông "<span>Danh sách các nhà văn chính thức</span>Xuất hiện như một tác giả trên trang.
						</li>
						<li>Các bài viết được tạo bởi các nhà văn đã đăng ký là "tác giả chính thức" sẽ tự động nhận được"<span>Nội dung ưu tiên</span>"Cài đặt được áp dụng. Kết quả là 24 giờ sau khi xuất bản sẽ được ưu tiên lên trang đầu.
						</li>
					</ul>
					<p>
						Để thêm một tác giả chính thức, hãy làm theo các bước dưới đây.
					</p>
					<ul>
						<li><span>Nội dung</span>Truy cập vào</li>
						<li>Nhấp vào tên người viết của bài báo do người viết có cài đặt người viết chính thức mà bạn muốn thay đổi. Nhấp vào "Trở thành tác giả chính thức" được menu hiển thị</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection