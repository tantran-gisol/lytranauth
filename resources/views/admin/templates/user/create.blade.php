@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="change-avatar">
			<div>
				<h3>Thông tin quản trị viên</h3>
				<p>Chỉnh sửa thông tin quản trị viên.</p>
				@include('admin.parts.alert')
			</div>
		</div>		
		<div class="admin-edit-info" style="margin-top: 30px;">
			<form class="edit-info" action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="form-group edit-info-email row" style="background: #f8f8f8; padding-top: 1rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Địa chỉ email</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" required="" />
						<div class="form-check">
							<!-- <input type="checkbox" class="form-check-input active" name="receive_notification" id="receive_notification" value="1" {{old('receive_notification') ? 'checked' : ''}} />
							<label class="form-check-label" for="receive_notification">メールで管理者通知を受け取る</label> -->
						</div>
					</div>
				</div>
				<div class="form-group edit-info-warning row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Mật khẩu</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="password" class="form-control" name="password" value="{{ old('password') }}" required="" />
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2.5rem 0; background: #f8f8f8; margin-top: 2rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Tên</label>
					</div>
					<div class="col-md-9 col-12" style="margin-bottom: 10px;">
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" required=""/>
					</div>
				</div>
				<div class="form-group edit-info-sns row"style="margin-top:18px !important;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Hình đại diện</label>
					</div>
					<div class="col-md-9 col-12" style="margin-top: 7px;">
						<img id="changeimg" src="{{ asset('image/no-img.png') }}" style="height: 116px;width: 116px; object-fit: cover;"><br>
						<div class="row" style="margin-top: 16px;">
								<input type="file" name="file" id="file" />
						</div>
					</div>
				</div>
				<div class="form-group row" style="background: #f8f8f8;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12" style="padding-top: 1rem;">
						<label>Hồ sơ</label>
					</div>
					<div class="col-md-9 col-12" style="padding-top: 1rem;">
						<textarea type="text" class="edit-your-info form-control" rows="8" name="writer_profile_html">{{ old('writer_profile_html') }}</textarea>
						<label class="text-info-txt row" style="margin-left: 0; padding: .5rem 0; background: #ffffff; width: 75%; margin-top: 18px;">
							<i class="fas fa-info-circle" style="margin-left: .5rem;"></i>Các phương tiện truyền thông<span style="color: #c43638;">Trang người dùng</span>HTML có thể được sử dụng
						</label>
					</div>
				</div>
				<div class="form-group row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label style="margin-top: 8px;">Thẩm quyền</label>
					</div>
					<div class="col-md-9 col-12">
						<select name="role_id" style=" width: 215px;" required="">
							<option value="">Thẩm quyền</option>
							@if(count($roles))
								@foreach($roles as $role)
									<option value="{{ $role->id }}" {{ old('role_id') == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
								@endforeach
							@endif
						</select>

						<!-- <div class="dropdown-warning font-weight-bold">
							<input type="checkbox" id="disable_article_publishing" name="disable_article_publishing" value="1" {{old('disable_article_publishing') ? 'checked' : ''}} />
							<label for="disable_article_publishing">記事の公開を不可にする</label>
						</div> -->
						
						<!-- <label class="text-info-txt row" style="margin-left: 17px">
							<i class="fas fa-info-circle"></i>「記事の公開を不可にする」をチェックすると、ユーザーは下きき状態の記事だ<br>けを扱える状態になります。<br>
							この設定はユーザーの権限が「記事作成者」の場合にのみ指定可能です。
						</label> -->
					</div>
				</div>
				<div style="margin-top: 70px;">
					<a href="{{ route('admin.user.index') }}" class="btn-back btn btn-no-radius" style="font-size: 12px; width: 100px; line-height: 28px;">
						<i class="fas fa-arrow-left"></i>
						Quay lại
					</a>
					<button class="btn-submit btn btn-no-radius" style="font-size: 12px; width: 100px;">Cập nhật</button>
				</div>
			</form>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
@endsection