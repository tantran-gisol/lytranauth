@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Xây dựng media</h3>
		</div>
		<nav class="content-tabs-management">
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
  			<a class="nav-item nav-link active" href="{{ route('admin.statistic.index') }}">Bài đăng</a>
  			<a class="nav-item nav-link" href="{{ route('admin.statistic.user') }}">Xếp hạng người dùng</a>
				</div>
		</nav>
		<div style="margin-top: 10px;">
			<p style="margin-bottom: 0px;">Kiểm tra thông tin đóng góp trên các phương tiện truyền thông trong 30 ngày qua bằng bài viết.</p>
			<p>Tác giả bài viết chỉ có thể xem thông tin về bài báo mà họ đã viết. Ban quản trị có thể kiểm tra thông tin của tất cả các bài viết.</p>
		</div>
		<div class="container">
	    	<div id="chartdiv" style="width: 100%; height: 300px;"></div>
    		<p id="chartTitle" style="text-align: center;">
    			@if(isset($postStatistic) && !is_null($postStatistic))
    				{{ route('frontend.post.show', $postStatistic->slug) }} - {{ $postStatistic->title }}
  				@endif
  			</p>
	 	</div>
		<div class="media-table-contribution table-responsive-md" style="overflow: auto;">
			<table class="table" style="font-size: 12px;">	  
				<tr>
				  	<th style="border-right: none; border-left: none;"><p style="margin-left: 10%;margin-bottom: 0;">Title</p></th>
				    <th style="text-align: right;border-left: none;border-right: none;margin-left: 5px;">30&nbsp;Day&nbsp;&nbsp;
				    	<div class="float-right" style="position: relative;">
				    		<a href="{{ route('admin.statistic.index', ['order' => 'asc']) }}"><i class="fas fa-sort-up" style="color: {{ (request()->order=='asc') ? '#C43638' : 'black'}}; position: absolute; top: 50%; left: -8px; transform: translateY(-32%); "></i></a>
				    		 &nbsp;
				    		<a href="{{ route('admin.statistic.index', ['order' => 'desc']) }}"><i class="fas fa-sort-down" style="color: {{ (request()->order!='asc') ? '#C43638' : 'black'}}; position: absolute; top: 35%; right: -8px; transform: translateY(-50%);"></i></a>
				    	</div>
				    </th>
				</tr>
				@if(isset($posts) && count($posts))
					@foreach( $posts as $post )
						<tr class="post-view-statistic {{ $loop->first ? 'table-active' : '' }}" style="cursor: pointer;" data-action="{{ route('admin.post.get_view_log', $post) }}">
							<td>
								<div style="float: left;margin-right: 10px;">
									@if($post->featureImage)
										<img src="{{ Helper::getMediaUrl($post->featureImage, 'thumbnail') }}" style="width:50px;">
									@else
										<img src="{{ asset('image/200x150.png') }}" style="width:50px;">
									@endif
								</div>
								<p><span>{{ $post->title }}</span>
								</p>
								<a class="post-links" href="{{ route('frontend.post.show', $post->slug) }}" target="_blank" style="color: #C43638;"><i class="fas fa-eye"  style="color: #C43638;margin-right: 5px;"></i>{{ route('frontend.post.show', $post->slug) }}</a>
								<div style="clear: both; "></div>
							</td>
							<td style="text-align: right;"><p style="margin-right: 15px;">{{ ($totalView) ? number_format(($post->last_month_total_views_count/$totalView)*100, 2) : '' }}%</p></td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<p style="color: #999999;">#{{ (($posts->currentPage() - 1) * $posts->perPage()) + 1 }}...#{{ ($posts->currentPage() * $posts->perPage()) <= $posts->total() ? $posts->currentPage() * $posts->perPage() : $posts->total() }} / {{ $posts->total() }}</p>
		<div class="pagination-menu d-flex">{{ $posts->appends(['order' => request()->order])->onEachSide(1)->render() }}</div>
		<div class="col-12 dash-border-box" style="margin: 20px 0;">
			<div class="d-flex">
				<div style="margin: 1rem;margin-left: 10px; margin-top: 27px;">
					<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
				</div>

				<div style="margin: .4rem 0;">
					<div>
						<form action="{{ route('admin.statistic.index') }}" method="GET" class="d-flex">
							<input class="form-control" type="text" name="id" value="{{ request()->get('id') }}" placeholder="123456 - 記事ID" style="margin: 15px 0;font-size: 12px;">
							<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem; margin: 15px; "><i class="fas fa-search" style="color: #ffffff; width: 25px;"></i>Tìm kiếm </button>
						</form>
					</div>
					<div style="opacity: .5;">
						<i class="fas fa-info-circle" style="margin-right: 15px;margin-left: 5px;color: gray; float: left;"></i>
					</div>
					<div class="text-media-contribution">
						<p style="margin-bottom: 0px;">Bạn có thể thu hẹp vào những vấn đề cụ thể. </p>
						<p> Vui lòng sử dụng nó để chia sẻ thông tin quan sát điểm cố định bằng cách đánh dấu trang.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-----x----- main -----x----->
@endsection
@section('footer_js')
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/lang/ja_JP.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

	<!-- Chart code -->
	<script>
	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create("chartdiv", am4charts.XYChart);
	chart.language.locale = am4lang_ja_JP;

	var data = [];
	var value = 0;
	var postStatistic = {!! (isset($postStatistic) && !is_null($postStatistic)) ? json_encode($postStatistic->toArray()) : json_encode([]) !!};
	if(typeof(postStatistic.view_log) != "undefined" && postStatistic.view_log !== null){
		data = postStatistic.view_log;
	}
	chart.data = data;

	// Create axes
	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.minGridDistance = 150;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.min = 0;
	valueAxis.renderer.labels.template.adapter.add("text", function(text, target) {
	  return text.match(/\./) ? "" : text;
	});

	// Create series
	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = "views";
	series.dataFields.dateX = "date";
	series.tooltipText = "{views} views"

	series.tooltip.pointerOrientation = "vertical";

	chart.cursor = new am4charts.XYCursor();
	chart.cursor.snapToSeries = series;
	chart.cursor.xAxis = dateAxis;

	chart.scrollbarX = new am4charts.XYChartScrollbar();
	chart.scrollbarX.series.push(series);

	}); // end am4core.ready()
	</script>
@endsection