@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main" >
		<div class="post-edit" style="padding: 25px 0;">
			<h3>Cài đặt tài khoản</h3>
			<h6 style="font-weight: bold;">Quản lý địa chỉ email để thông báo cho bạn về các thắc mắc.</h6>
			@include('admin.parts.alert')
		</div>
		<div class= setting-account>
			<p>
				<!-- メディアにお問い合わせが実施されると、サポートメールアドレスに対してシステムから通知メールが送信されます。お問い合わせ機能の詳細については「<span>お問い合わせ管理</span>」画面の説明もあわせてご参照ください。 -->
				Khi bạn liên hệ với đơn vị truyền thông, hệ thống sẽ gửi email thông báo đến địa chỉ email hỗ trợ của bạn. Để biết chi tiết về chức năng yêu cầu, vui lòng tham khảo giải thích trên màn hình "<span> Quản lý yêu cầu </span>".
			</p>
		</div>
		<div class="setting-account-table table-responsive-md">
			<table class="table">
			 	<tr>
					<th>Địa chỉ email hỗ trợ</th>
					<th style="width: 200px;">Trạng thái</th>
					<th style="width: 250px;"></th>
				</tr>
			  @if(count($supportEmails))
			  	@foreach($supportEmails as $supportEmail)
					  <tr>
					    <td>{{ $supportEmail->email }}</td>
					    <td>{{ ($supportEmail->is_approvide) ? __('Đã xác nhận') : __('Đang chờ xác minh') }}</td>
					    <td><a href="{{ route('admin.supportemail.destroy', $supportEmail) }}" style="color:#C43638 "><i class="fas fa-exclamation-triangle"></i><span>&nbsp;Xóa bỏ</span></a></td>
					  </tr>
					@endforeach
			  @endif
			</table>
		</div>
		<div style="margin-top: 20px;">
			<form action="{{ route('admin.supportemail.store') }}" method="post">
				{{ csrf_field() }}
				<input type="text" class="form-control mb-3" placeholder="user@example.com" name="email" style="width: 250px; font-size: 12px;">
				<button class="btn-submit btn" style="border-radius: 0;">Cập nhật</button>
			</form>
		</div>
		<div class="d-flex" style="margin-left: 15px; margin-top: 25px;" >
			<div>
				<i class="fas fa-info-circle" style="color: gray;"></i>
			</div>
			<div class="text-account">
				<!-- <p>追加されたメールアドレスに確認メールが送信されます。メールに記載されているURLにアクセスすると、転送先として利用可能になります。</p>
				<p>サポートメールアドレスは10件まで登録できます。</p>
				<p>このメールアドレスがメディアのサイト上で公開されることはありません。</p> -->
				<p>Một email xác nhận sẽ được gửi đến địa chỉ email đã thêm. Nếu bạn truy cập URL trong email, bạn có thể sử dụng URL đó làm đích chuyển tiếp</p>
				<p>Bạn có thể đăng ký tối đa 10 địa chỉ email hỗ trợ</p>
				<p>Địa chỉ email này sẽ không được công bố trên các trang web truyền thông</p>
			</div>
		</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection