<?php
return [
	'entry' => [
		'post' => '投稿',
		'tag' => 'タグ',
		'category' => 'カテゴリー',
		'menu_tag' => 'メニュータグ',
		'menu' => 'メニュー',
		'user' => 'ユーザー',
		'inquiry' => 'inquiry',
		'support_email' => 'サポートメール',
		'media' => 'メディア',
		'setting' => '設定',
		'page' => 'ページ',
		'vote' => 'アンケート',
	],
	'entries' => [
		'post' => '投稿',
		'tag' => 'タグ',
		'category' => 'カテゴリー',
		'menu_tag' => 'メニュータグ',
		'menu' => 'メニュー',
		'user' => 'ユーザー',
		'inquiry' => 'inquiries',
		'support_email' => 'サポートメール',
		'media' => 'メディア',
		'setting' => '設定',
		'page' => 'ページ',
		'vote' => 'アンケート',
	]
];