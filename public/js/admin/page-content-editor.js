jQuery('document').ready(function($){
  if($('#editor').length > 0){
    var base_url = window.location.origin;
    ClassicEditor.create( document.querySelector( '#editor' ), {
      toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', '|', 'indent', 'outdent', '|', 'blockQuote', 'insertTable', 'mediaEmbed', 'undo', 'redo' ],
      mediaEmbed: {
        previewsInData: true,
        extraProviders: [
          {
            name: 'adiva',
            url: /^(.*)\/([\w-]+)/,
            html: match => {
              const url = match[ 0 ];
              if(url.indexOf('https://aidea.style') != -1){
                var iframeUrl = url + '/embed/';
              }else{
                var iframeUrl = url + '?view=embed';
              }              
              return (
                '<div class="iframely-embed">' +
                    '<div class="iframely-responsive">' +
                        `<iframe src="${ iframeUrl }" ` +
                            'frameborder="0" width="525" height="270">' +
                        '</iframe>' +
                    '</div>' +
                '</div>'
              );
            }
          },
          {
            name: 'any',
            url: /.+/,
            html: match => {
              const url = match[ 0 ];
              var iframeUrl = url;
              return (
                '<div class="iframely-embed">' +
                    '<div class="iframely-responsive">' +
                        `<iframe src="${ iframeUrl }" ` +
                            'frameborder="0" width="525" height="270">' +
                        '</iframe>' +
                    '</div>' +
                '</div>'
              );
            }
          }
        ]
      },
      language: 'ja'
    } )
    .then( editor => {
      console.log( editor );
    } )
    .catch( error => {
      console.error( error );
    } );
  }
});