// $(function(){
// 	$('.instagram-posts-scrolls').infiniteslide({
// 		speed: 50,
// 		pauseonhover: false
// 	});
// });

$(document).ready(function() {

	$('#page-top').click(function() {
		$('html, body').animate({scrollTop: 0}, 500);
	});

	$(document).on('click', 'a[href^="#"]', function (event) {
		var bodyHeight = $('body').height();
		var target = $(this.getAttribute('href'));
		console.log(bodyHeight);

		if (target.length) {
			event.preventDefault();
			$('html, body').stop().animate({
				scrollTop: target.offset().top - bodyHeight
			}, 800);
		}
	});
});

function changeHambugerBar() {
	document.getElementById("menu-bar").classList.toggle("change")
}
$('.halud-slide-show .item').hover(function(){
    let dataImages = $(this).attr('data-images');
    $('.halud-slide-show').css("background-image",`url(${dataImages})`);
});

$(".view a").on('click', function(){
    $('.products ul').toggleClass('list');
    return false;
});

$('.not-dqtab .next').click(function(e){

    var count = 0
    $(this).parents('.content').find('.tab-content').each(function(e){
        count +=1;
    })

    var str = $(this).parent().find('.tab-titlexs').attr('data-tab'),
        res = str.replace("tab-", ""),
        datasection = $(this).closest('.not-dqtab').attr('data-section');
    res = Number(res);
    if(res < count){
        var current = res + 1;
    }else{
        var current = 1;
    }
    action(current,datasection);
})
$('.not-dqtab .prev').click(function(e){
    var count = 0
    $(this).parents('.content').find('.tab-content').each(function(e){
        count +=1;
    })

    var str = $(this).parent().find('.tab-titlexs').attr('data-tab'),
        res = str.replace("tab-", ""),
        datasection = $(this).closest('.not-dqtab').attr('data-section'),
        res = Number(res);
    if(res > 1){
        var current = res - 1;
    }else{
        var current = count;
    }
    action(current,datasection);
})