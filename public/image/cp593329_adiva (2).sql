-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 25, 2020 at 02:01 PM
-- Server version: 5.6.43
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp593329_adiva`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `featured_image`, `sub_featured_image`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Feature', NULL, NULL, NULL, '2020-01-20 15:52:14', '2020-01-20 15:52:14'),
(2, 'Django', '/storage/media/original/1587792421_1579503207_feature-1-2.jpg', '/storage/media/original/1587792435_1579503224_feature-1-1.jpg', 1, '2020-01-20 15:53:49', '2020-04-25 14:27:25'),
(3, 'SpeedFight', '/storage/media/original/1587792478_1579503447_feature-2-2.jpg', '/storage/media/original/1587792490_1579503468_feature-2-1.jpg', 1, '2020-01-20 15:57:54', '2020-04-25 14:28:16'),
(4, 'CityStar', '/storage/media/original/1587792512_1579503556_feature-3-2.jpg', '/storage/media/original/1587792522_1579503581_feature-3-1.jpg', 1, '2020-01-20 15:59:47', '2020-04-25 14:28:45'),
(6, 'Series', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', NULL, '2020-01-20 16:06:14', '2020-04-25 17:59:31'),
(7, '電動バイクで地球を救う', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', '/storage/media/original/1587805113_1579504191_IMG_5772.jpg', 6, '2020-01-20 16:11:43', '2020-04-25 18:08:14'),
(8, 'プジョーブランド', '/storage/media/original/1587805215_1578910851_23.jpg', '/storage/media/original/1587805215_1578910851_23.jpg', 6, '2020-01-20 16:13:40', '2020-04-25 18:02:41'),
(9, '初心者ライダーが乗ってみた！', '/storage/media/original/1587805461_1579504621_IMG_7865-2.jpg', '/storage/media/original/1587805461_1579504621_IMG_7865-2.jpg', 6, '2020-01-20 16:17:27', '2020-04-25 18:04:41'),
(10, '電動バイクの夢を見るか？', '/storage/media/original/1587805503_1578908597_1.jpg', '/storage/media/original/1587805503_1578908597_1.jpg', 6, '2020-01-20 16:19:16', '2020-04-25 18:05:11'),
(11, '清水まどかのカメラ修行', '/storage/media/original/1587805535_1579504857_S__24739851.jpg', '/storage/media/original/1587805535_1579504857_S__24739851.jpg', 6, '2020-01-20 16:21:18', '2020-04-25 18:05:44'),
(12, '正規販売店紹介', '/storage/media/original/1587805562_1579505079_aidea_showroom.jpg', '/storage/media/original/1587805562_1579505079_aidea_showroom.jpg', 6, '2020-01-20 16:25:00', '2020-04-25 18:07:12'),
(13, '不二子ちゃんになりたい', '/storage/media/original/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', '/storage/media/original/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 6, '2020-01-20 16:27:44', '2020-04-25 18:07:34'),
(14, 'VX-1', NULL, NULL, 1, '2020-07-08 16:04:13', '2020-07-08 16:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `work_status` enum('acknowledged','incompatible','ignore') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'acknowledged',
  `agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_note` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xlarge_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `large_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medium_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `small_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ratio3x2_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ratio4x3_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_cover` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `name`, `info`, `original_path`, `xlarge_path`, `large_path`, `medium_path`, `small_path`, `thumbnail_path`, `created_at`, `updated_at`, `post_id`, `ratio3x2_path`, `ratio4x3_path`, `is_cover`) VALUES
(1, 'adiva.png', '200x100 - 6.88 KB - image/png', 'media/original/1587787134_adiva.png', NULL, NULL, NULL, 'media/small/1587787134_adiva.png', 'media/thumbnail/1587787134_adiva.png', '2020-04-25 12:58:54', '2020-04-25 12:58:54', NULL, 'media/ratio3x2/1587787134_adiva.png', 'media/ratio4x3/1587787134_adiva.png', 0),
(2, 'adiva.png', '200x100 - 6.88 KB - image/png', 'media/original/1587787284_adiva.png', NULL, NULL, NULL, 'media/small/1587787284_adiva.png', 'media/thumbnail/1587787284_adiva.png', '2020-04-25 13:01:24', '2020-04-25 13:01:24', NULL, 'media/ratio3x2/1587787284_adiva.png', 'media/ratio4x3/1587787284_adiva.png', 0),
(3, 'adiva.png', '200x100 - 6.88 KB - image/png', 'media/original/1587787730_adiva.png', NULL, NULL, NULL, 'media/small/1587787730_adiva.png', 'media/thumbnail/1587787730_adiva.png', '2020-04-25 13:08:50', '2020-04-25 13:08:50', NULL, 'media/ratio3x2/1587787730_adiva.png', 'media/ratio4x3/1587787730_adiva.png', 0),
(4, 'anh_link.png', '954x824 - 1,000.45 KB - image/png', 'media/original/1587787919_anh_link.png', NULL, 'media/large/1587787919_anh_link.png', 'media/medium/1587787919_anh_link.png', 'media/small/1587787919_anh_link.png', 'media/thumbnail/1587787919_anh_link.png', '2020-04-25 13:12:01', '2020-04-25 13:12:01', NULL, 'media/ratio3x2/1587787919_anh_link.png', 'media/ratio4x3/1587787919_anh_link.png', 0),
(5, 'peugot.png', '200x100 - 11.15 KB - image/png', 'media/original/1587788663_peugot.png', NULL, NULL, NULL, 'media/small/1587788663_peugot.png', 'media/thumbnail/1587788663_peugot.png', '2020-04-25 13:24:23', '2020-04-25 13:24:23', NULL, 'media/ratio3x2/1587788663_peugot.png', 'media/ratio4x3/1587788663_peugot.png', 0),
(6, 'ruby-cover_MG_3820-768x394.jpg', '768x394 - 17.28 KB - image/jpeg', 'media/original/1587788831_ruby-cover_MG_3820-768x394.jpg', NULL, 'media/large/1587788831_ruby-cover_MG_3820-768x394.jpg', 'media/medium/1587788831_ruby-cover_MG_3820-768x394.jpg', 'media/small/1587788831_ruby-cover_MG_3820-768x394.jpg', 'media/thumbnail/1587788831_ruby-cover_MG_3820-768x394.jpg', '2020-04-25 13:27:11', '2020-04-25 13:27:11', NULL, 'media/ratio3x2/1587788831_ruby-cover_MG_3820-768x394.jpg', 'media/ratio4x3/1587788831_ruby-cover_MG_3820-768x394.jpg', 0),
(7, '1579503085_preview.jpg', '500x180 - 62.89 KB - image/jpeg', 'media/original/1587792289_1579503085_preview.jpg', NULL, NULL, 'media/medium/1587792289_1579503085_preview.jpg', 'media/small/1587792289_1579503085_preview.jpg', 'media/thumbnail/1587792289_1579503085_preview.jpg', '2020-04-25 14:24:49', '2020-04-25 14:24:49', NULL, 'media/ratio3x2/1587792289_1579503085_preview.jpg', 'media/ratio4x3/1587792289_1579503085_preview.jpg', 0),
(8, '1579503855_preview-1.jpg', '500x180 - 56.98 KB - image/jpeg', 'media/original/1587792381_1579503855_preview-1.jpg', NULL, NULL, 'media/medium/1587792381_1579503855_preview-1.jpg', 'media/small/1587792381_1579503855_preview-1.jpg', 'media/thumbnail/1587792381_1579503855_preview-1.jpg', '2020-04-25 14:26:21', '2020-04-25 14:26:21', NULL, 'media/ratio3x2/1587792381_1579503855_preview-1.jpg', 'media/ratio4x3/1587792381_1579503855_preview-1.jpg', 0),
(9, '1579503207_feature-1-2.jpg', '600x450 - 200.65 KB - image/jpeg', 'media/original/1587792421_1579503207_feature-1-2.jpg', NULL, NULL, 'media/medium/1587792421_1579503207_feature-1-2.jpg', 'media/small/1587792421_1579503207_feature-1-2.jpg', 'media/thumbnail/1587792421_1579503207_feature-1-2.jpg', '2020-04-25 14:27:01', '2020-04-25 14:27:01', NULL, 'media/ratio3x2/1587792421_1579503207_feature-1-2.jpg', 'media/ratio4x3/1587792421_1579503207_feature-1-2.jpg', 0),
(10, '1579503224_feature-1-1.jpg', '600x220 - 69.57 KB - image/jpeg', 'media/original/1587792435_1579503224_feature-1-1.jpg', NULL, NULL, 'media/medium/1587792435_1579503224_feature-1-1.jpg', 'media/small/1587792435_1579503224_feature-1-1.jpg', 'media/thumbnail/1587792435_1579503224_feature-1-1.jpg', '2020-04-25 14:27:15', '2020-04-25 14:27:15', NULL, 'media/ratio3x2/1587792435_1579503224_feature-1-1.jpg', 'media/ratio4x3/1587792435_1579503224_feature-1-1.jpg', 0),
(11, '1579503447_feature-2-2.jpg', '600x450 - 135.16 KB - image/jpeg', 'media/original/1587792478_1579503447_feature-2-2.jpg', NULL, NULL, 'media/medium/1587792478_1579503447_feature-2-2.jpg', 'media/small/1587792478_1579503447_feature-2-2.jpg', 'media/thumbnail/1587792478_1579503447_feature-2-2.jpg', '2020-04-25 14:27:59', '2020-04-25 14:27:59', NULL, 'media/ratio3x2/1587792478_1579503447_feature-2-2.jpg', 'media/ratio4x3/1587792478_1579503447_feature-2-2.jpg', 0),
(12, '1579503468_feature-2-1.jpg', '600x220 - 55.89 KB - image/jpeg', 'media/original/1587792490_1579503468_feature-2-1.jpg', NULL, NULL, 'media/medium/1587792490_1579503468_feature-2-1.jpg', 'media/small/1587792490_1579503468_feature-2-1.jpg', 'media/thumbnail/1587792490_1579503468_feature-2-1.jpg', '2020-04-25 14:28:10', '2020-04-25 14:28:10', NULL, 'media/ratio3x2/1587792490_1579503468_feature-2-1.jpg', 'media/ratio4x3/1587792490_1579503468_feature-2-1.jpg', 0),
(13, '1579503556_feature-3-2.jpg', '600x450 - 157.99 KB - image/jpeg', 'media/original/1587792512_1579503556_feature-3-2.jpg', NULL, NULL, 'media/medium/1587792512_1579503556_feature-3-2.jpg', 'media/small/1587792512_1579503556_feature-3-2.jpg', 'media/thumbnail/1587792512_1579503556_feature-3-2.jpg', '2020-04-25 14:28:32', '2020-04-25 14:28:32', NULL, 'media/ratio3x2/1587792512_1579503556_feature-3-2.jpg', 'media/ratio4x3/1587792512_1579503556_feature-3-2.jpg', 0),
(14, '1579503581_feature-3-1.jpg', '600x220 - 65.13 KB - image/jpeg', 'media/original/1587792522_1579503581_feature-3-1.jpg', NULL, NULL, 'media/medium/1587792522_1579503581_feature-3-1.jpg', 'media/small/1587792522_1579503581_feature-3-1.jpg', 'media/thumbnail/1587792522_1579503581_feature-3-1.jpg', '2020-04-25 14:28:42', '2020-04-25 14:28:42', NULL, 'media/ratio3x2/1587792522_1579503581_feature-3-1.jpg', 'media/ratio4x3/1587792522_1579503581_feature-3-1.jpg', 0),
(15, '1579503643_p5.jpg', '600x450 - 160.13 KB - image/jpeg', 'media/original/1587792537_1579503643_p5.jpg', NULL, NULL, 'media/medium/1587792537_1579503643_p5.jpg', 'media/small/1587792537_1579503643_p5.jpg', 'media/thumbnail/1587792537_1579503643_p5.jpg', '2020-04-25 14:28:57', '2020-04-25 14:28:57', NULL, 'media/ratio3x2/1587792537_1579503643_p5.jpg', 'media/ratio4x3/1587792537_1579503643_p5.jpg', 0),
(16, '1579504191_IMG_5772.jpg', '2641x1981 - 2.05 MB - image/jpeg', 'media/original/1587805113_1579504191_IMG_5772.jpg', 'media/xlarge/1587805113_1579504191_IMG_5772.jpg', 'media/large/1587805113_1579504191_IMG_5772.jpg', 'media/medium/1587805113_1579504191_IMG_5772.jpg', 'media/small/1587805113_1579504191_IMG_5772.jpg', 'media/thumbnail/1587805113_1579504191_IMG_5772.jpg', '2020-04-25 17:58:45', '2020-04-25 17:58:45', NULL, 'media/ratio3x2/1587805113_1579504191_IMG_5772.jpg', 'media/ratio4x3/1587805113_1579504191_IMG_5772.jpg', 0),
(17, '1578910851_23.jpg', '1280x853 - 226.73 KB - image/jpeg', 'media/original/1587805215_1578910851_23.jpg', 'media/xlarge/1587805215_1578910851_23.jpg', 'media/large/1587805215_1578910851_23.jpg', 'media/medium/1587805215_1578910851_23.jpg', 'media/small/1587805215_1578910851_23.jpg', 'media/thumbnail/1587805215_1578910851_23.jpg', '2020-04-25 18:00:16', '2020-04-25 18:00:16', NULL, 'media/ratio3x2/1587805215_1578910851_23.jpg', 'media/ratio4x3/1587805215_1578910851_23.jpg', 0),
(18, '1579504621_IMG_7865-2.jpg', '3345x2230 - 2.40 MB - image/jpeg', 'media/original/1587805461_1579504621_IMG_7865-2.jpg', 'media/xlarge/1587805461_1579504621_IMG_7865-2.jpg', 'media/large/1587805461_1579504621_IMG_7865-2.jpg', 'media/medium/1587805461_1579504621_IMG_7865-2.jpg', 'media/small/1587805461_1579504621_IMG_7865-2.jpg', 'media/thumbnail/1587805461_1579504621_IMG_7865-2.jpg', '2020-04-25 18:04:32', '2020-04-25 18:04:32', NULL, 'media/ratio3x2/1587805461_1579504621_IMG_7865-2.jpg', 'media/ratio4x3/1587805461_1579504621_IMG_7865-2.jpg', 0),
(19, '1578908597_1.jpg', '1280x960 - 309.93 KB - image/jpeg', 'media/original/1587805503_1578908597_1.jpg', 'media/xlarge/1587805503_1578908597_1.jpg', 'media/large/1587805503_1578908597_1.jpg', 'media/medium/1587805503_1578908597_1.jpg', 'media/small/1587805503_1578908597_1.jpg', 'media/thumbnail/1587805503_1578908597_1.jpg', '2020-04-25 18:05:04', '2020-04-25 18:05:04', NULL, 'media/ratio3x2/1587805503_1578908597_1.jpg', 'media/ratio4x3/1587805503_1578908597_1.jpg', 0),
(20, '1579504857_S__24739851.jpg', '1478x1108 - 671.57 KB - image/jpeg', 'media/original/1587805535_1579504857_S__24739851.jpg', 'media/xlarge/1587805535_1579504857_S__24739851.jpg', 'media/large/1587805535_1579504857_S__24739851.jpg', 'media/medium/1587805535_1579504857_S__24739851.jpg', 'media/small/1587805535_1579504857_S__24739851.jpg', 'media/thumbnail/1587805535_1579504857_S__24739851.jpg', '2020-04-25 18:05:37', '2020-04-25 18:05:37', NULL, 'media/ratio3x2/1587805535_1579504857_S__24739851.jpg', 'media/ratio4x3/1587805535_1579504857_S__24739851.jpg', 0),
(21, '1579505079_aidea_showroom.jpg', '1654x1102 - 1.47 MB - image/jpeg', 'media/original/1587805562_1579505079_aidea_showroom.jpg', 'media/xlarge/1587805562_1579505079_aidea_showroom.jpg', 'media/large/1587805562_1579505079_aidea_showroom.jpg', 'media/medium/1587805562_1579505079_aidea_showroom.jpg', 'media/small/1587805562_1579505079_aidea_showroom.jpg', 'media/thumbnail/1587805562_1579505079_aidea_showroom.jpg', '2020-04-25 18:06:08', '2020-04-25 18:06:08', NULL, 'media/ratio3x2/1587805562_1579505079_aidea_showroom.jpg', 'media/ratio4x3/1587805562_1579505079_aidea_showroom.jpg', 0),
(22, '1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', '1280x853 - 413.50 KB - image/jpeg', 'media/original/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/xlarge/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/large/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/medium/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/small/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/thumbnail/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', '2020-04-25 18:07:27', '2020-04-25 18:07:27', NULL, 'media/ratio3x2/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 'media/ratio4x3/1587805646_1579505247_c68970e6b34f94861ce933102eb3fd333bbd3455_xlarge-1.jpg', 0),
(23, 'IMG_5622.jpg', '1598x1198 - 529.53 KB - image/jpeg', 'media/original/1587808490_IMG_5622.jpg', 'media/xlarge/1587808490_IMG_5622.jpg', 'media/large/1587808490_IMG_5622.jpg', 'media/medium/1587808490_IMG_5622.jpg', 'media/small/1587808490_IMG_5622.jpg', 'media/thumbnail/1587808490_IMG_5622.jpg', '2020-04-25 18:54:58', '2020-04-25 18:54:58', NULL, 'media/ratio3x2/1587808490_IMG_5622.jpg', 'media/ratio4x3/1587808490_IMG_5622.jpg', 0),
(24, 'IMG_5443-768x576.jpg', '768x576 - 126.77 KB - image/jpeg', 'media/original/1587809480_IMG_5443-768x576.jpg', NULL, 'media/large/1587809480_IMG_5443-768x576.jpg', 'media/medium/1587809480_IMG_5443-768x576.jpg', 'media/small/1587809480_IMG_5443-768x576.jpg', 'media/thumbnail/1587809480_IMG_5443-768x576.jpg', '2020-04-25 19:11:20', '2020-04-25 19:11:20', NULL, 'media/ratio3x2/1587809480_IMG_5443-768x576.jpg', 'media/ratio4x3/1587809480_IMG_5443-768x576.jpg', 0),
(25, '1579505079_aidea_showroom.jpg', '1654x1102 - 1.47 MB - image/jpeg', 'media/original/1587876370_1579505079_aidea_showroom.jpg', 'media/xlarge/1587876370_1579505079_aidea_showroom.jpg', 'media/large/1587876370_1579505079_aidea_showroom.jpg', 'media/medium/1587876370_1579505079_aidea_showroom.jpg', 'media/small/1587876370_1579505079_aidea_showroom.jpg', 'media/thumbnail/1587876370_1579505079_aidea_showroom.jpg', '2020-04-26 13:46:15', '2020-04-26 13:46:15', NULL, 'media/ratio3x2/1587876370_1579505079_aidea_showroom.jpg', 'media/ratio4x3/1587876370_1579505079_aidea_showroom.jpg', 1),
(26, 'f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', '768x512 - 77.24 KB - image/jpeg', 'media/original/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', NULL, 'media/large/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', 'media/medium/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', 'media/small/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', 'media/thumbnail/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', '2020-04-26 13:47:38', '2020-04-26 13:47:38', NULL, 'media/ratio3x2/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', 'media/ratio4x3/1587876457_f63a34862d7cccda73be7a29c5eb23b66918ac0d_xlarge-768x512.jpg', 0),
(27, 'LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', '768x576 - 85.85 KB - image/jpeg', 'media/original/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', NULL, 'media/large/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', 'media/medium/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', 'media/small/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', 'media/thumbnail/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', '2020-04-26 13:48:35', '2020-04-26 13:48:35', NULL, 'media/ratio3x2/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', 'media/ratio4x3/1587876514_LtZEknFnTzsaKts1574758669_1574758805-768x576.jpg', 0),
(28, 'django_125_tricolore_abs_1600-768x408.jpg', '768x408 - 97.48 KB - image/jpeg', 'media/original/1587876677_django_125_tricolore_abs_1600-768x408.jpg', NULL, 'media/large/1587876677_django_125_tricolore_abs_1600-768x408.jpg', 'media/medium/1587876677_django_125_tricolore_abs_1600-768x408.jpg', 'media/small/1587876677_django_125_tricolore_abs_1600-768x408.jpg', 'media/thumbnail/1587876677_django_125_tricolore_abs_1600-768x408.jpg', '2020-04-26 13:51:17', '2020-04-26 13:51:17', NULL, 'media/ratio3x2/1587876677_django_125_tricolore_abs_1600-768x408.jpg', 'media/ratio4x3/1587876677_django_125_tricolore_abs_1600-768x408.jpg', 0),
(29, 'IMG_5441-768x576.jpeg', '768x576 - 67.53 KB - image/jpeg', 'media/original/1587878557_IMG_5441-768x576.jpeg', NULL, 'media/large/1587878557_IMG_5441-768x576.jpeg', 'media/medium/1587878557_IMG_5441-768x576.jpeg', 'media/small/1587878557_IMG_5441-768x576.jpeg', 'media/thumbnail/1587878557_IMG_5441-768x576.jpeg', '2020-04-26 14:22:37', '2020-04-26 14:22:37', NULL, 'media/ratio3x2/1587878557_IMG_5441-768x576.jpeg', 'media/ratio4x3/1587878557_IMG_5441-768x576.jpeg', 0),
(30, 'IMG_5443-768x576.jpg', '768x576 - 126.77 KB - image/jpeg', 'media/original/1587878564_IMG_5443-768x576.jpg', NULL, 'media/large/1587878564_IMG_5443-768x576.jpg', 'media/medium/1587878564_IMG_5443-768x576.jpg', 'media/small/1587878564_IMG_5443-768x576.jpg', 'media/thumbnail/1587878564_IMG_5443-768x576.jpg', '2020-04-26 14:22:45', '2020-04-26 14:22:45', NULL, 'media/ratio3x2/1587878564_IMG_5443-768x576.jpg', 'media/ratio4x3/1587878564_IMG_5443-768x576.jpg', 0),
(31, 'Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', '768x512 - 111.82 KB - image/jpeg', 'media/original/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', NULL, 'media/large/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', 'media/medium/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', 'media/small/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', 'media/thumbnail/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', '2020-04-28 00:25:22', '2020-04-28 00:25:22', NULL, 'media/ratio3x2/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', 'media/ratio4x3/1588001119_Bwnc0VZfJuUpX7s1574754107_1574754184-768x512.jpg', 0),
(32, '４-768x576.jpg', '768x576 - 74.13 KB - image/jpeg', 'media/original/1588001206_４-768x576.jpg', NULL, 'media/large/1588001206_４-768x576.jpg', 'media/medium/1588001206_４-768x576.jpg', 'media/small/1588001206_４-768x576.jpg', 'media/thumbnail/1588001206_４-768x576.jpg', '2020-04-28 00:26:47', '2020-04-28 00:26:47', NULL, 'media/ratio3x2/1588001206_４-768x576.jpg', 'media/ratio4x3/1588001206_４-768x576.jpg', 0),
(33, '2019_EV_AA-4_SIL_Acition_14-768x512.jpg', '768x512 - 129.50 KB - image/jpeg', 'media/original/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', NULL, 'media/large/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', 'media/medium/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', 'media/small/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', 'media/thumbnail/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', '2020-04-28 00:26:50', '2020-04-28 00:26:50', NULL, 'media/ratio3x2/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', 'media/ratio4x3/1588001210_2019_EV_AA-4_SIL_Acition_14-768x512.jpg', 0),
(35, 'IMG_2307-768x576.jpeg', '768x576 - 101.58 KB - image/jpeg', 'media/original/1588001222_IMG_2307-768x576.jpeg', NULL, 'media/large/1588001222_IMG_2307-768x576.jpeg', 'media/medium/1588001222_IMG_2307-768x576.jpeg', 'media/small/1588001222_IMG_2307-768x576.jpeg', 'media/thumbnail/1588001222_IMG_2307-768x576.jpeg', '2020-04-28 00:27:02', '2020-04-28 00:27:02', NULL, 'media/ratio3x2/1588001222_IMG_2307-768x576.jpeg', 'media/ratio4x3/1588001222_IMG_2307-768x576.jpeg', 0),
(36, 'moto_dig_2.jpg', '1280x829 - 133.33 KB - image/jpeg', 'media/original/1588060644_moto_dig_2.jpg', 'media/xlarge/1588060644_moto_dig_2.jpg', 'media/large/1588060644_moto_dig_2.jpg', 'media/medium/1588060644_moto_dig_2.jpg', 'media/small/1588060644_moto_dig_2.jpg', 'media/thumbnail/1588060644_moto_dig_2.jpg', '2020-04-28 16:57:26', '2020-04-28 16:57:26', NULL, 'media/ratio3x2/1588060644_moto_dig_2.jpg', 'media/ratio4x3/1588060644_moto_dig_2.jpg', 1),
(37, '2.image_30.jpg', '1280x853 - 113.44 KB - image/jpeg', 'media/original/1588060757_2.image_30.jpg', 'media/xlarge/1588060757_2.image_30.jpg', 'media/large/1588060757_2.image_30.jpg', 'media/medium/1588060757_2.image_30.jpg', 'media/small/1588060757_2.image_30.jpg', 'media/thumbnail/1588060757_2.image_30.jpg', '2020-04-28 16:59:18', '2020-04-28 16:59:18', NULL, 'media/ratio3x2/1588060757_2.image_30.jpg', 'media/ratio4x3/1588060757_2.image_30.jpg', 0),
(38, '2.image_31.jpg', '1280x853 - 91.56 KB - image/jpeg', 'media/original/1588060758_2.image_31.jpg', 'media/xlarge/1588060758_2.image_31.jpg', 'media/large/1588060758_2.image_31.jpg', 'media/medium/1588060758_2.image_31.jpg', 'media/small/1588060758_2.image_31.jpg', 'media/thumbnail/1588060758_2.image_31.jpg', '2020-04-28 16:59:22', '2020-04-28 16:59:22', NULL, 'media/ratio3x2/1588060758_2.image_31.jpg', 'media/ratio4x3/1588060758_2.image_31.jpg', 0),
(40, '2.image_9.jpg', '1280x853 - 78.98 KB - image/jpeg', 'media/original/1588066627_2.image_9.jpg', 'media/xlarge/1588066627_2.image_9.jpg', 'media/large/1588066627_2.image_9.jpg', 'media/medium/1588066627_2.image_9.jpg', 'media/small/1588066627_2.image_9.jpg', 'media/thumbnail/1588066627_2.image_9.jpg', '2020-04-28 18:37:08', '2020-04-28 18:37:08', NULL, 'media/ratio3x2/1588066627_2.image_9.jpg', 'media/ratio4x3/1588066627_2.image_9.jpg', 1),
(41, '2.image_31.jpg', '1280x853 - 91.56 KB - image/jpeg', 'media/original/1588071792_2.image_31.jpg', 'media/xlarge/1588071792_2.image_31.jpg', 'media/large/1588071792_2.image_31.jpg', 'media/medium/1588071792_2.image_31.jpg', 'media/small/1588071792_2.image_31.jpg', 'media/thumbnail/1588071792_2.image_31.jpg', '2020-04-28 20:03:13', '2020-04-28 20:03:13', NULL, 'media/ratio3x2/1588071792_2.image_31.jpg', 'media/ratio4x3/1588071792_2.image_31.jpg', 1),
(42, 'insta.png', '585x306 - 92.46 KB - image/png', 'media/original/1588210619_insta.png', NULL, NULL, 'media/medium/1588210619_insta.png', 'media/small/1588210619_insta.png', 'media/thumbnail/1588210619_insta.png', '2020-04-30 10:36:59', '2020-04-30 10:36:59', NULL, 'media/ratio3x2/1588210619_insta.png', 'media/ratio4x3/1588210619_insta.png', 0),
(43, 'insta.png', '585x306 - 92.46 KB - image/png', 'media/original/1588212495_insta.png', NULL, NULL, 'media/medium/1588212495_insta.png', 'media/small/1588212495_insta.png', 'media/thumbnail/1588212495_insta.png', '2020-04-30 11:08:15', '2020-04-30 11:08:15', NULL, 'media/ratio3x2/1588212495_insta.png', 'media/ratio4x3/1588212495_insta.png', 0),
(44, 'スクリーンショット 2020-04-30 15.22.40.png', '2220x1166 - 1,010.18 KB - image/png', 'media/original/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/xlarge/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/large/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/medium/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/small/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/thumbnail/1588227950_スクリーンショット 2020-04-30 15.22.40.png', '2020-04-30 15:25:54', '2020-04-30 15:25:54', NULL, 'media/ratio3x2/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 'media/ratio4x3/1588227950_スクリーンショット 2020-04-30 15.22.40.png', 0),
(45, 'cropped-aidea_logo_vertical_1c_01-1-192x192.png', '192x192 - 22.67 KB - image/png', 'media/original/1588517999_cropped-aidea_logo_vertical_1c_01-1-192x192.png', NULL, NULL, NULL, 'media/small/1588517999_cropped-aidea_logo_vertical_1c_01-1-192x192.png', 'media/thumbnail/1588517999_cropped-aidea_logo_vertical_1c_01-1-192x192.png', '2020-05-03 23:59:59', '2020-05-03 23:59:59', NULL, 'media/ratio3x2/1588517999_cropped-aidea_logo_vertical_1c_01-1-192x192.png', 'media/ratio4x3/1588517999_cropped-aidea_logo_vertical_1c_01-1-192x192.png', 0),
(46, '1588060644_moto_dig_2.jpg', '1280x829 - 445.48 KB - image/jpeg', 'media/original/1588566331_1588060644_moto_dig_2.jpg', 'media/xlarge/1588566331_1588060644_moto_dig_2.jpg', 'media/large/1588566331_1588060644_moto_dig_2.jpg', 'media/medium/1588566331_1588060644_moto_dig_2.jpg', 'media/small/1588566331_1588060644_moto_dig_2.jpg', 'media/thumbnail/1588566331_1588060644_moto_dig_2.jpg', '2020-05-04 13:25:32', '2020-05-04 13:25:32', NULL, 'media/ratio3x2/1588566331_1588060644_moto_dig_2.jpg', 'media/ratio4x3/1588566331_1588060644_moto_dig_2.jpg', 1),
(47, '01.png', '590x442 - 491.26 KB - image/png', 'media/original/1589450221_01.png', NULL, NULL, 'media/medium/1589450221_01.png', 'media/small/1589450221_01.png', 'media/thumbnail/1589450221_01.png', '2020-05-14 18:57:04', '2020-05-14 18:57:04', 63, 'media/ratio3x2/1589450221_01.png', 'media/ratio4x3/1589450221_01.png', 1),
(48, '01.png', '1204x675 - 1.45 MB - image/png', 'media/original/1589450784_01.png', NULL, 'media/large/1589450784_01.png', 'media/medium/1589450784_01.png', 'media/small/1589450784_01.png', 'media/thumbnail/1589450784_01.png', '2020-05-14 19:06:28', '2020-05-14 19:06:28', 63, 'media/ratio3x2/1589450784_01.png', 'media/ratio4x3/1589450784_01.png', 0),
(49, '02.png', '1202x676 - 1.49 MB - image/png', 'media/original/1589450788_02.png', NULL, 'media/large/1589450788_02.png', 'media/medium/1589450788_02.png', 'media/small/1589450788_02.png', 'media/thumbnail/1589450788_02.png', '2020-05-14 19:06:30', '2020-05-14 19:06:30', 63, 'media/ratio3x2/1589450788_02.png', 'media/ratio4x3/1589450788_02.png', 0),
(50, '03.png', '1202x677 - 1.43 MB - image/png', 'media/original/1589450790_03.png', NULL, 'media/large/1589450790_03.png', 'media/medium/1589450790_03.png', 'media/small/1589450790_03.png', 'media/thumbnail/1589450790_03.png', '2020-05-14 19:06:37', '2020-05-14 19:06:37', 63, 'media/ratio3x2/1589450790_03.png', 'media/ratio4x3/1589450790_03.png', 0),
(52, 'aideaショールーム.jpg', '2362x1574 - 3.62 MB - image/jpeg', 'media/original/1589453906_aideaショールーム.jpg', 'media/xlarge/1589453906_aideaショールーム.jpg', 'media/large/1589453906_aideaショールーム.jpg', 'media/medium/1589453906_aideaショールーム.jpg', 'media/small/1589453906_aideaショールーム.jpg', 'media/thumbnail/1589453906_aideaショールーム.jpg', '2020-05-14 19:58:36', '2020-05-14 19:58:36', 65, 'media/ratio3x2/1589453906_aideaショールーム.jpg', 'media/ratio4x3/1589453906_aideaショールーム.jpg', 1),
(57, '01.jpg', '4496x3000 - 566.54 KB - image/jpeg', 'media/original/1590031008_01.jpg', 'media/xlarge/1590031008_01.jpg', 'media/large/1590031008_01.jpg', 'media/medium/1590031008_01.jpg', 'media/small/1590031008_01.jpg', 'media/thumbnail/1590031008_01.jpg', '2020-05-21 12:17:03', '2020-05-21 12:17:03', 71, 'media/ratio3x2/1590031008_01.jpg', 'media/ratio4x3/1590031008_01.jpg', 1),
(58, '01.jpg', '4496x3000 - 566.54 KB - image/jpeg', 'media/original/1590031344_01.jpg', 'media/xlarge/1590031344_01.jpg', 'media/large/1590031344_01.jpg', 'media/medium/1590031344_01.jpg', 'media/small/1590031344_01.jpg', 'media/thumbnail/1590031344_01.jpg', '2020-05-21 12:22:38', '2020-05-21 12:22:38', 71, 'media/ratio3x2/1590031344_01.jpg', 'media/ratio4x3/1590031344_01.jpg', 0),
(59, '02.jpg', '4496x3000 - 565.46 KB - image/jpeg', 'media/original/1590031358_02.jpg', 'media/xlarge/1590031358_02.jpg', 'media/large/1590031358_02.jpg', 'media/medium/1590031358_02.jpg', 'media/small/1590031358_02.jpg', 'media/thumbnail/1590031358_02.jpg', '2020-05-21 12:22:51', '2020-05-21 12:22:51', 71, 'media/ratio3x2/1590031358_02.jpg', 'media/ratio4x3/1590031358_02.jpg', 0),
(60, '03.jpg', '4496x3000 - 593.65 KB - image/jpeg', 'media/original/1590031371_03.jpg', 'media/xlarge/1590031371_03.jpg', 'media/large/1590031371_03.jpg', 'media/medium/1590031371_03.jpg', 'media/small/1590031371_03.jpg', 'media/thumbnail/1590031371_03.jpg', '2020-05-21 12:23:04', '2020-05-21 12:23:04', 71, 'media/ratio3x2/1590031371_03.jpg', 'media/ratio4x3/1590031371_03.jpg', 0),
(61, '76702362_2328076397414923_416958165284814848_o-1.jpg', '960x684 - 65.05 KB - image/jpeg', 'media/original/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', NULL, 'media/large/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', 'media/medium/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', 'media/small/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', 'media/thumbnail/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', '2020-05-22 13:32:43', '2020-05-22 13:32:43', 70, 'media/ratio3x2/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', 'media/ratio4x3/1590121963_76702362_2328076397414923_416958165284814848_o-1.jpg', 1),
(62, 'nobox_08.jpg', '1240x825 - 134.37 KB - image/jpeg', 'media/original/1590122343_nobox_08.jpg', NULL, 'media/large/1590122343_nobox_08.jpg', 'media/medium/1590122343_nobox_08.jpg', 'media/small/1590122343_nobox_08.jpg', 'media/thumbnail/1590122343_nobox_08.jpg', '2020-05-22 13:39:04', '2020-05-22 13:39:04', NULL, 'media/ratio3x2/1590122343_nobox_08.jpg', 'media/ratio4x3/1590122343_nobox_08.jpg', 1),
(63, 'FireShot Capture 224 -  - adiva.sakura.ne.jp.png', '341x334 - 91.11 KB - image/png', 'media/original/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', NULL, NULL, 'media/medium/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', 'media/small/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', 'media/thumbnail/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', '2020-05-22 15:45:50', '2020-05-22 15:45:50', 76, 'media/ratio3x2/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', 'media/ratio4x3/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png', 0),
(64, 'G.png', '165x209 - 70.39 KB - image/png', 'media/original/1590129950_G.png', NULL, NULL, NULL, 'media/small/1590129950_G.png', 'media/thumbnail/1590129950_G.png', '2020-05-22 15:45:50', '2020-05-22 15:45:50', 76, 'media/ratio3x2/1590129950_G.png', 'media/ratio4x3/1590129950_G.png', 0),
(65, '1579503085_preview.jpg', '500x180 - 62.89 KB - image/jpeg', 'media/original/1590821739_1579503085_preview.jpg', NULL, NULL, 'media/medium/1590821739_1579503085_preview.jpg', 'media/small/1590821739_1579503085_preview.jpg', 'media/thumbnail/1590821739_1579503085_preview.jpg', '2020-05-30 15:55:39', '2020-05-30 15:55:39', 57, 'media/ratio3x2/1590821739_1579503085_preview.jpg', 'media/ratio4x3/1590821739_1579503085_preview.jpg', 0),
(66, 'b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', '1280x960 - 96.67 KB - image/jpeg', 'media/original/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/xlarge/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/large/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/medium/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/small/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/thumbnail/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', '2020-05-30 15:55:50', '2020-05-30 15:55:50', 57, 'media/ratio3x2/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 'media/ratio4x3/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg', 0),
(67, '2020_200604_0004.jpg', '1478x1108 - 418.36 KB - image/jpeg', 'media/original/1591607761_2020_200604_0004.jpg', 'media/xlarge/1591607761_2020_200604_0004.jpg', 'media/large/1591607761_2020_200604_0004.jpg', 'media/medium/1591607761_2020_200604_0004.jpg', 'media/small/1591607761_2020_200604_0004.jpg', 'media/thumbnail/1591607761_2020_200604_0004.jpg', '2020-06-08 18:16:03', '2020-06-08 18:16:03', NULL, 'media/ratio3x2/1591607761_2020_200604_0004.jpg', 'media/ratio4x3/1591607761_2020_200604_0004.jpg', 0),
(68, '2020_200604_0005.jpg', '1478x1108 - 365.86 KB - image/jpeg', 'media/original/1591607763_2020_200604_0005.jpg', 'media/xlarge/1591607763_2020_200604_0005.jpg', 'media/large/1591607763_2020_200604_0005.jpg', 'media/medium/1591607763_2020_200604_0005.jpg', 'media/small/1591607763_2020_200604_0005.jpg', 'media/thumbnail/1591607763_2020_200604_0005.jpg', '2020-06-08 18:16:07', '2020-06-08 18:16:07', NULL, 'media/ratio3x2/1591607763_2020_200604_0005.jpg', 'media/ratio4x3/1591607763_2020_200604_0005.jpg', 0),
(69, '2020_200604_0015.jpg', '1478x1108 - 327.04 KB - image/jpeg', 'media/original/1591607822_2020_200604_0015.jpg', 'media/xlarge/1591607822_2020_200604_0015.jpg', 'media/large/1591607822_2020_200604_0015.jpg', 'media/medium/1591607822_2020_200604_0015.jpg', 'media/small/1591607822_2020_200604_0015.jpg', 'media/thumbnail/1591607822_2020_200604_0015.jpg', '2020-06-08 18:17:03', '2020-06-08 18:17:03', NULL, 'media/ratio3x2/1591607822_2020_200604_0015.jpg', 'media/ratio4x3/1591607822_2020_200604_0015.jpg', 1),
(70, '2020_200604_0015.jpg', '1478x1108 - 327.04 KB - image/jpeg', 'media/original/1591608555_2020_200604_0015.jpg', 'media/xlarge/1591608555_2020_200604_0015.jpg', 'media/large/1591608555_2020_200604_0015.jpg', 'media/medium/1591608555_2020_200604_0015.jpg', 'media/small/1591608555_2020_200604_0015.jpg', 'media/thumbnail/1591608555_2020_200604_0015.jpg', '2020-06-08 18:29:16', '2020-06-08 18:29:16', NULL, 'media/ratio3x2/1591608555_2020_200604_0015.jpg', 'media/ratio4x3/1591608555_2020_200604_0015.jpg', 0),
(73, 'AA-Cargo工場前撮影.jpg', '2480x1654 - 675.76 KB - image/jpeg', 'media/original/1592272147_AA-Cargo工場前撮影.jpg', 'media/xlarge/1592272147_AA-Cargo工場前撮影.jpg', 'media/large/1592272147_AA-Cargo工場前撮影.jpg', 'media/medium/1592272147_AA-Cargo工場前撮影.jpg', 'media/small/1592272147_AA-Cargo工場前撮影.jpg', 'media/thumbnail/1592272147_AA-Cargo工場前撮影.jpg', '2020-06-16 10:49:17', '2020-06-16 10:49:17', NULL, 'media/ratio3x2/1592272147_AA-Cargo工場前撮影.jpg', 'media/ratio4x3/1592272147_AA-Cargo工場前撮影.jpg', 0),
(75, 'aideaショールーム.jpg', '2362x1574 - 3.62 MB - image/jpeg', 'media/original/1592272157_aideaショールーム.jpg', 'media/xlarge/1592272157_aideaショールーム.jpg', 'media/large/1592272157_aideaショールーム.jpg', 'media/medium/1592272157_aideaショールーム.jpg', 'media/small/1592272157_aideaショールーム.jpg', 'media/thumbnail/1592272157_aideaショールーム.jpg', '2020-06-16 10:49:36', '2020-06-16 10:49:36', NULL, 'media/ratio3x2/1592272157_aideaショールーム.jpg', 'media/ratio4x3/1592272157_aideaショールーム.jpg', 0),
(80, 'iStock-1124981104.jpg', '5000x3402 - 1.02 MB - image/jpeg', 'media/original/1592272176_iStock-1124981104.jpg', 'media/xlarge/1592272176_iStock-1124981104.jpg', 'media/large/1592272176_iStock-1124981104.jpg', 'media/medium/1592272176_iStock-1124981104.jpg', 'media/small/1592272176_iStock-1124981104.jpg', 'media/thumbnail/1592272176_iStock-1124981104.jpg', '2020-06-16 10:50:18', '2020-06-16 10:50:18', NULL, 'media/ratio3x2/1592272176_iStock-1124981104.jpg', 'media/ratio4x3/1592272176_iStock-1124981104.jpg', 0),
(82, 'AA-Cargo工場前撮影.jpg', '2480x1654 - 675.76 KB - image/jpeg', 'media/original/1592272271_AA-Cargo工場前撮影.jpg', 'media/xlarge/1592272271_AA-Cargo工場前撮影.jpg', 'media/large/1592272271_AA-Cargo工場前撮影.jpg', 'media/medium/1592272271_AA-Cargo工場前撮影.jpg', 'media/small/1592272271_AA-Cargo工場前撮影.jpg', 'media/thumbnail/1592272271_AA-Cargo工場前撮影.jpg', '2020-06-16 10:51:15', '2020-06-16 10:51:15', NULL, 'media/ratio3x2/1592272271_AA-Cargo工場前撮影.jpg', 'media/ratio4x3/1592272271_AA-Cargo工場前撮影.jpg', 1),
(83, 'aidea_logo.png', '937x250 - 10.27 KB - image/png', 'media/original/1592369789_aidea_logo.png', NULL, 'media/large/1592369789_aidea_logo.png', 'media/medium/1592369789_aidea_logo.png', 'media/small/1592369789_aidea_logo.png', 'media/thumbnail/1592369789_aidea_logo.png', '2020-06-17 13:56:29', '2020-06-17 13:56:29', NULL, 'media/ratio3x2/1592369789_aidea_logo.png', 'media/ratio4x3/1592369789_aidea_logo.png', 0),
(84, '2019_Django_125_ABS_Deep_Pink.jpg', '2480x2055 - 1.22 MB - image/jpeg', 'media/original/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/xlarge/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/large/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/medium/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/small/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/thumbnail/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', '2020-06-22 10:34:27', '2020-06-22 10:34:27', 88, 'media/ratio3x2/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 'media/ratio4x3/1592789663_2019_Django_125_ABS_Deep_Pink.jpg', 1),
(85, 'aideaショールーム.jpg', '2362x1574 - 3.62 MB - image/jpeg', 'media/original/1593513887_aideaショールーム.jpg', 'media/xlarge/1593513887_aideaショールーム.jpg', 'media/large/1593513887_aideaショールーム.jpg', 'media/medium/1593513887_aideaショールーム.jpg', 'media/small/1593513887_aideaショールーム.jpg', 'media/thumbnail/1593513887_aideaショールーム.jpg', '2020-06-30 19:45:07', '2020-06-30 19:45:07', NULL, 'media/ratio3x2/1593513887_aideaショールーム.jpg', 'media/ratio4x3/1593513887_aideaショールーム.jpg', 1),
(86, 'aideaショールーム.jpg', '2362x1574 - 3.62 MB - image/jpeg', 'media/original/1593513916_aideaショールーム.jpg', 'media/xlarge/1593513916_aideaショールーム.jpg', 'media/large/1593513916_aideaショールーム.jpg', 'media/medium/1593513916_aideaショールーム.jpg', 'media/small/1593513916_aideaショールーム.jpg', 'media/thumbnail/1593513916_aideaショールーム.jpg', '2020-06-30 19:45:25', '2020-06-30 19:45:25', NULL, 'media/ratio3x2/1593513916_aideaショールーム.jpg', 'media/ratio4x3/1593513916_aideaショールーム.jpg', 1),
(87, 'top.jpg', '3703x2469 - 2.13 MB - image/jpeg', 'media/original/1593572585_top.jpg', 'media/xlarge/1593572585_top.jpg', 'media/large/1593572585_top.jpg', 'media/medium/1593572585_top.jpg', 'media/small/1593572585_top.jpg', 'media/thumbnail/1593572585_top.jpg', '2020-07-01 12:03:23', '2020-07-01 12:03:23', NULL, 'media/ratio3x2/1593572585_top.jpg', 'media/ratio4x3/1593572585_top.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `url`, `location`, `order`, `created_at`, `updated_at`, `show`) VALUES
(1, 'ニュース', '/tag/256', 'footer_menu_1', NULL, '2020-02-03 23:24:48', '2020-06-17 14:57:59', 0),
(2, '知る', '/tag/15', 'footer_menu_1', NULL, '2020-02-03 23:25:15', '2020-06-17 14:58:00', 0),
(3, '走る', '/tag/16', 'footer_menu_1', NULL, '2020-02-03 23:26:18', '2020-06-17 14:58:02', 0),
(4, '遊ぶ', '/tag/515', 'footer_menu_1', NULL, '2020-02-03 23:26:43', '2020-06-17 14:58:05', 0),
(5, 'お問い合わせ', 'https://aidea.net/contact', 'footer_menu_1', NULL, '2020-02-03 23:28:01', '2020-06-17 14:58:06', 0),
(6, '不二子ちゃんになりたい', '/cat/13', 'footer_menu_2', NULL, '2020-02-03 23:31:25', '2020-06-17 15:02:21', 1),
(7, '電動バイクの夢を見るか？', '/cat/10', 'footer_menu_2', NULL, '2020-02-03 23:32:01', '2020-06-17 15:02:22', 1),
(8, '初心者ライダーが乗ってみた！', '/cat/9', 'footer_menu_2', NULL, '2020-02-03 23:34:15', '2020-06-17 15:02:23', 1),
(9, 'プジョーブランド', '/cat/8', 'footer_menu_2', NULL, '2020-02-03 23:34:55', '2020-06-17 15:02:24', 1),
(10, '清水まどかのカメラ修行', '/cat/11', 'footer_menu_2', NULL, '2020-02-03 23:35:47', '2020-06-17 15:02:25', 1),
(11, '正規販売店紹介', '/cat/12', 'footer_menu_2', NULL, '2020-02-03 23:36:16', '2020-06-17 15:02:26', 1),
(12, '利用規約', '/_p/terms-of-service', 'footer_menu_3', NULL, '2020-02-03 23:37:51', '2020-06-17 14:59:02', 1),
(13, 'プライパシーポリシー', '/_p/privacy-policy', 'footer_menu_3', NULL, '2020-02-03 23:38:40', '2020-06-17 14:59:01', 1),
(14, '運営会社', '#', 'footer_menu_3', NULL, '2020-02-03 23:38:56', '2020-06-17 14:58:58', 1),
(15, 'お問い合わせ', 'https://aidea.net/contact', 'footer_menu_3', NULL, '2020-02-03 23:39:15', '2020-06-17 14:58:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_tags`
--

CREATE TABLE `menu_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `shown_in_header_menu` tinyint(1) NOT NULL DEFAULT '0',
  `shown_in_feed_label` tinyint(1) NOT NULL DEFAULT '0',
  `display_text_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `display_detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shown_in_editor` tinyint(1) NOT NULL DEFAULT '0',
  `reverse_order` tinyint(1) NOT NULL DEFAULT '0',
  `hide_from_display` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_tags`
--

INSERT INTO `menu_tags` (`id`, `tag_id`, `order`, `shown_in_header_menu`, `shown_in_feed_label`, `display_text_enabled`, `display_detail`, `shown_in_editor`, `reverse_order`, `hide_from_display`, `created_at`, `updated_at`) VALUES
(2, 15, 2, 1, 0, 0, NULL, 0, 0, 0, '2019-11-19 02:35:53', '2020-01-22 12:23:11'),
(5, 515, 4, 1, 0, 0, NULL, 0, 0, 0, '2020-07-08 15:59:56', '2020-07-08 15:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_24_082045_create_sites_table', 1),
(4, '2019_10_25_074102_create_posts_table', 1),
(5, '2019_10_25_080225_create_tags_table', 1),
(6, '2019_10_25_081358_create_tag_post_table', 1),
(7, '2019_10_25_084034_create_medias_table', 1),
(8, '2019_10_25_084845_create_post_views_table', 1),
(9, '2019_10_25_091009_create_settings_table', 1),
(10, '2019_10_25_091853_create_support_emails_table', 1),
(11, '2019_10_25_092519_create_menu_tags_table', 1),
(12, '2019_10_25_105649_create_inquiries_table', 1),
(13, '2019_11_01_093335_create_roles_table', 1),
(14, '2019_11_01_093729_add_role_id_to_users_table', 1),
(15, '2019_11_07_021402_change_inquiry_work_status_inquiries_table', 2),
(16, '2019_11_07_151354_change_foreign_key_post_id_in_medias_table', 3),
(17, '2019_11_27_020023_add_tag_order_to_tag_post_table', 4),
(18, '2019_11_29_072436_add_excerpt_and_slide_images_to_posts_table', 5),
(19, '2019_12_02_103638_add_ratio3x2_path_to_medias_table', 6),
(20, '2019_12_04_033318_add_author_id_to_post_views_table', 7),
(21, '2019_12_05_030529_add_user_assigned_ids_to_posts_table', 7),
(22, '2019_12_05_031216_add_ratio4x3_path_to_medias_table', 7),
(23, '2019_12_06_021214_create_post_user_table', 8),
(24, '2019_12_06_091530_remove_user_assigned_ids_from_posts_table', 8),
(25, '2019_12_10_092520_add_slug_to_posts_table', 8),
(26, '2019_12_17_033027_modify_nullable_settings_table', 9),
(27, '2019_12_19_021718_create_pages_table', 9),
(28, '2019_12_19_080200_add_status_to_pages_table', 9),
(29, '2019_12_23_040758_create_votes_table', 10),
(30, '2019_12_23_073331_add_vote_fields_to_posts_table', 10),
(31, '2019_12_24_070612_create_tag_categories_table', 10),
(32, '2019_12_24_070902_add_tag_category_id_to_tags_table', 10),
(33, '2019_12_25_161017_add_vote_expire_time_to_posts_table', 11),
(34, '2019_12_27_021831_add_is_cover_to_medias_table', 11),
(35, '2020_01_08_194736_create_post_sliders_table', 12),
(36, '2020_01_15_125336_add_featured_image_to_tags_table', 13),
(37, '2020_01_15_183757_add_content_to_votes_table', 13),
(38, '2020_01_16_164038_change_slide_images_type_on_post_sliders_table', 14),
(39, '2020_01_18_111948_remove_tag_category_id_on_tags_table', 15),
(40, '2020_01_18_112309_add_categories_table', 15),
(41, '2020_01_18_112756_add_category_post_table', 15),
(42, '2020_01_18_113558_delete_tag_categories_table', 15),
(43, '2020_02_03_163318_create_menu_table', 16),
(44, '2020_02_06_191911_add_show_to_menus_table', 17),
(45, '2020_04_22_191336_update_media_images_column_on_posts_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `cover_image` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `cover_image`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '検索', 'search', NULL, NULL, 1, 1, '2019-12-20 18:44:26', '2019-12-20 18:44:26'),
(2, 'お問い合わせ', 'inquiry', NULL, NULL, 1, 1, '2019-12-20 18:44:26', '2019-12-20 18:44:26'),
(3, 'プライバシーポリシー', 'privacy-policy', NULL, NULL, 1, 1, '2019-12-20 18:44:26', '2019-12-20 18:44:26'),
(4, '利用規約', 'terms-of-service', NULL, NULL, 1, 1, '2019-12-20 18:44:26', '2019-12-20 18:44:26'),
(5, '特集一覧', 'feature', NULL, 7, 1, 1, '2019-12-20 18:44:26', '2020-04-25 14:24:49'),
(6, '連載一覧', 'series', NULL, 8, 1, 1, '2019-12-20 18:44:26', '2020-04-25 14:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('coinguyen1710@gmail.com', '$2y$10$Z50QGsO29QPFFJN19lJVMeyfAdzWFkCS9lQxVXbWmAtiiGDnh/92C', '2020-01-16 23:10:59'),
('tantran1104@gmail.com', '$2y$10$.QPkv3w8rke1xz3o.eOV2OV83R7jXGZ0MFd0PKzJQ2y3oAv8yUo/q', '2020-04-09 19:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `cover_image` bigint(20) UNSIGNED DEFAULT NULL,
  `cover_image_style` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_via_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_via_href` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `html_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogp_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogp_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogp_image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `excerpt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vote_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vote_count` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vote_status` tinyint(1) DEFAULT '0',
  `vote_expire_time` date DEFAULT NULL,
  `media_images` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `cover_image`, `cover_image_style`, `cover_via_text`, `cover_via_href`, `post_status`, `user_id`, `site_id`, `html_title`, `html_meta_description`, `html_meta_keyword`, `canonical_url`, `ogp_title`, `ogp_description`, `ogp_image_url`, `created_at`, `updated_at`, `excerpt`, `vote_id`, `vote_count`, `vote_status`, `vote_expire_time`, `media_images`) VALUES
(48, NULL, '48', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 11:47:33', '2020-05-07 11:47:33', NULL, NULL, NULL, 0, NULL, NULL),
(49, NULL, '49', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 15:38:10', '2020-05-07 15:38:10', NULL, NULL, NULL, 0, NULL, NULL),
(50, NULL, '50', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 15:38:18', '2020-05-07 15:38:18', NULL, NULL, NULL, 0, NULL, NULL),
(51, 'This is publish post', '51', '<p>aasafsafsa</p>', NULL, 'default', NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 15:38:00', '2020-05-08 16:08:43', NULL, NULL, NULL, 0, NULL, NULL),
(52, NULL, '52', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 13:55:25', '2020-05-13 13:55:25', NULL, NULL, NULL, 0, NULL, NULL),
(53, NULL, '53', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 13:56:32', '2020-05-13 13:56:32', NULL, NULL, NULL, 0, NULL, NULL),
(55, NULL, '55', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 13:58:03', '2020-05-13 13:58:03', NULL, NULL, NULL, 0, NULL, NULL),
(56, NULL, '56', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 13:58:55', '2020-05-13 13:58:55', NULL, NULL, NULL, 0, NULL, NULL),
(57, '8888888', '57', '<p>88888</p><p>&nbsp;</p><figure class=\"media\"><div data-oembed-url=\"https://[slider id=10]\"><div class=\"post-slider\" data-id=\"10\">[slider id=10]</div></div></figure>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 19:33:00', '2020-05-30 15:56:29', NULL, NULL, NULL, 0, NULL, NULL),
(58, '888888888', '58', '<p>555555</p>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-14 10:25:00', '2020-05-22 14:25:17', NULL, NULL, NULL, 0, NULL, NULL),
(59, '555555', '59', '<p>55555</p>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-14 10:26:00', '2020-05-22 14:24:55', NULL, NULL, NULL, 0, NULL, NULL),
(60, 'ｐｐｐｐｐｐｐｐｐｐ', '60', '<p>ｐｐｐｐｐｐｐｐｐ</p>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-14 10:29:00', '2020-05-22 14:23:51', NULL, NULL, NULL, 0, NULL, NULL),
(61, 'おおおおおおおおおお', '61', '<p>おおおおおおおお</p>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-14 10:30:00', '2020-05-22 14:23:23', NULL, NULL, NULL, 0, NULL, NULL),
(63, 'タグ4　0505', '63', '<h5 class=\"summary\">マーケティング部の玉井がインスタで見つけた世界各国のプジョー公式写真をご紹介！</h5><p>&nbsp;</p><p>みなさまこんにちは、マーケティング部の玉井です。</p><p>最近アイディアスタイルで、様々な国のプジョー公式YouTubeチャンネルから動画を紹介していました！</p><p>まだ見たことがない方はぜひこちらもご覧ください。↓</p><p><span style=\"color:hsl(24,71%,53%);\"><strong>記事リンク</strong></span></p><p>その国に行かなくても、どんな風にプジョーのスクーターに乗って楽しんでいるのかを見ることができるのは、とても面白いですよね。</p><p>今日は、動画ではなく写真で楽しんでいただきたいと思い、Instagramで見つけたプジョー公式写真を紹介したいと思います！</p><h2>紹介する国は・・・チェコ共和国です！</h2><p><span style=\"background-color:rgb(255,255,255);color:rgb(33,37,41);\">チェコは中央ヨーロッパにある国ですね！さっそくチェコのプジョー公式Instagramから写真を見ていきましょう。</span></p><h3><span style=\"background-color:rgb(255,255,255);color:rgb(33,37,41);font-size:16px;\">空の青さとピッタリなジャンゴ</span></h3><p>&nbsp;</p><figure class=\"media\"><div data-oembed-url=\"https://www.instagram.com/p/Bkw_lhSgwiV/?utm_source=ig_web_copy_link\"><div class=\"iframely-embed\"><div class=\"iframely-responsive\"><iframe class=\"instagram-media instagram-media-rendered\" src=\"https://www.instagram.com/p/Bkw_lhSgwiV/embed/\" frameborder=\"0\" height=\"270\"></iframe></div></div></div></figure><p>&nbsp;</p><p>こちらは小さな町モドジツェの風景です！チェコは建物の装飾が素敵な国なんです。</p><p>屋根のカラーが鮮やかで空の青さを際立てていますね。</p><p>建物と空のどちらのカラーも取り入れているようなジャンゴが可愛らしい！</p><p><strong>■ジャンゴをもっと知りたい方はこちら。↓</strong></p><p><strong>記事リンク</strong></p><p>&nbsp;</p><h3><strong>ストリートアートとスピードファイト</strong></h3><figure class=\"media\"><div data-oembed-url=\"https://www.instagram.com/p/Btv7jJ4C2Fu/?utm_source=ig_web_copy_link\"><div class=\"iframely-embed\"><div class=\"iframely-responsive\"><iframe class=\"instagram-media instagram-media-rendered\" src=\"https://www.instagram.com/p/Btv7jJ4C2Fu/embed/\" frameborder=\"0\" height=\"270\"></iframe></div></div></div></figure><p>&nbsp;</p><p>こちらはブルノでの写真です。</p><p>スピードファイトが並んだ後ろにはストリートアートが！スピードファイトのカッコよさとあっていますよね。</p><p>それとよーく見てみると線路の上で撮影されています。きっとすでに使われなくなった線路なのでしょう。発想が素敵です！</p><p><strong>■スピードファイトをもっと知りたい方はこちら。↓</strong></p><p><strong>記事リンク</strong></p><figure class=\"media\"><div data-oembed-url=\"https://[slider id=5]\"><div class=\"post-slider\" data-id=\"5\">[slider id=5]</div></div></figure><h2><strong>チェコの街並みとプジョースクーター、いかがでしたか？</strong></h2><p>日本では見れない風景の中撮影されたプジョーのスクーターたちは、どことなく顔つきが違って見えますね。</p><p>また様々な国のプジョー公式Instagramより紹介していきたいと思います！お楽しみに！！</p><h3><strong>日本のプジョー公式Instagramも見てみてください！</strong></h3><p>[linkcard url=\"https://www.instagram.com/adivajapan/\" /]</p><p>&nbsp;</p><p>&nbsp;</p>', 47, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-05 18:49:00', '2020-05-22 16:51:31', NULL, 8, '{\"opt_1\":0,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 0, '2020-06-07', NULL),
(65, 'タグ3　0510', '65', '<p>いつもAIDEA STYLEをお読みいただきありがとうございます！</p><p>先月、新型コロナウィルスの感染拡大防止のため、赤坂ショールームの臨時休業を5月6日まで延長と伝えさせていただきましたが、政府からの緊急事態宣言延長に基づいて、以下の通り期間の変更をさせていただきます。</p><figure class=\"table\"><table><tbody><tr><td><p>4月2日（木）～5月6日（水）まで</p><p>↓</p><p><span style=\"font-size:16px;\"><strong>4月2日（木）～</strong></span><span style=\"color:hsl(6,78%,57%);font-size:16px;\"><strong>5月31日（日）まで</strong></span></p><p>※政府からのアナウンスや感染拡大の状況によっては再度期間の見直しをさせていただく場合がございます。</p></td></tr></tbody></table></figure><p>みなさまにはご不便をお掛け致しますが、ご理解いただけますと幸いです。</p><p>期間中のお問い合わせなどについては以下をご確認ください！</p><h3><strong>期間中の赤坂ショールームへのお問い合わせについて</strong></h3><p><span style=\"background-color:rgb(255,255,255);color:rgb(33,37,41);\">引き続き、赤坂ショールーム宛にいただいたお電話は、担当者に転送される形になっていますのでご安心ください！</span></p><p><strong>お問い合わせ電話番号：03-6427-3600</strong></p><p><strong>受付時間：10:00～19:00（土日祝を除く）</strong></p><p>※お電話が立て込んだ場合、折り返しとなる場合がございます。予めご了承ください。</p><p>また、お急ぎでなければお問い合わせフォームがございますので、メールでの対応も可能です。</p><figure class=\"media\"><div data-oembed-url=\"https://[slider id=7]\"><div class=\"post-slider\" data-id=\"7\">[slider id=7]</div></div></figure><p><strong>■aideaのお問い合わせについてはこちら。↓</strong></p><p>[linkcard url=\"https://aidea.net/contact\" /]</p><p><strong>■プジョーのお問い合わせについてはこちら。↓</strong></p><p>[linkcard url=\"https://peugeotscooters.jp/contact/\" /]</p><h6 class=\"break-page\">&nbsp;</h6><h3>カタログ希望の方</h3><p>カタログのご郵送は、現在ストップさせていただいておりますが、WEBサイトよりデータでダウンロードが可能です！</p><p>ぜひこちらもご覧いただければと思います。</p><p><strong>■aideaのカタログデータはこちら。↓</strong></p><p>[linkcard url=\"https://aidea.net/catalog\" /]</p><p><strong>■プジョーのカタログデータはこちら。↓</strong></p><p>[linkcard url=\"https://peugeotscooters.jp/catalog/\" /]</p><h2>赤坂ショールームからのお知らせでした。</h2><p><span style=\"background-color:rgb(255,255,255);color:rgb(33,37,41);\">一日も早く新型コロナウィルスが終息し、また赤坂ショールームでみなさまにお会いできることを心待ちにしております！</span></p>', 52, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-10 19:40:00', '2020-05-22 16:53:25', NULL, 7, '{\"opt_1\":0,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-05-22', '[]'),
(70, 'タグ3　0520', '70', '<p>&nbsp;</p><p>テスト</p>', 61, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-20 12:15:00', '2020-05-22 16:52:10', NULL, NULL, NULL, 0, NULL, NULL),
(71, 'タグ4　0515', '71', '<p>[vote]</p><blockquote><p><span style=\"font-size:17px;\">いま私たちが直面している大きな問題、地球温暖化。</span><br><span style=\"font-size:17px;\">その解決策の一つが、ZEV（ゼロエミッションヴィークル）です。</span><br><span style=\"font-size:17px;\">&nbsp;</span></p><p><span style=\"font-size:17px;\">その中でも、コンパクトでエネルギー効率の高い電動バイクは、</span><br><span style=\"font-size:17px;\">環境にも社会にも貢献できるモビリティといえるでしょう。</span><br><span style=\"font-size:17px;\">&nbsp;</span></p><p><span style=\"font-size:17px;\">電動バイクという小さな一歩が</span><br><span style=\"font-size:17px;\">地球にとって大きな飛躍となることを願って。</span></p><p><a href=\"https://aidea.net/\"><span style=\"font-size:17px;\">https://aidea.net/</span></a></p><p>&nbsp;</p></blockquote><h4>てすと</h4><h2>てすと</h2><p>&nbsp;</p><h3>てすと</h3><p>dssdgdsgdsgd &nbsp; &nbsp; sgd</p><p><span style=\"font-size:17px;\"><i>てすと</i></span></p><p><span style=\"font-size:19px;\"><i><strong>てすと</strong></i></span></p><p><span style=\"font-size:22px;\"><i><strong>てすと</strong></i></span><br>&nbsp;</p><figure class=\"image image_resized\" style=\"width:804px;\"><img src=\"/storage/media/original/1590031358_02.jpg\" alt=\"http://aidea.style\"></figure><figure class=\"media\"><div data-oembed-url=\"https://[slider id=8]\"><div class=\"post-slider\" data-id=\"8\">[slider id=8]</div></div></figure>', 57, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 12:15:00', '2020-06-18 16:31:49', NULL, 8, '{\"opt_1\":1,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-06-12', '[{\"id\":49,\"url\":\"/storage/media/original/1589450788_02.png\"},{\"id\":50,\"url\":\"/storage/media/original/1589450790_03.png\"},{\"id\":48,\"url\":\"/storage/media/original/1589450784_01.png\"},{\"id\":47,\"url\":\"/storage/media/original/1589450221_01.png\"}]'),
(76, '5555', '76', '<figure class=\"media\"><div data-oembed-url=\"https://[slider id=9]\"><div class=\"post-slider\" data-id=\"9\">[slider id=9]</div></div></figure>', NULL, 'default', NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 15:45:00', '2020-05-22 15:52:57', NULL, 8, '{\"opt_1\":0,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-05-25', NULL),
(81, NULL, '81', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-08 18:40:51', '2020-06-08 18:40:51', NULL, NULL, NULL, 0, NULL, NULL),
(82, NULL, '82', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 15:20:18', '2020-06-11 15:20:18', NULL, NULL, NULL, 0, NULL, NULL),
(83, NULL, '83', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 15:20:22', '2020-06-11 15:20:22', NULL, NULL, NULL, 0, NULL, NULL),
(86, 'あんけ', '86', '<p>あんけーと！</p><figure class=\"image\"><img src=\"/storage/media/original/1592272271_AA-Cargo%E5%B7%A5%E5%A0%B4%E5%89%8D%E6%92%AE%E5%BD%B1.jpg\"><figcaption>あああ</figcaption></figure>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 18:18:00', '2020-06-16 18:37:48', NULL, 8, '{\"opt_1\":0,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-06-17', NULL),
(87, 'てすと', '87', '<p><span style=\"font-family:sans-serif;\"><i>てすと！</i></span></p><p><span style=\"font-family:sans-serif;\"><i>てすと！てすと！てすと！てすと！てすと！てすと！てすと！てすと！</i></span></p><p><span style=\"font-family:sans-serif;\"><i>TEST!TEST!TEST!TEST!TEST!</i></span></p><p><span style=\"font-family:sans-serif;\"><i>て～す～と～！てすと！</i></span></p>', NULL, 'default', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 18:51:00', '2020-07-01 12:04:26', NULL, 8, '{\"opt_1\":0,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-06-30', NULL),
(88, 'テスト', '88', '<p>&nbsp;</p><h2>アンケート</h2><p>&nbsp;</p><p>[vote]</p><p>&nbsp;</p><p>テスト</p><p>&nbsp;</p><figure class=\"image image_resized\" style=\"width:801px;\"><img src=\"/storage/media/original/1591607822_2020_200604_0015.jpg\" alt=\"https://aidea.net/\"><figcaption>ナンバー取得しました！</figcaption></figure><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>', 84, 'default', NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-22 10:09:00', '2020-06-22 10:45:57', NULL, 8, '{\"opt_1\":1,\"opt_2\":0,\"opt_3\":0,\"opt_4\":0,\"opt_5\":0}', 1, '2020-06-30', NULL),
(89, NULL, '89', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-22 10:32:27', '2020-06-22 10:32:27', NULL, NULL, NULL, 0, NULL, NULL),
(92, NULL, '92', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 19:42:08', '2020-07-07 19:42:08', NULL, NULL, NULL, 0, NULL, NULL),
(93, NULL, '93', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-08 14:28:55', '2020-07-08 14:28:55', NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_sliders`
--

CREATE TABLE `post_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `slide_images` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_sliders`
--

INSERT INTO `post_sliders` (`id`, `post_id`, `slide_images`, `created_at`, `updated_at`) VALUES
(5, 63, '{\"48\":\"\\/storage\\/media\\/original\\/1589450784_01.png\",\"49\":\"\\/storage\\/media\\/original\\/1589450788_02.png\",\"50\":\"\\/storage\\/media\\/original\\/1589450790_03.png\"}', '2020-05-14 19:06:24', '2020-05-14 19:06:37'),
(8, 71, '{\"58\":\"\\/storage\\/media\\/original\\/1590031344_01.jpg\",\"59\":\"\\/storage\\/media\\/original\\/1590031358_02.jpg\",\"60\":\"\\/storage\\/media\\/original\\/1590031371_03.jpg\"}', '2020-05-21 12:22:24', '2020-05-21 12:23:04'),
(9, 76, '{\"63\":\"\\/storage\\/media\\/original\\/1590129950_FireShot Capture 224 -  - adiva.sakura.ne.jp.png\",\"64\":\"\\/storage\\/media\\/original\\/1590129950_G.png\"}', '2020-05-22 15:45:50', '2020-05-22 15:45:50'),
(10, 57, '{\"65\":\"\\/storage\\/media\\/original\\/1590821739_1579503085_preview.jpg\",\"66\":\"\\/storage\\/media\\/original\\/1590821745_b5483014ac9a4ba4b2a544cf3edfaaaa6e3b45ce_xlarge.jpg\"}', '2020-05-30 15:55:39', '2020-05-30 15:55:50');

-- --------------------------------------------------------

--
-- Table structure for table `post_user`
--

CREATE TABLE `post_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_views`
--

CREATE TABLE `post_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_views`
--

INSERT INTO `post_views` (`id`, `post_id`, `session_id`, `ip`, `agent`, `created_at`, `updated_at`, `user_id`) VALUES
(59, 65, 'fSg8m3JotKtjBj8HEfEsOtwKSq34bunIolLI5WBk', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', '2020-05-22 11:34:06', '2020-05-22 11:34:06', 1),
(60, 70, 'AthI0iloRgMzj4vRowzsvQRR9JdUQEcWyzGqlmUa', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 13:06:00', '2020-05-22 13:06:00', 1),
(61, 71, '0zG5hr9AnwCYC4wGkBjHpUrxkLA4k9CdYKdKktry', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 13:29:15', '2020-05-22 13:29:15', 1),
(62, 70, '0zG5hr9AnwCYC4wGkBjHpUrxkLA4k9CdYKdKktry', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 13:29:23', '2020-05-22 13:29:23', 1),
(63, 65, '0zG5hr9AnwCYC4wGkBjHpUrxkLA4k9CdYKdKktry', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 13:29:34', '2020-05-22 13:29:34', 1),
(68, 63, 'YAME1NIiUv4OrMfWWrzVNHRmXcD5zx9EshxpTzCx', '153.209.161.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Mobile/15E148 Safari/604.1', '2020-05-22 14:24:05', '2020-05-22 14:24:05', 1),
(69, 63, '0zG5hr9AnwCYC4wGkBjHpUrxkLA4k9CdYKdKktry', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 16:02:20', '2020-05-22 16:02:20', 1),
(70, 71, 'lWYNTKaCPLBDi8Y4FzoRov4E1VchQoWBBsZ7PT8K', '153.209.161.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Mobile/15E148 Safari/604.1', '2020-05-22 16:16:57', '2020-05-22 16:16:57', 1),
(71, 61, '0zG5hr9AnwCYC4wGkBjHpUrxkLA4k9CdYKdKktry', '153.209.161.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', '2020-05-22 16:55:34', '2020-05-22 16:55:34', 1),
(72, 70, 'umzst9UwgjKElUgApoeDeVbkLnvoFmW4bXCuiren', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', '2020-06-04 16:23:43', '2020-06-04 16:23:43', 1),
(73, 70, '8iCY1pejSNWmFD3PVABh6XsTYdznPFGmP8V820Za', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', '2020-06-04 16:34:11', '2020-06-04 16:34:11', 1),
(74, 70, '2pp7XTlJvrNzrnUN9cwxFBEnxcgL6XT1ToSttey5', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', '2020-06-05 13:09:50', '2020-06-05 13:09:50', 1),
(75, 71, '2pp7XTlJvrNzrnUN9cwxFBEnxcgL6XT1ToSttey5', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', '2020-06-05 13:10:54', '2020-06-05 13:10:54', 1),
(76, 71, 'hT55RWiwGmWNAwxFO4wXcNijmZWEUUq3WC6PBIvh', '173.252.95.34', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-05 13:11:27', '2020-06-05 13:11:27', 1),
(77, 71, 'LVVCechLNIfbdjBWyqSOCeB6qL8U2con698rctSc', '173.252.95.9', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-05 13:11:28', '2020-06-05 13:11:28', 1),
(78, 71, 'JfReYGjinwDFruUysjLd3Cp6Wv3FknB2i8BvgOMt', '173.252.95.117', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-05 13:11:28', '2020-06-05 13:11:28', 1),
(79, 71, 'WNqTtQHFWxLsq5KKnDOFgNow7tsb1nsLtZFP3iXU', '171.243.75.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-05 13:11:36', '2020-06-05 13:11:36', 1),
(80, 65, 'WNqTtQHFWxLsq5KKnDOFgNow7tsb1nsLtZFP3iXU', '171.243.75.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-05 13:22:30', '2020-06-05 13:22:30', 1),
(81, 71, 'NCpW13ZRyKTmsShrKrUFROhDjiCwvKfF4bFKE11j', '171.243.75.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '2020-06-05 13:34:20', '2020-06-05 13:34:20', 1),
(82, 71, '1FSxoiNhnEQsJVVt94afL9iRufMPVwCZ6hqf9QOi', '173.252.95.29', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-05 13:37:57', '2020-06-05 13:37:57', 1),
(83, 71, '88dpQWDMuhmBXT3A7DLvqvojK3nx4zsFFQB3XC1u', '173.252.95.18', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-05 13:56:35', '2020-06-05 13:56:35', 1),
(84, 65, 'yn52NjuRdi1Y8BdLvcW9Ua4GFJZbxFmudCi87Db8', '171.243.75.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-05 16:10:34', '2020-06-05 16:10:34', 1),
(85, 71, 'yn52NjuRdi1Y8BdLvcW9Ua4GFJZbxFmudCi87Db8', '171.243.75.19', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-05 16:10:54', '2020-06-05 16:10:54', 1),
(86, 61, 'gm6KH74XWvuEd09X5fKxIGvlBXLGx9xI7Xw9RXQ3', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-08 18:13:47', '2020-06-08 18:13:47', 1),
(87, 71, 'EmVFNGyw7sGvws9QHpgjg9OzH2XFZwW4VlmCIa4R', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-10 23:00:45', '2020-06-10 23:00:45', 1),
(88, 71, 'ZEufPKH18WzttvMYxX5idxNM36rvlcEgXNNqoHoU', '116.98.254.160', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-10 23:31:58', '2020-06-10 23:31:58', 1),
(89, 71, 'qyI6zOcwFqinGPW9qUqejJKnh4zQj6NVR6uIUXvW', '173.252.95.17', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-10 23:39:57', '2020-06-10 23:39:57', 1),
(90, 71, 'WA5iRHA0Q3JAeq7fPA7r9ik9okvKOcpnmuUPF20z', '173.252.95.3', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', '2020-06-10 23:39:58', '2020-06-10 23:39:58', 1),
(91, 65, 'EmVFNGyw7sGvws9QHpgjg9OzH2XFZwW4VlmCIa4R', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-10 23:42:09', '2020-06-10 23:42:09', 1),
(92, 70, '35ipkTgpceeQhpXCzHbMSSkuswWd0dP0SgZ1s5Vo', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 11:29:00', '2020-06-11 11:29:00', 1),
(97, 86, '8JAwNR8cUx1km4rhQmu3NOrqRVGsGMuBUAFdtbhC', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-16 18:25:18', '2020-06-16 18:25:18', 1),
(98, 87, '5TpzFnmr1mzFAJz6gByNBSzy9Wfs0sKYH1d5iIW8', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-16 18:58:34', '2020-06-16 18:58:34', 27),
(100, 71, 'pjPt6MxSkajV2FYyCpT9oPCSIKB9zvOa0PjKoA0h', '58.186.197.248', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-17 13:12:44', '2020-06-17 13:12:44', 1),
(101, 87, 'bwfcWJxr0eeTXHJJBkL868JQG8guB1jkvAy2m0gF', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-17 14:09:11', '2020-06-17 14:09:11', 27),
(102, 87, 'UhAYA2r5GnGMIukfYzM4gcEzmEHQBkUvjpo29Mqd', '1.53.52.149', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-18 10:06:15', '2020-06-18 10:06:15', 27),
(103, 71, 'EgdqtPHEsJEkkhgkwEgFKBIKSApe2MjgPQo56WPT', '1.53.52.149', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-18 16:10:08', '2020-06-18 16:10:08', 1),
(104, 71, 'lV2oIO254GKIyuWcw8qcb21HWDniKdIp0pDPGbL7', '1.53.52.149', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '2020-06-18 16:14:13', '2020-06-18 16:14:13', 1),
(105, 87, 'unY0C60LubQPWwJb0gYKb2EJEKZyVCPZ5onpxJ8d', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-22 11:32:07', '2020-06-22 11:32:07', 27),
(106, 87, 'WuJeGcrVX6aG3xmucMrzI1eVGez7d9EpWhoVCl64', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', '2020-06-22 11:36:59', '2020-06-22 11:36:59', 27),
(107, 87, 'ZSQMy68oXbgKiwYkpouHWvqdFwMRrnz4YFFOpMuD', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', '2020-06-23 15:28:00', '2020-06-23 15:28:00', 27),
(108, 87, 'l7T58f7ZJXLcwcegmFhccHvtekEllE3kyYhE2d4m', '54.157.208.23', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2020-06-23 15:33:05', '2020-06-23 15:33:05', 27),
(109, 70, 'LAL3A8FlUmGXz4tW7QuURbtAu9pG6UcV3FLZ2S1x', '1.53.52.149', 'Mozilla/5.0 (Windows NT 6.3; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', '2020-06-25 23:08:29', '2020-06-25 23:08:29', 1),
(117, 87, 'IbAdG8J46OzKi1eUlzrxwDsZLR1hO7uuw7nZ9gsS', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', '2020-06-29 18:05:13', '2020-06-29 18:05:13', 27),
(118, 87, 'X2dymDybZ80EzVLyqzqUbCAWJUaq2gImOjJXc2gE', '114.149.50.185', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', '2020-07-01 11:58:11', '2020-07-01 11:58:11', 8),
(127, 70, 'J6RXGsY6gvfQ41IrHuH7MVQk0xGxR0jUFVDqwDSj', '1.53.54.10', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', '2020-07-08 13:24:58', '2020-07-08 13:24:58', 1),
(128, 70, 'otXLWoHKYHfoWDQN8L62w7JOAZGbNoWYF3raf2Di', '1.52.185.23', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', '2020-07-19 17:58:44', '2020-07-19 17:58:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '管理者', '2019-11-01 02:59:15', '2019-11-01 02:59:15'),
(2, 'writer', '記事作成者', '2019-11-01 02:59:15', '2019-11-01 02:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'general_seo_title', 'general_seo_title', 'Aidea Style | バイクと共に送る生活を楽しむためのライフスタイルマガジン', '2019-12-25 02:46:10', '2020-02-11 16:36:35'),
(2, 'general_copyright', 'general_copyright', 'aidea 株式会社', '2019-12-25 02:46:10', '2019-12-27 02:38:54'),
(3, 'general_seo_description', 'general_seo_description', 'AIDEA STYLE（アイディアスタイル） について説明させていただきます。', '2019-12-25 02:46:10', '2019-12-27 02:38:54'),
(4, 'general_seo_image', 'general_seo_image', NULL, '2019-12-25 02:46:10', '2020-04-10 12:54:50'),
(5, 'general_logo', 'general_logo', NULL, '2019-12-25 02:46:10', '2020-06-17 14:10:14'),
(6, 'general_logo_mobile', 'general_logo_mobile', NULL, '2019-12-25 02:46:10', '2020-06-16 19:06:00'),
(7, 'general_home_label_top_menu', 'general_home_label_top_menu', 'HOME', '2019-12-25 02:46:10', '2020-02-28 16:07:36'),
(8, 'general_seo_keywords', 'general_seo_keywords', NULL, '2019-12-25 02:46:10', '2019-12-25 02:46:10'),
(9, 'embed_width', 'embed_width', '300', '2019-12-25 02:49:36', '2019-12-27 06:34:46'),
(10, 'embed_height', 'embed_height', '200', '2019-12-25 02:49:36', '2019-12-27 06:34:46'),
(11, 'general_seo_domain', 'general_seo_domain', NULL, '2020-01-06 17:25:00', '2020-01-06 17:25:00'),
(12, 'general_fb_id', 'general_fb_id', NULL, '2020-01-06 17:25:00', '2020-01-06 17:25:00'),
(13, 'general_social_share_option', 'general_social_share_option', '[\"default\",\"twitter\",\"facebook\"]', '2020-01-06 17:25:00', '2020-03-02 11:34:37'),
(14, 'siderbar_feature_cat_id', 'siderbar_feature_cat_id', '1', '2020-01-20 15:54:41', '2020-01-20 15:54:41'),
(15, 'siderbar_feature_number', 'siderbar_feature_number', '3', '2020-01-20 15:54:41', '2020-01-20 15:54:41'),
(16, 'siderbar_series_cat_id', 'siderbar_series_cat_id', '6', '2020-01-20 15:54:41', '2020-01-20 16:06:46'),
(17, 'siderbar_series_number', 'siderbar_series_number', '3', '2020-01-20 15:54:41', '2020-01-20 16:06:46'),
(18, 'siderbar_ranking_number', 'siderbar_ranking_number', '3', '2020-02-02 22:48:54', '2020-02-28 16:13:15'),
(19, 'stylesheet', 'stylesheet', NULL, '2020-02-02 22:48:54', '2020-02-02 22:48:54'),
(20, 'footer_banner_image_1', 'footer_banner_image_1', '/storage/media/original/1587787134_adiva.png', '2020-02-03 22:49:01', '2020-04-25 12:59:43'),
(21, 'footer_banner_link_1', 'footer_banner_link_1', 'https://aidea.net/', '2020-02-03 22:49:01', '2020-02-03 23:19:57'),
(22, 'footer_banner_image_2', 'footer_banner_image_2', '/storage/media/original/1587788663_peugot.png', '2020-02-03 22:49:01', '2020-04-25 13:24:27'),
(23, 'footer_banner_link_2', 'footer_banner_link_2', 'https://www.instagram.com/peugeot/?hl=en', '2020-02-03 22:49:01', '2020-02-03 23:19:57'),
(24, 'admin_javascript', 'admin_javascript', 'jQuery(\'document\').ready(function($){\r\n  if($(\'#editor\').length > 0){\r\n    var base_url = window.location.origin;\r\n    ClassicEditor.create( document.querySelector( \'#editor\' ), {\r\n      toolbar: [ \'heading\', \'|\', \'bold\', \'italic\', \'fontSize\', \'fontColor\', \'alignment\', \'link\', \'bulletedList\', \'numberedList\', \'|\', \'imageUpload\', \'blockQuote\', \'insertTable\', \'mediaEmbed\', \'undo\', \'redo\' ],\r\n      heading: {\r\n        options: [\r\n          {\r\n            model: \'paragraphSummary\',\r\n            view: {\r\n              name: \'h5\',\r\n              classes: \'summary\'\r\n            },\r\n            title: \'Summary\',\r\n            class: \'ck-heading_heading4_summary\',\r\n          },\r\n          { model: \'paragraph\', title: \'Paragraph\', class: \'ck-heading_paragraph\' },\r\n          { model: \'heading1\', view: \'h2\', title: \'Heading 1\', class: \'ck-heading_heading1\' },\r\n          { model: \'heading2\', view: \'h3\', title: \'Heading 2\', class: \'ck-heading_heading2\' },\r\n          { model: \'heading3\', view: \'h4\', title: \'Heading 3\', class: \'ck-heading_heading3\' }\r\n          \r\n        ]\r\n      },\r\n      fontSize: {\r\n        options: [\r\n          9,\r\n          10,\r\n          11,\r\n          12,\r\n          13,\r\n          \'default\',\r\n          15,\r\n          16,\r\n          17,\r\n          18,\r\n          19,\r\n          20,\r\n          22,\r\n          24,\r\n          26,\r\n        ]\r\n      },\r\n      ckfinder: {\r\n        uploadUrl: _post_upload_image_action\r\n      },\r\n      mediaEmbed: {\r\n        previewsInData: true,\r\n        removeProviders: [\'googleMaps\'],\r\n        extraProviders: [\r\n          {\r\n            name: \'adiva\',\r\n            url: /^(.*)\\/([\\w-]+)/,\r\n            html: match => {\r\n              const url = match[ 0 ];\r\n            if(url.indexOf(\'https://aidea.style\') != -1){\r\n                var iframeUrl = url + \'/embed/\';\r\n              }else if(url.indexOf(\'<iframe\') != -1){\r\n                var iframeUrl = url.split(\'src=\')[1].split(/[ >]/)[0];\r\n                iframeUrl = iframeUrl.replace(\'\"\', \'\');\r\n              }else if(url.indexOf(\'https://peugeotscooters.jp/spring_campaign_2020/index.html\') != -1){\r\n                var iframeUrl = url + \'/?view=embed/\';\r\n              }else if(url.indexOf(\'https://aidea.net/shiro\') != -1){\r\n                var iframeUrl = url + \'/embed/\';\r\n              }else{  \r\n                if(url.indexOf(\'google.com/maps/embed\') != -1){\r\n                  var iframeUrl = match[\'input\'];\r\n                }else{\r\n                  var iframeUrl = url + \'?view=embed\';\r\n                }     \r\n              }\r\n              return (\r\n                \'<div class=\"iframely-embed\">\' +\r\n                    \'<div class=\"iframely-responsive\">\' +\r\n                        `<iframe src=\"${ iframeUrl }\" ` +\r\n                            \'frameborder=\"0\" height=\"270\">\' +\r\n                        \'</iframe>\' +\r\n                    \'</div>\' +\r\n                \'</div>\'\r\n              );\r\n            }\r\n          },\r\n          {\r\n            name: \'any\',\r\n            url: /.+/,\r\n            html: match => {\r\n              const url = match[ 0 ];\r\n              if(url.indexOf(\'[slider id=\') != -1){\r\n                var sliderId = url.substring(\r\n                  url.lastIndexOf(\"[slider id=\") + 11, \r\n                  url.lastIndexOf(\"]\")\r\n                );\r\n                return (\r\n                  \'<div class=\"post-slider\" data-id=\"\'+sliderId+\'\">[slider id=\' + sliderId +\']</div>\'\r\n                );\r\n              }else{\r\n                var iframeUrl = url;\r\n                return (\r\n                  \'<div class=\"iframely-embed\">\' +\r\n                      \'<div class=\"iframely-responsive\">\' +\r\n                          `<iframe src=\"${ iframeUrl }\" ` +\r\n                              \'frameborder=\"0\" height=\"270\">\' +\r\n                          \'</iframe>\' +\r\n                      \'</div>\' +\r\n                  \'</div>\'\r\n                );\r\n              } \r\n              \r\n              \r\n            }\r\n          }\r\n        ]\r\n      },\r\n      language: \'ja\'\r\n    } )\r\n    .then( editor => {\r\n      //console.log( Array.from( editor.ui.componentFactory.names() ) );\r\n      console.log( editor );\r\n    } )\r\n    .catch( error => {\r\n      console.error( error );\r\n    } );\r\n  }\r\n  $(\'body\').delegate(\'.ck-media-form .ck-input-text\', \'change\', function(e){\r\n    var value = $(this).val();\r\n    if(value.indexOf(\'<iframe\') != -1){\r\n      var iframeUrl = value.split(\'src=\')[1].split(/[ >]/)[0];\r\n      iframeUrl = iframeUrl.replace(\'\"\', \'\');\r\n      $(this).val(iframeUrl);\r\n    }\r\n  });\r\n  $(\'body\').delegate(\'.ck-media-form .ck-button-save\', \'click\', function(e){\r\n    var form = $(this).parents(\'.ck-media-form\');\r\n    var input = $(form).find(\'.ck-input-text\');\r\n    var value = $(input).val();\r\n    if(value.indexOf(\'<iframe\') != -1){\r\n      var iframeUrl = value.split(\'src=\')[1].split(/[ >]/)[0];\r\n      iframeUrl = iframeUrl.replace(\'\"\', \'\');\r\n      $(input).val(iframeUrl);\r\n    }\r\n  });\r\n});', '2020-02-03 23:51:39', '2020-02-06 19:09:38'),
(25, 'footer_menu_appearance', 'footer_menu_appearance', 'hide', '2020-02-11 11:18:56', '2020-06-22 11:15:58'),
(26, 'footer_banner_appearance', 'footer_banner_appearance', 'show', '2020-02-11 11:18:56', '2020-06-17 15:00:59'),
(27, 'general_favicon', 'general_favicon', '45', '2020-02-11 16:36:35', '2020-05-04 00:00:05'),
(28, 'widget_image', 'widget_image', '/storage/media/original/1587788831_ruby-cover_MG_3820-768x394.jpg', '2020-02-14 13:22:22', '2020-04-25 13:27:15');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `cover_image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `title`, `description`, `cover_image_url`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Thai Dai', 'askdjasgdasgdgaskgdkashdas', 'http://d1uzk9o9cg136f.cloudfront.net/resource/cover/10032.jpg', 1, '2019-11-01 02:59:15', '2019-12-25 02:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `support_emails`
--

CREATE TABLE `support_emails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_approvide` tinyint(1) NOT NULL DEFAULT '0',
  `approvide_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support_emails`
--

INSERT INTO `support_emails` (`id`, `email`, `is_approvide`, `approvide_key`, `created_at`, `updated_at`) VALUES
(1, 'abc@gmail.com', 1, 'xx2yj0v9H6xoLFX7wGnYVgHWiGPZRKp1PlkU2yjeFo9lRiQeX0', '2019-11-07 02:25:49', '2019-11-07 02:25:49'),
(4, 'nguyendaithai2203@gmail.com', 0, 'O0wkU7FvdzNoU3klLULOXufdL9c3UPl8QLr9MlUlB3tuQLfLzC', '2019-12-25 02:56:04', '2019-12-25 02:56:04'),
(5, 'admin@yopmail.com', 0, 'KlOMFFBSPZYJ5XTKe3UJrDzSLbV5tpmywGjOoP78NGWqidU6eo', '2019-12-25 02:56:11', '2019-12-25 02:56:11'),
(6, 'tantran1104@gmail.com', 0, 'dPuM4jDyW2YqtO6vKJrXRkwdelcXMkenJUsbAMC8o2T9PlcstT', '2019-12-25 02:56:19', '2019-12-25 02:56:19'),
(7, 'nguyenthiphuocnk@gmail.com', 0, 'x6fncVt6n6WYSSYHrngC4IsTAiNwcznLj8RVaTD9AXm3xf6AB9', '2019-12-25 02:56:22', '2019-12-25 02:56:22'),
(8, 'harigane@adiva.co.jp', 0, 'Rbjm49e1sZvED1toGFp0xsfkzfsrGIoYKjtTkXbhiFtk8E4TVy', '2019-12-25 02:56:26', '2019-12-25 02:56:26'),
(9, 'hanh@do-ke.jp', 0, 'BDQhfj0bJPkltF7974mJGIBr7mvT0z6o10klUNbfX5QUWMkf4O', '2020-01-08 10:30:12', '2020-01-08 10:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_featured_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `order`, `created_at`, `updated_at`, `featured_image`, `sub_featured_image`) VALUES
(15, '知る', 1, '2019-11-19 02:35:53', '2020-07-09 12:41:34', NULL, NULL),
(17, 'あそぶ', 2, '2019-11-19 02:36:15', '2020-07-09 12:41:34', NULL, NULL),
(41, 'aidea style', 3, NULL, '2020-07-09 12:41:34', NULL, NULL),
(46, 'aidea', 4, NULL, '2020-07-09 12:41:34', NULL, NULL),
(49, 'AA-4', 5, NULL, '2020-07-09 12:41:34', NULL, NULL),
(51, 'motorcycle', 6, NULL, '2020-07-09 12:41:34', NULL, NULL),
(56, 'AA-1', 7, NULL, '2020-07-09 12:41:34', NULL, NULL),
(59, 'VX-1', 8, NULL, '2020-07-09 12:41:34', NULL, NULL),
(110, 'ツイート', 9, NULL, '2020-07-09 12:41:34', NULL, NULL),
(120, 'プジョー.プジョーモトシクル', 10, NULL, '2020-07-09 12:41:34', NULL, NULL),
(172, 'SDGs', 11, NULL, '2020-07-09 12:41:34', NULL, NULL),
(257, '動画', 12, NULL, '2020-07-10 17:29:28', NULL, NULL),
(258, '電動バイク', 13, NULL, '2020-07-10 17:29:28', NULL, NULL),
(269, '赤坂ショールーム', 14, NULL, '2020-07-10 17:29:28', NULL, NULL),
(286, '赤坂ランチ', 15, NULL, '2020-07-10 17:29:28', NULL, NULL),
(296, '電動バイクの夢を見るか？', 16, NULL, '2020-07-10 17:29:28', NULL, NULL),
(319, '作ってみた', 17, NULL, '2020-07-10 17:29:28', NULL, NULL),
(353, 'アディバ', 18, NULL, '2020-07-10 17:29:28', NULL, NULL),
(398, 'アイディアレディ遊ぶ', 19, NULL, '2020-07-10 17:29:28', NULL, NULL),
(512, 'ジャンゴ', 20, NULL, '2020-07-10 17:29:28', NULL, NULL),
(513, 'プジョー', 21, NULL, '2020-07-10 17:29:28', NULL, NULL),
(515, '遊ぶ', 22, NULL, '2020-07-10 17:29:28', NULL, NULL),
(516, 'アイディアレディ', 23, NULL, '2020-07-10 17:29:28', NULL, NULL),
(517, '不二子ちゃんになりたい', 24, NULL, '2020-07-10 17:29:28', NULL, NULL),
(520, 'プジョーモトシクル', 25, NULL, '2020-07-10 17:29:28', NULL, NULL),
(523, 'シティスター', 26, NULL, '2020-07-10 17:29:28', NULL, NULL),
(534, '初心者ライダーが乗ってみた', 27, NULL, '2020-07-10 17:29:28', NULL, NULL),
(554, 'スピードファイト', 28, NULL, '2020-07-10 17:29:28', NULL, NULL),
(646, 'SHIRO', 29, NULL, '2020-07-10 17:29:28', NULL, NULL),
(647, 'ヘルメット', 30, NULL, '2020-07-10 17:29:28', NULL, NULL),
(648, 'EXPERIENCE', 31, NULL, '2020-07-10 17:29:28', NULL, NULL),
(649, 'DESIGN', 32, NULL, '2020-07-10 17:29:28', NULL, NULL),
(650, 'News', 34, NULL, '2020-07-10 17:29:28', NULL, NULL),
(651, 'TECHNOLOGY', 33, NULL, '2020-07-10 17:29:28', NULL, NULL),
(655, '投稿', 35, '2020-07-09 12:44:00', '2020-07-10 17:29:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tag_post`
--

CREATE TABLE `tag_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tag_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag_post`
--

INSERT INTO `tag_post` (`id`, `tag_id`, `post_id`, `created_at`, `updated_at`, `tag_order`) VALUES
(80, 46, 65, NULL, NULL, 2),
(84, 46, 63, NULL, NULL, 3),
(101, 46, 71, NULL, NULL, 3),
(102, 49, 71, NULL, NULL, 4),
(103, 49, 63, NULL, NULL, 4),
(104, 46, 70, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `writer_profile_html` text COLLATE utf8_unicode_ci,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receive_notification` tinyint(1) NOT NULL DEFAULT '0',
  `disable_article_publishing` tinyint(1) NOT NULL DEFAULT '0',
  `site_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_official_writer` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `writer_profile_html`, `facebook_id`, `twitter_id`, `receive_notification`, `disable_article_publishing`, `site_id`, `is_official_writer`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'Admin', 'admin@yopmail.com', '$2y$10$TjTK2SWFQBGoQtW3CodvcOLsNtSG2gXzIWtkzJgCL5Rxfy6mwuPGS', 'avatar/1580651157_adiva-news.jpg', 'Test decscription html', NULL, NULL, 1, 0, 1, 0, NULL, '2019-11-01 02:59:15', '2020-04-08 15:10:12', 1),
(4, 'aidea NEWS_change', 'blair42@rutherford.com', '$2y$10$WdMPmQj3bD8yRvlL0ou7wOC1fisAMIKWOfRKse/OK/wwhcmk39oU2', 'avatar/1579235880_aidea NEWS.jpg', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:21', '2020-06-08 17:10:00', 2),
(5, 'ショールーム', 'king.keshawn@wolff.net', '$2y$10$.5pf7noueSruiNlAzr1z5.mXTWIAR6f4/...4oRRzdokCvh0LwmjS', 'avatar/1579235932_ショールーム.jpg', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2020-01-17 13:40:37', 1),
(6, '成田裕一郎', 'karolann.okon@block.com', '$2y$10$ls6/SPET.tltztl/q8/nZuxdUwdEjQoa9B0DjJ6QfVNs9DmPMfwKa', 'avatar/1579236120_成田裕一郎.jpg', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2020-01-17 13:42:03', 1),
(8, '玉井里菜', 'reynold.bogan@hotmail.com', '$2y$10$b221qKWWNXscpP9kgdK4UOBGE3vNAa7aQ9nSAEu4TaxG8eHzj2t5u', 'avatar/1579236157_玉井里菜.jpg', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2020-07-01 11:59:12', 1),
(9, 'アイディアレディ', 'uthompson@hotmail.com', '$2y$10$Ku6l4w7iyJqXAKDJdLEu.u4hTkMFtsQvttIIG/X2HyEybqvZnjSUS', 'avatar/1579236225_アイディアレディ.jpg', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2020-01-17 13:47:39', 1),
(10, '営業部', 'padberg.rebecca@hotmail.com', '$2y$10$GxKpS0oLUtTd8jQ9wuRqH.donU8pGtpzmO.6RxrEoOY2PSE6iU7hS', 'avatar/1579236732_営業部.jpg', NULL, NULL, NULL, 0, 0, 1, 0, 'D3CSTf6YAjouoWHRlPx96mGTgOOsWL8Ec81moh4QqLqVojbxWlsKsj76As8R', '2019-11-05 03:39:22', '2020-04-09 18:58:54', 1),
(27, '永田達哉', 'tatsuya.nagata@aidea.net', '$2y$10$euXAEOIQs.ihph/JfC6YOe3Ze/yQ5sl7vpC6jJl5BXBtHI0idwWDm', NULL, 'ぷろふぃーる', NULL, NULL, 0, 0, 1, 0, NULL, '2020-06-16 18:49:32', '2020-06-16 18:49:32', 2);

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `opt_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opt_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opt_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opt_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opt_5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`id`, `name`, `opt_1`, `opt_2`, `opt_3`, `opt_4`, `opt_5`, `created_at`, `updated_at`, `content`) VALUES
(7, 'Test', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'yyyyyyyyyyyyyyyyyyyyyyyyyy', 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz', NULL, NULL, '2020-04-23 07:37:39', '2020-04-23 07:37:39', 'Test vote'),
(8, 'お弁当アンケート', '焼鮭弁当', 'エビ天丼弁当', '豚バラ生姜焼き弁当', '薩摩地鶏の照焼弁当', '白身魚のフライ弁当', '2020-05-21 10:22:32', '2020-05-21 10:22:32', '姉妹会社・魚月で販売を開始したお弁当の中でどれが一番食べたいなと思っていただけましたか？');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_post_category_id_foreign` (`category_id`),
  ADD KEY `category_post_post_id_foreign` (`post_id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medias_post_id_foreign` (`post_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_tags`
--
ALTER TABLE `menu_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_tags_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`),
  ADD KEY `pages_cover_image_foreign` (`cover_image`),
  ADD KEY `pages_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_site_id_foreign` (`site_id`),
  ADD KEY `posts_vote_id_foreign` (`vote_id`);

--
-- Indexes for table `post_sliders`
--
ALTER TABLE `post_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_sliders_post_id_foreign` (`post_id`);

--
-- Indexes for table `post_user`
--
ALTER TABLE `post_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_user_post_id_foreign` (`post_id`),
  ADD KEY `post_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_views`
--
ALTER TABLE `post_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_views_post_id_foreign` (`post_id`),
  ADD KEY `post_views_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_user_id_foreign` (`user_id`);

--
-- Indexes for table `support_emails`
--
ALTER TABLE `support_emails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `support_emails_email_unique` (`email`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_post`
--
ALTER TABLE `tag_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_post_tag_id_foreign` (`tag_id`),
  ADD KEY `tag_post_post_id_foreign` (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `category_post`
--
ALTER TABLE `category_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `menu_tags`
--
ALTER TABLE `menu_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `post_sliders`
--
ALTER TABLE `post_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `post_user`
--
ALTER TABLE `post_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_views`
--
ALTER TABLE `post_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `support_emails`
--
ALTER TABLE `support_emails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=656;

--
-- AUTO_INCREMENT for table `tag_post`
--
ALTER TABLE `tag_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `category_post`
--
ALTER TABLE `category_post`
  ADD CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `medias`
--
ALTER TABLE `medias`
  ADD CONSTRAINT `medias_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `menu_tags`
--
ALTER TABLE `menu_tags`
  ADD CONSTRAINT `menu_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_cover_image_foreign` FOREIGN KEY (`cover_image`) REFERENCES `medias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_vote_id_foreign` FOREIGN KEY (`vote_id`) REFERENCES `votes` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `post_sliders`
--
ALTER TABLE `post_sliders`
  ADD CONSTRAINT `post_sliders_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_user`
--
ALTER TABLE `post_user`
  ADD CONSTRAINT `post_user_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_views`
--
ALTER TABLE `post_views`
  ADD CONSTRAINT `post_views_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_views_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tag_post`
--
ALTER TABLE `tag_post`
  ADD CONSTRAINT `tag_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tag_post_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
